# Module DB Edition #

This module provides a way to create a UI that allows to display and edit database information while keeping consistency. It works by using a JavaScript configuration file.

## Version ##

Current version : v2.0.0.

## Known bugs ##

- The previewEdit feature does not work correctly with more than one deep layer in the detailled view
- Extensible on non array type is not implemented
- Extensible on non simple arrays is not implemented

## User manual ##
### Supported display types ###

#### Component ####

Component in a cell, receive data in a data binding.

#### Readonly ####

Display an information as a string. Cannot be edited even in edit mode. ---> should be replaced soon

#### Text ####

Display an information as a string, and provide an input box type text to enter data.
Supported `saveAs`:
- String

#### Number ####

Display an information as a string, and provide an input box type number to enter data.
Supported `saveAs`:
- Number

#### Checkbox ####

Display an information as a checkbox, and provide an input box type `checkbox` to enter data.
Supported `saveAs`:
- Boolean


## Dev manual ##
### Plugins apis ###

RetrieveData (collection, configuration) : object;

configuration: {
  from: int
  to: int

  searchOn: string
  searchInput: string

  sortOn: string
  reverse: boolean

  \_id: id
}

UpdateData (collection, id, cookie, updateObject)

InsertData (collection, object)

DeleteData (collection, id)

LockData (collection, id)

UnlockData (cookie)
