﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Dynamic;
using System.Collections.Generic;

namespace UnitTestsDBEdition
{
    [TestClass]
    public class TestDeleteData
    {
        public static Utils utils;
        public const string populatedCollection = "s_TestDeleteData";

        [ClassInitialize()]
        public static void InitDb(TestContext testContext)
        {
            List<ExpandoObject> entries = new List<ExpandoObject>();
            dynamic json;

            json = new ExpandoObject();
            json._id = "s_0001";
            json.age = "n_1";
            entries.Add(json);

            utils = new Utils(populatedCollection, entries);
        }

        [ClassCleanup()]
        public static void CleanUpDb()
        {
            utils.ReleaseUtils();
            utils = null;
        }

        [TestMethod]
        public async Task TestDeleteData1()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json._id = "s_0001";

            res = await utils.PostAsync(Utils.deleteData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);

            // check that it was deleted
            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }
    }
}
