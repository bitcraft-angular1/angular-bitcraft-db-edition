﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Dynamic;
using System.Collections.Generic;

namespace UnitTestsDBEdition
{
    [TestClass]
    public class TestDeleteDataInvalid
    {
        public static Utils utils;
        public const string populatedCollection = "s_TestDeleteDataInvalid";

        [ClassInitialize()]
        public static void InitDb(TestContext testContext)
        {
            utils = new Utils(populatedCollection);
        }

        [ClassCleanup()]
        public static void CleanUpDb()
        {
            utils.ReleaseUtils();
            utils = null;
        }

        [TestMethod]
        public async Task TestDeleteDataInvalid1()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json._id = "s_0001";

            res = await utils.PostAsync(Utils.deleteData, json);
            Assert.AreEqual(HttpStatusCode.NotFound, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }
    }
}
