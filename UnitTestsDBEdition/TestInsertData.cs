﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Dynamic;
using System.Collections.Generic;

namespace UnitTestsDBEdition
{
    [TestClass]
    public class TestInsertData
    {
        public static Utils utils;
        public const string populatedCollection = "s_TestInsertData";

        [ClassInitialize()]
        public static void InitDb(TestContext testContext)
        {
            utils = new Utils(populatedCollection);
        }

        [ClassCleanup()]
        public static void CleanUpDb()
        {
            Task<Tuple<HttpResponseMessage, dynamic>> res;
            dynamic json;

            json = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json._id = "s_0001";
            res = utils.PostAsync(Utils.deleteData, json);
            res.Wait();

            utils.ReleaseUtils();
            utils = null;
        }

        [TestMethod]
        public async Task TestInsertData1()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.@object = new ExpandoObject();
            json.@object._id = "s_0001";
            json.@object.age = "n_24";

            res = await utils.PostAsync(Utils.insertData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);

            // check that we can retrieve the added item
            json = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration = new ExpandoObject();
            json.configuration._id = "s_0001";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);
            Utils.AreEqual("n_24", res.Item2.data[0].age);
        }
    }
}
