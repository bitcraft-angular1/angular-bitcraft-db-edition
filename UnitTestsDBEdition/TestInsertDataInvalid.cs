﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Dynamic;
using System.Collections.Generic;

namespace UnitTestsDBEdition
{
    [TestClass]
    public class TestInsertDataInvalid
    {
        public static Utils utils;
        public const string populatedCollection = "s_TestInsertDataInvalid";

        [ClassInitialize()]
        public static void InitDb(TestContext testContext)
        {
            utils = new Utils(populatedCollection);
        }

        [ClassCleanup()]
        public static void CleanUpDb()
        {
            utils.ReleaseUtils();
            utils = null;
        }

        [TestMethod]
        public async Task TestInsertDataInvalid1()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.collection = utils.PopulatedCollection;

            res = await utils.PostAsync(Utils.insertData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInsertDataInvalid2()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.@object = new ExpandoObject();
            json.@object._id = "s_0001";
            json.@object.age = "n_24";

            res = await utils.PostAsync(Utils.insertData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInsertDataInvalid3()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.@object = new ExpandoObject();
            json.@object._id = "s_0001";
            json.@object.age = "n_24";

            res = await utils.PostAsync(Utils.insertData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);

            res = await utils.PostAsync(Utils.insertData, json);
            Assert.AreEqual(HttpStatusCode.InternalServerError, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }
    }
}
