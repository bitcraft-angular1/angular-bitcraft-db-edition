﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Dynamic;
using System.Collections.Generic;

namespace UnitTestsDBEdition
{
    [TestClass]
    public class TestLockData
    {
        public static Utils utils;
        public const string populatedCollection = "s_TestLockData";

        [ClassInitialize()]
        public static void InitDb(TestContext testContext)
        {
            List<ExpandoObject> entries = new List<ExpandoObject>();
            dynamic json;

            for (int i = 1; i < 6; i++)
            {
                json = new ExpandoObject();
                json._id = "s_000"+i;
                json.age = "n_1";
                entries.Add(json);
            }

            utils = new Utils(populatedCollection, entries);
        }

        [ClassCleanup()]
        public static void CleanUpDb()
        {
            utils.ReleaseUtils();
            utils = null;
        }

        [TestMethod]
        public async Task TestLockData1()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json._id = "s_0001";
            json.collection = utils.PopulatedCollection;

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);
        }

        [TestMethod]
        public async Task TestLockData2()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json._id = "s_0002";
            json.collection = utils.PopulatedCollection;

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string cookie = res.Item2.cookie;
            json.cookie = cookie;
            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);
        }

        [TestMethod]
        public async Task TestLockData3()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json._id = "s_0003";
            json.collection = utils.PopulatedCollection;

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            // lock a second item, freeing the first one
            json._id = "s_0004";
            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            // lock again the first one
            json._id = "s_0003";
            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);
        }

        [TestMethod]
        public async Task TestLockData4()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json._id = "s_0004";
            json.collection = utils.PopulatedCollection;

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string cookie = res.Item2.cookie;
            json.cookie = cookie;
            res = await utils.PostAsync(Utils.unlockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);

            json = new ExpandoObject();
            json._id = "s_0004";
            json.collection = utils.PopulatedCollection;

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);
        }
    }
}
