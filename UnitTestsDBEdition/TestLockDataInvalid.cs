﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Dynamic;
using System.Collections.Generic;

namespace UnitTestsDBEdition
{
    [TestClass]
    public class TestLockDataInvalid
    {
        public static Utils utils;
        public const string populatedCollection = "s_TestLockDataInvalid";

        [ClassInitialize()]
        public static void InitDb(TestContext testContext)
        {
            List<ExpandoObject> entries = new List<ExpandoObject>();
            dynamic json;

            for (int i = 1; i < 6; i++)
            {
                json = new ExpandoObject();
                json._id = "s_000" + i;
                json.age = "n_1";
                entries.Add(json);
            }

            utils = new Utils(populatedCollection, entries);
        }

        [ClassCleanup()]
        public static void CleanUpDb()
        {
            utils.ReleaseUtils();
            utils = null;
        }

        [TestMethod]
        public async Task TestLockDataInvalid1()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.collection = utils.PopulatedCollection;

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestLockDataInvalid2()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json._id = "s_0001";

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestLockDataInvalid3()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json._id = "s_0001";
            json.collection = "b_true";

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestLockDataInvalid4()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json._id = "s_0001";
            json.collection = "n_15";

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestLockDataInvalid5()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json._id = "s_0001";
            json.collection = utils.PopulatedCollection;

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(423, (int)res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
           // Utils.AreEqual("s_Seb", res.Item2.owner);
        }
    }
}
