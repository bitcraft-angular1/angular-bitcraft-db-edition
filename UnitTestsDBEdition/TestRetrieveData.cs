﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Dynamic;
using System.Collections.Generic;

namespace UnitTestsDBEdition
{
    [TestClass]
    public class TestRetrieveData
    {
        public static Utils utils;
        public const string populatedCollection = "s_TestRetrieveData";

        [ClassInitialize()]
        public static void InitDb(TestContext testContext)
        {
            List<ExpandoObject> entries = new List<ExpandoObject>();
            dynamic json;

            json = new ExpandoObject();
            json._id = "s_4578-2456-9749";
            json.age = "n_12";
            json.name = "s_Alice";
            entries.Add(json);

            json = new ExpandoObject();
            json._id = "s_4578-2456-9750";
            json.age = "n_17";
            json.name = "s_Jean-Jacques";
            entries.Add(json);

            json = new ExpandoObject();
            json._id = "s_4578-2456-9751";
            json.age = "n_22";
            json.name = "s_かすみ";
            entries.Add(json);

            json = new ExpandoObject();
            json._id = "s_4578-2456-9752";
            json.age = "n_4";
            json.name = "s_Bob";
            entries.Add(json);

            json = new ExpandoObject();
            json._id = "s_4578-2456-9753";
            json.age = "n_16";
            json.name = "s_Triton";
            entries.Add(json);

            utils = new Utils(populatedCollection, entries);
        }

        [ClassCleanup()]
        public static void CleanUpDb()
        {
            utils.ReleaseUtils();
            utils = null;
        }

        [TestMethod]
        public async Task TestRetrieveData1()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);
            Assert.AreEqual("n_5", res.Item2.count.ToString());
        }

        [TestMethod]
        public async Task TestRetrieveData2()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = "s_trtalala";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.AreEqual("n_0", res.Item2.count.ToString());
        }

        [TestMethod]
        public async Task TestRetrieveData3()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.from = "n_0";
            json.configuration.to = "n_2";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);
            Assert.AreEqual(2, res.Item2.data.Count);

            string[] names = { "s_Alice", "s_Jean-Jacques" };
            string[] ages = { "n_12", "n_17" };

            for (int i = 0; i < res.Item2.data.Count; i++)
            {
                Utils.AreEqual(names[i], res.Item2.data[i].name);
                Utils.AreEqual(ages[i], res.Item2.data[i].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData4()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.from = "n_3";
            json.configuration.to = "n_5";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string[] names = { "s_Bob", "s_Triton" };
            string[] ages = { "n_4", "n_16" };

            for (int i = 0; i < res.Item2.data.Count; i++)
            {
                Utils.AreEqual(names[i], res.Item2.data[i].name);
                Utils.AreEqual(ages[i], res.Item2.data[i].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData5()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.sortOn = "s_age";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string[] names = { "s_Bob", "s_Alice", "s_Triton", "s_Jean-Jacques", "s_かすみ" };
            string[] ages = { "n_4", "n_12", "n_16", "n_17", "n_22" };

            for (int i = 0; i < res.Item2.data.Count; i++)
            {
                Utils.AreEqual(names[i], res.Item2.data[i].name);
                Utils.AreEqual(ages[i], res.Item2.data[i].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData6()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.sortOn = "s_name";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string[] names = { "s_Alice", "s_Bob", "s_Jean-Jacques", "s_Triton", "s_かすみ" };
            string[] ages = { "n_12", "n_4", "n_17", "n_16", "n_22" };

            for (int i = 0; i < res.Item2.data.Count; i++)
            {
                Utils.AreEqual(names[i], res.Item2.data[i].name);
                Utils.AreEqual(ages[i], res.Item2.data[i].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData7()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.sortOn = "s_age";
            json.configuration.reverse = "b_true";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string[] names = { "s_Bob", "s_Alice", "s_Triton", "s_Jean-Jacques", "s_かすみ" };
            string[] ages = { "n_4", "n_12", "n_16", "n_17", "n_22" };

            for (int i = res.Item2.data.Count - 1, j = 0; i >= 0 ; i--, j++)
            {
                Utils.AreEqual(names[i], res.Item2.data[j].name);
                Utils.AreEqual(ages[i], res.Item2.data[j].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData8()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.sortOn = "s_name";
            json.configuration.reverse = "b_true";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string[] names = { "s_Alice", "s_Bob", "s_Jean-Jacques", "s_Triton", "s_かすみ" };
            string[] ages = { "n_12", "n_4", "n_17", "n_16", "n_22" };

            for (int i = res.Item2.data.Count - 1, j = 0; i >= 0; i--, j++)
            {
                Utils.AreEqual(names[i], res.Item2.data[j].name);
                Utils.AreEqual(ages[i], res.Item2.data[j].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData9()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.sortOn = "s_name";
            json.configuration.from = "n_0";
            json.configuration.to = "n_3";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string[] names = { "s_Alice", "s_Bob", "s_Jean-Jacques" };
            string[] ages = { "n_12", "n_4", "n_17" };

            for (int i = 0; i < res.Item2.data.Count; i++)
            {
                Utils.AreEqual(names[i], res.Item2.data[i].name);
                Utils.AreEqual(ages[i], res.Item2.data[i].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData10()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "s_name";
            json.configuration.searchInput = "s_a";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);
            Assert.AreEqual(1, res.Item2.data.Count);

            string[] names = { "s_Jean-Jacques" };
            string[] ages = { "n_17" };

            for (int i = 0; i < res.Item2.data.Count; i++)
            {
                Utils.AreEqual(names[i], res.Item2.data[i].name);
                Utils.AreEqual(ages[i], res.Item2.data[i].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData11()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "s_name";
            json.configuration.searchInput = "s_o";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string[] names = { "s_Bob", "s_Triton" };
            string[] ages = { "n_4", "n_16" };

            for (int i = 0; i < res.Item2.data.Count; i++)
            {
                Utils.AreEqual(names[i], res.Item2.data[i].name);
                Utils.AreEqual(ages[i], res.Item2.data[i].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData12()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "s_name";
            json.configuration.searchInput = "s_o";
            json.configuration.from = "n_0";
            json.configuration.to = "n_1";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string[] names = { "s_Bob" };
            string[] ages = { "n_4" };

            for (int i = 0; i < res.Item2.data.Count; i++)
            {
                Utils.AreEqual(names[i], res.Item2.data[i].name);
                Utils.AreEqual(ages[i], res.Item2.data[i].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData13()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "s_name";
            json.configuration.searchInput = "s_o";
            json.configuration.from = "n_1";
            json.configuration.to = "n_2";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string[] names = { "s_Triton" };
            string[] ages = { "n_16" };

            for (int i = 0; i < res.Item2.data.Count; i++)
            {
                Utils.AreEqual(names[i], res.Item2.data[i].name);
                Utils.AreEqual(ages[i], res.Item2.data[i].age);
            }
        }

        [TestMethod]
        public async Task TestRetrieveData14()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration._id = "s_4578-2456-9750";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            string[] names = { "s_Jean-Jacques" };
            string[] ages = { "n_17" };

            for (int i = 0; i < res.Item2.data.Count; i++)
            {
                Utils.AreEqual(names[i], res.Item2.data[i].name);
                Utils.AreEqual(ages[i], res.Item2.data[i].age);
            }
        }
    }
}
