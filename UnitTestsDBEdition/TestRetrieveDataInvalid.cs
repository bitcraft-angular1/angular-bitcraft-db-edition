﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Dynamic;
using System.Collections.Generic;

namespace UnitTestsDBEdition
{
    [TestClass]
    public class TestRetrieveDataInvalid
    {
        public static Utils utils;
        public const string populatedCollection = "s_TestRetrieveDataInvalid";

        [ClassInitialize()]
        public static void InitDb(TestContext testContext)
        {
            utils = new Utils(populatedCollection);
        }

        [ClassCleanup()]
        public static void CleanUpDb()
        {
            utils.ReleaseUtils();
            utils = null;
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData1()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData2()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData3()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.from = "n_1";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData4()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.to = "n_1";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData5()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.reverse = "b_true";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData6()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.reverse = "n_1";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData7()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchInput = "s_a";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData8()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "s_name";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData9()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "s_name";
            json.configuration.searchInput = "s_";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData10()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "s_name";
            json.configuration.searchInput = "n_1";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData11()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "s_name";
            json.configuration.searchInput = "b_true";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData12()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "s_";
            json.configuration.searchInput = "s_a";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData13()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "n_1";
            json.configuration.searchInput = "s_a";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }

        [TestMethod]
        public async Task TestInvalidRetrieveData14()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.configuration = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration.searchOn = "b_true";
            json.configuration.searchInput = "s_a";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.BadRequest, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);
        }
    }
}
