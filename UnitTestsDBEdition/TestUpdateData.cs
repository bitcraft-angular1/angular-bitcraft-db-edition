﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Dynamic;
using System.Collections.Generic;

namespace UnitTestsDBEdition
{
    [TestClass]
    public class TestUpdateData
    {
        public static Utils utils;
        public const string populatedCollection = "s_TestUpdateData";

        [ClassInitialize()]
        public static void InitDb(TestContext testContext)
        {
            List<ExpandoObject> entries = new List<ExpandoObject>();
            dynamic json;

            for (int i = 1; i < 5; i++)
            {
                json = new ExpandoObject();
                json._id = "s_000" + i;
                json.@int = "n_1";
                json.@bool = "b_true";
                json.@string = "s_mystring";
                json.@array = new[] { "n_1", "n_2" };
                json.dict = new ExpandoObject();
                json.dict.b = "b_true";
                entries.Add(json);
            }

            utils = new Utils(populatedCollection, entries);
        }

        [ClassCleanup()]
        public static void CleanUpDb()
        {
            utils.ReleaseUtils();
            utils = null;
        }

        [TestMethod]
        public async Task TestUpdateData1()
        {
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            dynamic test = JsonConvert.DeserializeObject("{ \"_id\": \"s_0001\", \"int\": \"n_1\", \"bool\": \"b_true\", \"string\": \"s_mystring\", \"array\": [\"n_1\", \"n_2\"], \"dict\": {\"b\": \"b_true\"} }");

            Utils.AreEqual("n_1", test.@int);
            Utils.AreEqual("b_true", test.@bool);
            Utils.AreEqual("s_mystring", test.@string);
            Utils.AreEqual("n_1", test.array[0]);
            Utils.AreEqual("n_2", test.array[1]);
            Utils.AreEqual("b_true", test.dict.b);

            // lock it
            json = new ExpandoObject();
            json._id = "s_0001";
            json.collection = utils.PopulatedCollection;

            res = await utils.PostAsync(Utils.lockData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);
            string cookie = res.Item2.cookie;

            // update it
            json.updateObject = new ExpandoObject();
            json.updateObject.@int = "n_5";
            json.updateObject.@bool = "b_false";
            json.cookie = cookie;

            res = await utils.PostAsync(Utils.updateData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNull(res.Item2);

            // verify it
            json = new ExpandoObject();
            json.collection = utils.PopulatedCollection;
            json.configuration = new ExpandoObject();
            json.configuration._id = "s_0001";

            res = await utils.PostAsync(Utils.retrieveData, json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            Assert.IsNotNull(res.Item2);

            Utils.AreEqual("n_5", res.Item2.data[0].@int);
            Utils.AreEqual("b_false", res.Item2.data[0].@bool);
            Utils.AreEqual("s_mystring", res.Item2.data[0].@string);
            Utils.AreEqual("n_1", res.Item2.data[0].array[0]);
            Utils.AreEqual("n_2", res.Item2.data[0].array[1]);
            Utils.AreEqual("b_true", res.Item2.data[0].dict.b);
        }
    }
}
