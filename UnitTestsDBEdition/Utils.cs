﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UnitTestsDBEdition
{
    public class Utils
    {
        public const int HttpLocked = 423;

        private const string serverUri = "http://localhost:1340/rest/";
        public const string lockData = serverUri + "lockData";
        public const string unlockData = serverUri + "unlockData";
        public const string retrieveData = serverUri + "retrieveData";
        public const string updateData = serverUri + "updateData";
        public const string deleteData = serverUri + "deleteData";
        public const string insertData = serverUri + "insertData";

        public string PopulatedCollection { get; }

        public HttpClient Client { get; } = new HttpClient(new HttpClientHandler { UseCookies = false });

        public Utils(string collectionToPopulate)
        {
            PopulatedCollection = collectionToPopulate;
        }

        public Utils(string collectionToPopulate, List<ExpandoObject> entries)
        {
            PopulatedCollection = collectionToPopulate;
            Task t = InitDb(entries);
            t.Wait();
        }

        public void ReleaseUtils()
        {
            Task t = CleanUpDb();
            t.Wait();
        }

        private async Task InitDb(List<ExpandoObject> entries)
        {
            // initialise a collection populated with params
            Tuple<HttpResponseMessage, dynamic> res;

            foreach (ExpandoObject jsonObject in entries)
            {
                dynamic json = new ExpandoObject();
                json.collection = PopulatedCollection;
                json.@object = jsonObject;
                res = await PostAsync(insertData, json);
                Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
            }
        }

        private async Task CleanUpDb()
        {
            // initialise a collection populatedTest with a few items
            Tuple<HttpResponseMessage, dynamic> res;
            dynamic json;

            json = new ExpandoObject();
            json.collection = PopulatedCollection;

            res = await PostAsync(serverUri+"cleanDb", json);
            Assert.AreEqual(HttpStatusCode.OK, res.Item1.StatusCode);
        }

        public async Task<Tuple<HttpResponseMessage, dynamic>> PostAsync(string requestUri, ExpandoObject json)
        {
            HttpContent content = new StringContent(JsonConvert.SerializeObject(json), System.Text.Encoding.UTF8, "application/json");
            content.Headers.Add("Cookie", "environment=Developement; token=Seb_qf4gshj71u4n29; groups=admin; NG_TRANSLATE_LANG_KEY=%22en%22"); //if we read the cookie it'll be already prefixed
            var res = await Client.PostAsync(requestUri, content);

            if (res.StatusCode == HttpStatusCode.OK)
            {
                var str = await res.Content.ReadAsStringAsync();
                return Tuple.Create(res, JsonConvert.DeserializeObject(str));
            }
            return Tuple.Create(res, JsonConvert.DeserializeObject(""));
        }

        public static void AreEqual<T>(T t1, JValue t2)
        {
            Assert.AreEqual<T>(t1, (T)t2.Value);
        }
    }
}
