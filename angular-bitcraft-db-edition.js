(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
/**
 * Created by richard on 20/10/16.
 */

'use strict';

var template = '' +
'<style>' +
'    .fa-buttons > div {' +
'        display: inline-block;' +
'        cursor: pointer;' +
'        font-size: 2em;' +
'        margin-top: -5px;' +
'        margin-bottom: -5px;' +
'        padding-right: 3px;' +
'        padding-left: 3px;' +
'    }' +
'    .no-padding {' +
'        padding: 0 !important;' +
'    }' +
'    .innertable {' +
'        margin: -1px;' +
'        width: calc(100% + 2px);' +
'        max-width: calc(100% + 2px);' +
'    }' +
'</style>' +
'<script type="text/ng-template"  id="table_renderer_object.html">' +
'    <table class="table table-bordered table-hover innertable">' +
'        <tr ng-repeat="row in pass">' +
'            <th ng-if="!row.options.hideKey" translate>{{row.options.name}}</th>' +
'            <td ng-if="!row.options.isLeaf" class="no-padding"><div ng-repeat="pass in [row.value]" ng-include="\'table_renderer_object.html\'"></div></td>' +
'            <td ng-if="row.options.isLeaf">' +
'                <div ng-if="row.options.displayAs !== 0">' +
'                    <bitcraft-db-edition-simple-type type="row.options.displayAs" value="row.value" values="$ctrl.getValues(row.options)" edit="true"></bitcraft-db-edition-simple-type>' +
'                </div>' +
'            </td>' +
'        </tr>' +
'    </table>' +
'</script>' +
'<div ng-show="done" id="db-edition-detailed-view">' +
'    <h3 ng-if="global.options.addTitle">{{global.options.addTitle | translate}}</h3>' +
'    <table class="table table-bordered table-hover">' +
'        <tr ng-repeat="row in global.data">' +
'            <th translate>{{row.options.name}}</th>' +
'            <td ng-if="!row.options.isLeaf" class="no-padding">' +
'                <div ng-repeat="pass in [row.value]" ng-include="\'table_renderer_object.html\'"></div>' +
'            </td>' +
'            <td ng-if="row.options.isLeaf">' +
'                <div ng-if="row.options.displayAs !== 0">' +
'                    <bitcraft-db-edition-simple-type type="row.options.displayAs" value="row.value" values="$ctrl.getValues(row.options)" edit="true"></bitcraft-db-edition-simple-type>' +
'                </div>' +
'            </td>' +
'        </tr>' +
'        <tr>' +
'            <td style="white-space: nowrap; text-align: center;" colspan="2">' +
'                <!-- form -->' +
'                <div class="fa-buttons">' +
'                    <div ng-click="$ctrl.add()" style="color: #449d44">' +
'                        <i class="fa fa-save" aria-hidden="true"></i>' +
'                    </div>' +
'                    <div ng-click="$ctrl.cancel()" style="color: #025aa5">' +
'                        <i class="fa fa-ban" aria-hidden="true"></i>' +
'                    </div>' +
'                </div>' +
'            </td>' +
'        </tr>' +
'    </table>' +
'</div>' +
'';

/*global angular*/

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition')
    .component('bitcraftDbEditionAdd', {
        template: template,
        controller: [
            '$scope', '$state', '$stateParams', 'Notification', 'DBEdition',
            function ($scope, $state, $stateParams, Notification, DBEdition) {
                //noinspection JSUnusedGlobalSymbols
                /**
                 * Init the data for display.
                 */
                this.$onInit = function () {
                    DBEdition.getDefaultData($stateParams.command).then(function (data) {
                        console.log(data);
                        $scope.global = data;
                        $scope.done = true;
                    }, function (err) {
                        Notification.error({message: 'Failed to retrieve data (' + err.statusText + ').'});
                    });
                };

                var self = this;

                self.cancel = function () {
                    $state.go('^');
                };

                self.add = function () {
                    DBEdition.insertData($scope.global.options.backend, $scope.global.options.collection, $scope.global.data, $stateParams.command.params).then(function () {
                        $state.go('^');
                    }, function (err) {
                        Notification.error({message: 'Failed to add the entry (' + err.statusText + ').'});
                    });
                };

                // select/dropdown
                self.getValues = function (options) {
                    if (options.values) {
                        return options.getValues();
                    }
                };
            }
        ]
    });

},{}],2:[function(require,module,exports){
/**
 * Created by richard on 20/10/16.
 */

'use strict';
require('./db-edition-add.component');

},{"./db-edition-add.component":1}],3:[function(require,module,exports){
/**
 * Created by richard on 20/10/16.
 */

'use strict';
/*global angular, $*/

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition')
    .component('bitcraftDbEditionComponentWrapper', {
        controller: [
            '$scope', '$element', '$compile',
            function ($scope, $element, $compile) {
                this.$onInit = function () {
                    var componentName = this.data.component.split(/(?=[A-Z])/).join('-');
                    var rowString = JSON.stringify(this.data.row).replace(/"/g, '\'');
                    var compiledComponent = $compile('<' + componentName + ' data="' + rowString + '"></' + componentName + '>')($scope.$new(false, $scope));

                    // 2020/02/20 Hack en mousse
                    // Stores the original object instead of a serialized value.
                    // The idea is to retrieve the good data from the original reference instead of
                    // the initial data (serialized one).
                    var self = this;
                    $element[0].getRowData = function() { return self.data; };

                    $($element[0]).append(compiledComponent);
                };
            }
        ],
        bindings: {
            data: '<'
        }
    });

},{}],4:[function(require,module,exports){
/**
 * Created by richard on 20/10/16.
 */

'use strict';
require('./db-edition-component-wrapper.component');
},{"./db-edition-component-wrapper.component":3}],5:[function(require,module,exports){
/*global angular, $ */
'use strict';

var template = '' +
'<!--suppress CssUnusedSymbol, HtmlUnknownAttribute, HtmlUnknownTag -->' +
'<style>' +
'    /* center the pagination */' +
'    .text-center {' +
'        display:block;' +
'    }' +
'    .text-center > ul {' +
'        margin: 0;' +
'    }' +
'    .table-wrap {' +
'        margin-bottom: 30px;' +
'    }' +
'    .table-wrap table {' +
'        margin-bottom: 5px;' +
'    }' +
'    tr > div, td > div {' +
'        display: inline-block;' +
'    }' +
'    .sort-none {' +
'        width: 0;' +
'        height: 0;' +
'        border: 5px solid transparent;' +
'        display: inline-block;' +
'    }' +
'    .sort-ascendant {' +
'        width: 0;' +
'        height: 0;' +
'        border-left: 5px solid transparent;' +
'        border-right: 5px solid transparent;' +
'        border-bottom: 10px solid black;' +
'        display: inline-block;' +
'    }' +
'    .sort-descendant {' +
'        width: 0;' +
'        height: 0;' +
'        border-left: 5px solid transparent;' +
'        border-right: 5px solid transparent;' +
'        border-top: 10px solid black;' +
'        display: inline-block;' +
'    }' +
'    .fa-buttons > div {' +
'        display: inline-block;' +
'        cursor: pointer;' +
'        font-size: 2em;' +
'        margin-top: -5px;' +
'        margin-bottom: -5px;' +
'        padding-right: 3px;' +
'        padding-left: 3px;' +
'    }' +
'    .red {' +
'        background-color: #a70f0f;' +
'    }' +
'    .green {' +
'        background-color: #04941c;' +
'    }' +
'</style>' +
'<script type="text/ng-template" id="checkModifications.html">' +
'    <div class="modal-header">' +
'        <h3 class="modal-title" translate>DATABASE_EDITION.CONFIRM_MESSAGE</h3>' +
'    </div>' +
'    <div class="modal-body" style="height: 500px;">' +
'        Bliblublo' +
'    </div>' +
'    <div class="modal-footer">' +
'        <button class="btn btn-default" type="button" ng-click="$dismiss()" translate>GENERAL.CANCEL</button>' +
'        <button class="btn btn-primary" type="button" ng-click="$close()" translate>DIALOGS_CONFIRMATION</button>' +
'    </div>' +
'</script>' +
'<div ng-show="done" id="db-edition-global-view">' +
'    <h3 ng-if="options.globalTitle">{{options.globalTitle | translate}}</h3>' +
'    <dir-pagination-controls on-page-change="$ctrl.changePage(newPageNumber)" class="text-center" boundary-links="true"></dir-pagination-controls>' +
'    <table class="table table-bordered table-hover">' +
'        <thead>' +
'            <tr><!-- line with the column titles -->' +
'                <th ng-if="options.edit || options.del || options.detailedView || options.aggregate.length"></th>' +
'                <th ng-click="headerEntry.sortable ? $ctrl.sort($index) : false" ng-repeat="headerEntry in header track by $index" style="white-space: nowrap">' +
'                    {{headerEntry.name | translate}}' +
'                    <div ng-class="headerEntry.sortable ? (headerEntry.sortClass ? headerEntry.sortClass : \'sort-none\') : false"></div>' +
'                </th>' +
'            </tr>' +
'            <tr ng-if="options.search"><!-- line to search -->' +
'                <th ng-if="options.edit || options.del || options.detailedView || options.aggregate.length"></th>' +
'                <th ng-repeat="headerEntry in header track by $index">' +
'                    <input ng-if="headerEntry.searchable" ng-model="headerEntry.searchInput" ng-change="$ctrl.search($index)" type="text" class="form-control" placeholder="{{\'DATABASE_EDITION.SEARCH_IN\' | translate}} {{headerEntry.name  | translate}}">' +
'                </th>' +
'            </tr>' +
'        </thead>' +
'        <tfoot ng-if="options.aggregate.length">' +
'            <tr ng-repeat="aggregator in options.aggregate track by $index" style="background-color: {{aggregator.bgColor}}">' +
'                <th translate>{{aggregator.name}}</th>' +
'                <th ng-repeat="headerEntry in header track by $index">' +
'                    <div ng-if="headerEntry.aggregatable[$parent.$index]">{{$ctrl.aggregate($index, aggregator.aggregationFunction)}}</div>' +
'                </th>' +
'            </tr>' +
'        </tfoot>' +
'        <tbody>' +
'            <tr dir-paginate-start="row in global.data | itemsPerPage: options.paginate track by $index" current-page="currentPageNumber" ng-show="row.preview">' +
'                <td rowspan="2">' +
'                    <div class="fa-buttons" ng-show="row.visible">' +
'                        <div ng-click="$ctrl.edit($ctrl.paginatedIndex + $index, \'update\')" style="color: #449d44">' +
'                            <i class="fa fa-save" aria-hidden="true"></i>' +
'                        </div>' +
'                        <div ng-click="$ctrl.edit($ctrl.paginatedIndex + $index, \'cancel\')" style="color: #025aa5">' +
'                            <i class="fa fa-ban" aria-hidden="true"></i>' +
'                        </div>' +
'                    </div>' +
'                </td>' +
'                <td ng-repeat="cell in row.columns track by $index" ng-class="{red: row.preview}">' +
'                    <div ng-if="cell.options.displayAs !== 0">' +
'                        <bitcraft-db-edition-simple-type type="cell.options.displayAs" value="row.oldValue[$index]" edit="false"></bitcraft-db-edition-simple-type>' +
'                    </div>' +
'                </td>' +
'            </tr>' +
'            <tr dir-paginate-end>' +
'                <td ng-if="!row.preview" ng-show="options.edit || options.del || options.detailedView || options.aggregate.length" style="white-space: nowrap">' +
'                    <!--form -->' +
'                    <div class="fa-buttons" ng-show="row.visible && !row.preview">' +
'                        <div ng-click="$ctrl.edit($ctrl.paginatedIndex + $index, \'update\')" style="color: #449d44">' +
'                            <i class="fa fa-save" aria-hidden="true"></i>' +
'                        </div>' +
'                        <div ng-click="$ctrl.edit($ctrl.paginatedIndex + $index, \'cancel\')" style="color: #025aa5">' +
'                            <i class="fa fa-ban" aria-hidden="true"></i>' +
'                        </div>' +
'                    </div>' +
'                    <div class="fa-buttons" ng-show="!row.visible">' +
'                        <div ng-if="options.detailedView" ng-click="$ctrl.seeDetails($ctrl.paginatedIndex + $index)" style="color: #449d44">' +
'                            <i class="fa fa-search-plus" aria-hidden="true"></i>' +
'                        </div>' +
'                        <div ng-if="options.edit" ng-click="$ctrl.edit($ctrl.paginatedIndex + $index, \'show\')" style="color: #025aa5">' +
'                            <i class="fa fa-edit" aria-hidden="true"></i>' +
'                        </div>' +
'                        <div ng-if="options.del" ng-click="$ctrl.delete($ctrl.paginatedIndex + $index)" style="color: #c9302c">' +
'                            <i class="fa fa-times" aria-hidden="true"></i>' +
'                        </div>' +
'                    </div>' +
'                </td>' +
'                <td ng-repeat="cell in row.columns track by $index" ng-class="{green: row.preview}">' +
'                    <div style="display:block" ng-if="cell.options.displayAs === 0">' +
'                        <bitcraft-db-edition-component-wrapper data="{row: cell.value, component: cell.options.accessor[0].accessor}"></bitcraft-db-edition-component-wrapper>' +
'                    </div>' +
'                    <div ng-if="cell.options.displayAs !== 0">' +
'                        <bitcraft-db-edition-simple-type type="cell.options.displayAs" value="cell.value" values="$ctrl.getValues(cell.options)" edit="row.visible && !row.preview"></bitcraft-db-edition-simple-type>' +
'                    </div>' +
'                </td>' +
'            </tr>' +
'        </tbody>' +
'    </table>' +
'    <div ng-if="options.add" class="buttons">' +
'        <button class="btn btn-primary btn-block" ng-click="$ctrl.add()" translate>DATABASE_EDITION.ADD</button>' +
'    </div>' +
'</div>' +
'';


function saveOldData(row) {
    var i;

    if (row.oldValue) {
        return;
    }

    row.oldValue = [];

    for (i = 0; i < row.columns.length; i = i + 1) {
        row.oldValue.push(angular.copy(row.columns[i].value));
    }
}

function restoreOldData(row) {
    var i;

    for (i = 0; i < row.columns.length; i = i + 1) {
        row.columns[i].value = angular.copy(row.oldValue[i]);
    }
}

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition')
    .component('bitcraftDbEditionGlobal', {
        template: template,
        controller: [
            '$scope', '$compile', '$state', '$stateParams', '$uibModal', 'Notification', 'DBEdition',
            function ($scope, $compile, $state, $stateParams, $uibModal, Notification, DBEdition) {
                function initializeComponents() {
                    var i;
                    var component, compiledComponent;
                    var globalView = $('#db-edition-global-view');

                    for (i = 0; i < $scope.command.components.length; i = i + 1) {
                        component = $scope.command.components[i];
                        if (component.view === 1 || component.view === 3) {
                            compiledComponent = $compile('<' + component.name + '></' + component.name + '>')($scope.$new(false, $scope));

                            if (component.anchorToTop) {
                                globalView.prepend(compiledComponent);
                            } else {
                                globalView.append(compiledComponent);
                            }
                        }
                    }

                    $scope.done = true;
                }

                // update data based on the new state
                function updateDisplayedData(next) {
                    DBEdition.retrieveData($scope.command.backend, $scope.command.name, null, null, $scope.currentPageNumber - 1, $scope.sortObj, $scope.searchObj, $scope.command.params).then(function (data) {
                        $scope.global = data;

                        if ($scope.searchObj.searchInput !== '') {
                            $scope.header[$scope.searchObj.index].searchInput = $scope.searchObj.searchInput;
                        }

                        if ($scope.sortObj.sortOn !== '') {
                            $scope.header[$scope.sortObj.index].sortClass = $scope.sortObj.reverse ? 'sort-descendant' : 'sort-ascendant';
                        }

                        if (next) {
                            next();
                        }
                    }, function (err) {
                        Notification.error({message: 'Failed to retrieve data (' + err.statusText + ').'});
                    });
                }

                var self = this;

                this.$onInit = function () {
                    /** @type{Command} */
                    $scope.command = DBEdition.getCommandFromName($stateParams.command);
                    if ($scope.command === null) { // TODO
                        console.error('Failed to retrieve command');
                        return;
                    }

                    // edition
                    $scope.currentlyVisible = null;

                    // pagination
                    $scope.currentPageNumber = 1;

                    // sorting
                    $scope.sortObj = {
                        sortOn: '',
                        reverse: true,
                        index: 0
                    };

                    // searching
                    $scope.searchObj = {
                        searchOn: '',
                        searchInput: '',
                        index: 0
                    };
                    $scope.searchDebounce = null;

                    $scope.header = $scope.command.getGlobalHeader();
                    $scope.options = $scope.command.getOptions();

                    self.paginatedIndex = 0;

                    if ($scope.command.disableInitialDataFetch) {
                        initializeComponents();
                    } else {
                        updateDisplayedData(initializeComponents);
                    }
                };

                // detailedView
                self.seeDetails = function (index) {
                    $state.go('.id', {id: $scope.global.data[index]._id}, {location: true});
                };

                // searching
                function executeSearch(index, searchValue) {
                    $scope.searchDebounce = null;

                    var searchKey = $scope.header[index].options.getAccessor();

                    if (searchKey === $scope.searchObj.searchOn) {
                        $scope.searchObj.searchInput = searchValue;
                    } else {
                        $scope.searchObj.searchOn = searchKey;
                        $scope.searchObj.searchInput = searchValue;
                        $scope.searchObj.index = index;
                    }

                    updateDisplayedData();
                }

                self.log = function (object) {
                    console.log(object);
                };

                self.search = function (index) {
                    if ($scope.searchDebounce !== null) {
                        clearTimeout($scope.searchDebounce);
                        $scope.searchDebounce = null;
                    }

                    $scope.searchDebounce = setTimeout(function () {
                        executeSearch(index, $scope.header[index].searchInput);
                    }, 200);
                };

                // sorting
                self.sort = function (index) {
                    var sortKey = $scope.header[index].options.getAccessor();

                    if (sortKey === $scope.sortObj.sortOn) {
                        $scope.sortObj.reverse = !$scope.sortObj.reverse;
                    } else {
                        $scope.sortObj.sortOn = sortKey;
                        $scope.sortObj.reverse = false;
                        $scope.sortObj.index = index;
                    }

                    updateDisplayedData();
                };

                // pagination
                self.changePage = function (newPageNumber) {
                    self.paginatedIndex = $scope.options.paginate * (newPageNumber - 1);
                    updateDisplayedData();
                };

                // deletion
                self.delete = function (index) {
                    DBEdition.deleteData($scope.command.backend, $scope.command.collection, $scope.global.data[index]._id, $scope.command.id, $scope.command.params).then(function () {
                        $scope.global.data.splice(index, 1);
                    }, function (err) {
                        Notification.error({message: 'Failed to delete the entry (' + err.statusText + ').'});
                    });
                };

                // edition
                var showFunction = function (index, cb) {
                    DBEdition.lockData($scope.command.backend, $scope.command.collection, $scope.global.data[index]._id, $scope.command.params).then(function () {
                        saveOldData($scope.global.data[index]);
                        $scope.global.data[index].visible = true;
                        $scope.currentlyVisible = index;

                        if (cb) {
                            cb();
                        }
                    }, function (err) {
                        Notification.error({message: 'Failed to lock the entry (' + err.statusText + ').'});
                    });
                };

                var cancelFunction = function (index, cb) {
                    DBEdition.unlockData($scope.command.backend, $scope.command.params).then(function () {
                        $scope.global.data[index].visible = false;
                        $scope.global.data[index].preview = false;
                        restoreOldData($scope.global.data[index]);
                        $scope.currentlyVisible = null;

                        if (cb) {
                            cb();
                        }
                    }, function (err) {
                        Notification.error({message: 'Failed to unlock the entry (' + err.statusText + ').'});
                    });
                };

                var updateFunction = function (index, cb) {
                    if ($scope.command.confirmEdit && !$scope.global.data[index].preview) {
                        $scope.global.data[index].preview = true;
                    } else {
                        DBEdition.updateData($scope.command.backend, $scope.command.collection, $scope.global.data[index]._id, $scope.command.id, $scope.global.data[index].columns, $scope.command.params).then(function () {
                            $scope.global.data[index].visible = false;
                            $scope.global.data[index].preview = false;
                            $scope.global.data[index].oldValue = null;
                            $scope.currentlyVisible = null;

                            if (cb) {
                                cb($scope.command.backend, $scope.command.params);
                            }
                        }, function (err) {
                            Notification.error({message: 'Failed to update the entry (' + err.statusText + ').'});
                        });
                    }
                };

                self.edit = function (index, action) {
                    switch (action) {
                    case 'show':
                        if ($scope.currentlyVisible !== null) {
                            cancelFunction($scope.currentlyVisible, function () {
                                showFunction(index);
                            });
                        } else {
                            showFunction(index);
                        }
                        break;
                    case 'cancel':
                        cancelFunction(index);
                        break;
                    case 'update':
                        updateFunction(index, DBEdition.unlockData);
                        break;
                    default:
                        throw new Error('Unknown action. (' + action + ')');
                    }
                };

                // aggregation
                self.aggregate = function (index, func) { // here index is the column
                    var i;
                    var res = null;
                    var totalElements = $scope.global.data.length;

                    for (i = 0; i < totalElements; i = i + 1) {
                        res = func(res, $scope.global.data[i].columns[index].value, i, totalElements - (i + 1), totalElements);
                    }

                    return res;
                };

                // addition
                self.add = function () {
                    $state.go('.add', {location: true});
                };

                // select/dropdown
                self.getValues = function (options) {
                    if (options.values) {
                        return options.values;
                    }
                    if (options.$name) {
                        console.log('TODO');
                    }
                };

                self.updateDisplayedData = updateDisplayedData;
            }
        ]
    });

},{}],6:[function(require,module,exports){
/**
 * Created by richard on 20/10/16.
 */

'use strict';
require('./db-edition-global.component');

},{"./db-edition-global.component":5}],7:[function(require,module,exports){
'use strict';

var template = '' +
'<style>' +
'    .fa-buttons > div {' +
'        display: inline-block;' +
'        cursor: pointer;' +
'        font-size: 2em;' +
'        margin-top: -5px;' +
'        margin-bottom: -5px;' +
'        padding-right: 3px;' +
'        padding-left: 3px;' +
'    }' +
'    .no-padding {' +
'        padding: 0 !important;' +
'    }' +
'    .innertable {' +
'        margin: -1px;' +
'        width: calc(100% + 2px);' +
'        max-width: calc(100% + 2px);' +
'    }' +
'    .red {' +
'        background-color: #a70f0f;' +
'    }' +
'    .green {' +
'        background-color: #04941c;' +
'    }' +
'    td {' +
'        height: 37px;' +
'    }' +
'</style>' +
'<script type="text/ng-template" id="table_renderer_preview.html">' +
'    <table class="table table-bordered table-hover innertable">' +
'        <!--row in pass.value track by $index -->' +
'        <tr ng-init="row = pass.value[index]" ng-repeat="index in $ctrl.forLoop(0, global.oldValue[key].length > pass.value.length ? global.oldValue[key].length : global.oldValue[key].length)">' +
'            <th ng-if="row && !row.options.hideKey" translate>{{row.options.name}}</th>' +
'            <td ng-if="row && !row.options.isLeaf" class="no-padding" ng-class="{red: global.preview}"><div ng-repeat="(key, pass) in $ctrl.makeObject(row, index)" ng-include="\'table_renderer_object.html\'"></div></td>' +
'            <td ng-if="!row || row.options.isLeaf" ng-class="{red: global.preview}">' +
'                <div>' +
'                    <bitcraft-db-edition-simple-type type="global.oldValue[key][index].options.displayAs" value="global.oldValue[key][index].value" edit="false"></bitcraft-db-edition-simple-type>' +
'                </div>' +
'            </td>' +
'        </tr>' +
'    </table>' +
'</script>' +
'<script type="text/ng-template" id="table_renderer_object.html">' +
'    <table class="table table-bordered table-hover innertable">' +
'        <tr ng-repeat="row in pass.value track by $index">' +
'            <th ng-if="!row.options.hideKey" translate>{{row.options.name}}</th>' +
'            <td ng-if="!row.options.isLeaf" class="no-padding" ng-class="{green: global.preview}"><div ng-repeat="(key, pass) in $ctrl.makeObject(row, $index)" ng-include="\'table_renderer_object.html\'"></div></td>' +
'            <td ng-if="row.options.isLeaf" ng-class="{green: global.preview}">' +
'                <div style="display:block" ng-if="row.options.displayAs === 0">' +
'                    <bitcraft-db-edition-component-wrapper data="{row: row.value, component: row.options.accessor[0].accessor}"></bitcraft-db-edition-component-wrapper>' +
'                </div>' +
'                <div ng-if="row.options.displayAs !== 0">' +
'                    <bitcraft-db-edition-simple-type type="row.options.displayAs" value="row.value" values="$ctrl.getValues(row.options)" edit="global.visible && !global.preview"></bitcraft-db-edition-simple-type>' +
'                </div>' +
'            </td>' +
'        </tr>' +
'        <tr ng-if="row.options.extensible && global.visible && !global.preview">' +
'            <td>' +
'                <button class="btn btn-primary" ng-click="$ctrl.addRow(pass);"><i class="fa fa-plus" aria-hidden="true"></i></button>' +
'                <button class="btn btn-danger" ng-click="$ctrl.delRow(pass);"><i class="fa fa-minus" aria-hidden="true"></i></button>' +
'            </td>' +
'        </tr>' +
'    </table>' +
'</script>' +
'<div ng-show="done" id="db-edition-detailed-view">' +
'    <h3 ng-if="global.options.detailedTitle">{{global.options.detailedTitle | translate}}</h3>' +
'    <table class="table table-bordered table-hover">' +
'        <tr ng-repeat="row in global.data track by $index">' +
'            <th translate>{{row.options.name}}</th>' +
'            <td ng-if="!row.options.isLeaf && global.preview" class="no-padding" ng-class="{red: global.preview}"><div ng-repeat="(key, pass) in $ctrl.makeObject(row, $index)" ng-include="\'table_renderer_preview.html\'"></div></td>' +
'            <td ng-if="row.options.isLeaf && global.preview" ng-class="{red: global.preview}">' +
'                <div ng-if="row.options.displayAs !== 0">' +
'                    <bitcraft-db-edition-simple-type type="row.options.displayAs" value="global.oldValue[$index]" edit="false"></bitcraft-db-edition-simple-type>' +
'                </div>' +
'            </td>' +
'            <td ng-if="!row.options.isLeaf" class="no-padding" ng-class="{green: global.preview}"><div ng-repeat="(key, pass) in $ctrl.makeObject(row, $index)" ng-include="\'table_renderer_object.html\'"></div></td>' +
'            <td ng-if="row.options.isLeaf" ng-class="{green: global.preview}">' +
'                <div style="display:block" ng-if="row.options.displayAs === 0">' +
'                    <bitcraft-db-edition-component-wrapper data="{row: row.value, component: row.options.accessor[0].accessor}"></bitcraft-db-edition-component-wrapper>' +
'                </div>' +
'                <div ng-if="row.options.displayAs !== 0">' +
'                    <bitcraft-db-edition-simple-type type="row.options.displayAs" value="row.value" values="$ctrl.getValues(row.options)" edit="global.visible && !global.preview"></bitcraft-db-edition-simple-type>' +
'                </div>' +
'            </td>' +
'        </tr>' +
'        <tr>' +
'            <td style="white-space: nowrap; text-align: center;" colspan="{{global.preview ? 3 : 2}}">' +
'                <!-- form -->' +
'                <div class="fa-buttons" ng-show="global.visible">' +
'                    <div ng-click="$ctrl.edit(\'update\')" style="color: #449d44">' +
'                        <i class="fa fa-save" aria-hidden="true"></i>' +
'                    </div>' +
'                    <div ng-click="$ctrl.edit(\'cancel\')" style="color: #025aa5">' +
'                        <i class="fa fa-ban" aria-hidden="true"></i>' +
'                    </div>' +
'                </div>' +
'                <div class="fa-buttons" ng-show="!global.visible">' +
'                    <div ng-click="$ctrl.seeGlobal()" style="color: #449d44">' +
'                        <i class="fa fa-search-minus" aria-hidden="true"></i>' +
'                    </div>' +
'                    <div ng-if="global.options.edit" ng-click="$ctrl.edit(\'show\')" style="color: #025aa5">' +
'                        <i class="fa fa-edit" aria-hidden="true"></i>' +
'                    </div>' +
'                    <div ng-if="global.options.del" ng-click="$ctrl.delete()" style="color: #c9302c">' +
'                        <i class="fa fa-times" aria-hidden="true"></i>' +
'                    </div>' +
'                </div>' +
'            </td>' +
'        </tr>' +
'    </table>' +
'</div>' +
'';

/*global $, angular*/

function saveOldData(row) {
    var i;

    if (row.oldValue) {
        return;
    }

    row.oldValue = [];

    for (i = 0; i < row.data.length; i = i + 1) {
        row.oldValue.push(angular.copy(row.data[i].value));
    }
}

function restoreOldData(row) {
    var i;

    for (i = 0; i < row.data.length; i = i + 1) {
        row.data[i].value = angular.copy(row.oldValue[i]);
    }
}

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition')
    .component('bitcraftDbEditionId', {
        template: template,
        controller: [
            '$scope', '$state', '$stateParams', '$compile', 'Notification', 'DBEdition',
            function ($scope, $state, $stateParams, $compile, Notification, DBEdition) {
                function initializeComponents() {
                    var i;
                    var component, compiledComponent;
                    var globalView = $('#db-edition-detailed-view');

                    for (i = 0; i < $scope.command.components.length; i = i + 1) {
                        component = $scope.command.components[i];
                        if (component.view === 2 || component.view === 3) {
                            compiledComponent = $compile('<' + component.name + '></' + component.name + '>')($scope.$new(false, $scope));

                            if (component.anchorToTop) {
                                globalView.prepend(compiledComponent);
                            } else {
                                globalView.append(compiledComponent);
                            }
                        }
                    }

                    $scope.done = true;
                }

                // update data based on the new state
                function updateDisplayedData() {
                    DBEdition.retrieveData($scope.command.backend, $scope.command.name, $stateParams.id, $scope.command.id, undefined, undefined, undefined, $scope.command.params).then(function (data) {
                        $scope.global = data;
                        $scope.done = true;
                        console.log(data);
                        initializeComponents();
                    }, function (err) {
                        Notification.error({message: 'Failed to retrieve data (' + err.statusText + ').'});
                    });
                }

                this.$onInit = function () {
                    /** @type{Command} */
                    $scope.command = DBEdition.getCommandFromName($stateParams.command);
                    if ($scope.command === null) { // TODO
                        console.error('Failed to retrieve command');
                        return;
                    }

                    // TODO go to seeGlobal if the config does not let us see the detailed view
                    // edition
                    $scope.currentlyVisible = null;

                    updateDisplayedData();
                };

                var self = this;

                // globalView
                self.seeGlobal = function () {
                    $state.go('^');
                };

                // deletion
                self.delete = function () {
                    DBEdition.deleteData($scope.command.backend, $scope.command.collection, $stateParams.id, $scope.command.id, $scope.command.params).then(function () {
                        self.seeGlobal();
                    }, function (err) {
                        Notification.error({message: 'Failed to delete the entry (' + err.statusText + ').'});
                    });
                };

                // edition
                var showFunction = function (cb) {
                    DBEdition.lockData($scope.command.backend, $scope.command.collection, $stateParams.id, $scope.command.params).then(function () {
                        saveOldData($scope.global);
                        $scope.global.visible = true;

                        if (cb) {
                            cb();
                        }
                    }, function (err) {
                        Notification.error({message: 'Failed to lock the entry (' + err.statusText + ').'});
                    });
                };

                var cancelFunction = function (cb) {
                    DBEdition.unlockData($scope.command.backend, $scope.command.params).then(function () {
                        $scope.global.visible = false;
                        $scope.global.preview = false;
                        restoreOldData($scope.global);

                        if (cb) {
                            cb();
                        }
                    }, function (err) {
                        Notification.error({message: 'Failed to unlock the entry (' + err.statusText + ').'});
                    });
                };

                var updateFunction = function (cb) {
                    if ($scope.command.confirmEdit && !$scope.global.preview) {
                        $scope.global.preview = true;
                    } else {
                        DBEdition.updateData($scope.command.backend, $scope.command.collection, $stateParams.id, $scope.command.id, $scope.global.data, $scope.command.params).then(function () {
                            $scope.global.visible = false;
                            $scope.global.preview = false;
                            $scope.global.oldValue = null;

                            if (cb) {
                                cb($scope.command.backend, $scope.command.params);
                            }
                        }, function (err) {
                            Notification.error({message: 'Failed to update the entry (' + err.statusText + ').'});
                        });
                    }
                };

                self.edit = function (action) {
                    switch (action) {
                    case 'show':
                        showFunction();
                        break;
                    case 'cancel':
                        cancelFunction();
                        break;
                    case 'update':
                        updateFunction(DBEdition.unlockData);
                        break;
                    default:
                        throw new Error('Unknown action. (' + action + ')');
                    }
                };

                // aggregation
                self.aggregate = function (index, func) { // here index is the column
                    var i;
                    var res = null;
                    var totalElements = $scope.global.data.length;

                    for (i = 0; i < totalElements; i = i + 1) {
                        res = func(res, $scope.global.data[i].columns[index].value, i, totalElements - (i + 1), totalElements);
                    }

                    return res;
                };

                // select/dropdown
                self.getValues = function (options) {
                    if (options.values) {
                        return options.getValues();
                    }
                };

                self.isRepeater = function (options) {
                    return options.accessor[0].accessor === '$';
                };

                self.makeObject = function (value, key1) {
                    var res = {};
                    res[key1] = value;
                    return res;
                };

                self.addRow = function (to) {
                    if (to.options.saveAs === 5) {
                        to.value.push(to.options.displayAs[0].getDefaultValue(null, to.value.length));
                    } else {
                        console.error('Not implemented yet !');
                    }
                };

                self.delRow = function (from) {
                    from.value.splice(-1, 1);
                };

                self.forLoop = function (start, limit, step) {
                    var i;
                    var res = [];
                    step = step || 1;
                    for (i = start; i < limit; i += step) {
                        res.push(i);
                    }
                    return res;
                };
            }
        ]
    });

},{}],8:[function(require,module,exports){
/**
 * Created by richard on 20/10/16.
 */

'use strict';
require('./db-edition-id.component');

},{"./db-edition-id.component":7}],9:[function(require,module,exports){
/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

var template = '' +
'<!--suppress ALL -->' +
'<div ng-switch on="$ctrl.type">' +
'    <div ng-switch-when="1">{{$ctrl.value}}</div>' +
'    <bitcraft-db-edition-text ng-switch-when="2" value="$ctrl.value" edit="$ctrl.edit"></bitcraft-db-edition-text>' +
'    <bitcraft-db-edition-number ng-switch-when="3" value="$ctrl.value" edit="$ctrl.edit"></bitcraft-db-edition-number>' +
'    <bitcraft-db-edition-checkbox ng-switch-when="4" value="$ctrl.value" edit="$ctrl.edit"></bitcraft-db-edition-checkbox>' +
'    <bitcraft-db-edition-checkboxes ng-switch-when="5" value="$ctrl.value" edit="$ctrl.edit"></bitcraft-db-edition-checkboxes>' +
'    <bitcraft-db-edition-date ng-switch-when="6" value="$ctrl.value" edit="$ctrl.edit"></bitcraft-db-edition-date>' +
'    <bitcraft-db-edition-image ng-switch-when="7" value="$ctrl.value" edit="$ctrl.edit"></bitcraft-db-edition-image>' +
'    <bitcraft-db-edition-blob-image ng-switch-when="8" value="$ctrl.value" edit="$ctrl.edit"></bitcraft-db-edition-blob-image>' +
'    <bitcraft-db-edition-select ng-switch-when="9" value="$ctrl.value" values="$ctrl.values" edit="$ctrl.edit"></bitcraft-db-edition-select>' +
'</div>' +
'';


//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition')
    .component('bitcraftDbEditionSimpleType', {
        template: template,
        bindings: {
            value: '=',
            values: '<',
            edit: '<',
            type: '<'
        }
    });

},{}],10:[function(require,module,exports){
/**
 * Created by richard on 19/10/16.
 */

'use strict';
require('./db-edition-simple-type.component.js');

},{"./db-edition-simple-type.component.js":9}],11:[function(require,module,exports){
/*global angular*/

angular.module('bitcraft-db-edition', [
    'angularUtils.directives.dirPagination',
    'ui.bootstrap',
    'ui.bootstrap.modal'
]);

},{}],12:[function(require,module,exports){
/*global angular*/
'use strict';

var libDBE =  require('./lib/db-edition-2.js').Configuration;
var libDBEHelpers = require('./lib/db-edition-2').Helper;

/**
 * @param {string} name of the cookie
 * @returns {string|null}
 */
function readCookie(name) {
    var nameEQ = name + "=";
    var cookies = document.cookie.split(';').map(function (s) { return s.replace(/\s/g, ''); });
    var i;
    for (i = 0; i < cookies.length; i += 1) {
        if (cookies[i].indexOf(nameEQ) === 0) {
            return cookies[i].substring(nameEQ.length);
        }
    }
    return null;
}

function decodeData(str) { //TODO handle mongoObjects WHEN 2.0 is working BY Richard
    var value = str.substr(2);
    if (value === '') {
        return undefined;
    }

    if (str[0] === 's') {
        return value;
    }
    if (str[0] === 'b') {
        if (value === 'true') {
            return true;
        }
        if (value === 'false') {
            return false;
        }
    }
    if (str[0] === 'n') {
        return Number(value);
    }
    if (str[0] === 'd') {
        return new Date(value);
    }
    if (str[0] === 'o') {
        return 'o_' + value;
    }

    return null;
}

function encodeData(data) {
    var type = typeof data;

    if (type === 'object') {
        if (data instanceof Date) {
            return 'd_' + data;
        }
    }

    if (type === 'number' || data instanceof Number) {
        return 'n_' + data;
    }

    if (type === 'boolean' || data instanceof Boolean) {
        return 'b_' + data;
    }

    if (type === 'string' || data instanceof String) {
        if (data[0] === 'o' && data[1] === '_') {
            return data;
        }

        return 's_' + data;
    }
}

function decodeObject(obj) {
    var key;
    var tmpObj;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            tmpObj = obj[key];
            if (typeof tmpObj === 'string' || tmpObj instanceof String) {
                obj[key] = decodeData(tmpObj);
            } else {
                decodeObject(tmpObj);
            }
        }
    }

    return obj;
}

function encodeObject(obj) {
    var key;
    var tmpObj;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            tmpObj = obj[key];
            if (typeof tmpObj === 'object' && !(tmpObj instanceof Date)) {
                encodeObject(tmpObj);
            } else {
                obj[key] = encodeData(tmpObj);
            }
        }
    }

    return obj;
}

function padResponse(array, totalElements, from, to) {
    var res = [];
    var i = 0;

    while (i < from) {
        res.push(null);
        i = i + 1;
    }

    while (i < to && i < totalElements) {
        res.push(array[i - from]);
        i = i + 1;
    }

    while (i < totalElements) {
        res.push(null);
        i = i + 1;
    }

    return res;
}

//noinspection JSUnresolvedFunction
angular.module('bitcraft-db-edition')
    .factory('DBEdition', [
        '$http', '$q',
        function ($http, $q) {
            var config = null;

            /**
             * @param {string} backend
             * @param {string} api
             * @returns {string}
             */
            function getUrl(backend, api) {
                return './rest/' + backend + '/' + api;
            }

            function getTypeValues($name) {
                var i;

                for (i = 0; i < config.types.length; i = i + 1) {
                    if (config.types[i].name === $name) {
                        return config.types[i].values;
                    }
                }

                return null;
            }

            function parseConfiguration(configuration) {
                config = libDBE.parseConfigurationObject(configuration);
                config.setServices($q, this);
            }

            /**
             * @param {string} commandName
             * @returns {Command}
             */
            function getCommandFromName(commandName) {
                /** @type{number} */
                var i;

                if (config === null) {
                    throw new Error('No configuration was provided, or the parsing failed.');
                }

                for (i = 0; i < config.commands.length; i = i + 1) {
                    if (config.commands[i].name === commandName) {
                        return config.commands[i];
                    }
                }

                return null;
            }

            function getDefaultData(command) {
                var deferred = $q.defer();
                var i;
                var currentCommand = null;

                if (config === null) {
                    throw new Error('No configuration was provided, or the parsing failed.');
                }

                for (i = 0; i < config.commands.length; i = i + 1) {
                    if (config.commands[i].name === command) {
                        currentCommand = config.commands[i];
                        break;
                    }
                }

                if (currentCommand === null) {
                    throw new Error('Unknown command (' + command + ').');
                }

                currentCommand.getDefaultData(function (err, data) {
                    if (err) {
                        deferred.reject(err);
                    } else {
                        deferred.resolve(data);
                    }
                });

                return deferred.promise;
            }

            function getData(backend, collection, filterField, filterValue, params) {
                var deferred = $q.defer();

                $http.post(getUrl(backend, 'getData'), encodeObject({
                    collection: collection,
                    filterField: filterField,
                    filterValue: filterValue,
                    params: angular.copy(params)
                })).then(function successCallback(res) {
                    deferred.resolve(decodeObject(res.data));
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function countData(backend, collection, params) {
                var deferred = $q.defer();

                $http.post(getUrl(backend, 'countData'), encodeObject({
                    collection: collection,
                    params: angular.copy(params)
                })).then(function successCallback(res) {
                    deferred.resolve(decodeObject(res.data));
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function retrieveData(backend, command, id, idName, page, sortObj, searchObj, params) {
                var deferred = $q.defer();
                var i;
                var currentCommand = null;
                var apiConfiguration = {};

                if (config === null) {
                    throw new Error('No configuration was provided, or the parsing failed.');
                }

                for (i = 0; i < config.commands.length; i = i + 1) {
                    if (config.commands[i].name === command) {
                        currentCommand = config.commands[i];
                        break;
                    }
                }

                if (currentCommand === null) {
                    throw new Error('Unknown command (' + command + ').');
                }

                if (id) {
                    apiConfiguration._id = id;
                }

                if (idName) {
                    apiConfiguration.idName = idName;
                }

                if (page !== undefined && page !== null && currentCommand.paginate !== 4294967295) {
                    apiConfiguration.from = page * currentCommand.paginate;
                    apiConfiguration.to = (page + 1) * currentCommand.paginate;
                }

                if (sortObj !== undefined && sortObj !== null && sortObj.sortOn !== '') {
                    apiConfiguration.sortOn = sortObj.sortOn;
                    apiConfiguration.reverse = sortObj.reverse;
                }

                if (searchObj !== undefined && searchObj !== null && searchObj.searchInput !== '') {
                    apiConfiguration.searchOn = searchObj.searchOn;
                    apiConfiguration.searchInput = searchObj.searchInput;
                }

                $http.post(getUrl(backend, 'retrieveData'), encodeObject({
                    collection: currentCommand.collection,
                    configuration: apiConfiguration,
                    params: angular.copy(params)
                })).then(function successCallback(res) {
                    currentCommand.parseData(decodeObject(res.data.data), id, function (err, resData) {
                        if (err) {
                            deferred.reject(err);
                            return;
                        }

                        // if (apiConfiguration.from !== undefined) {
                        //     $http.post(getUrl(backend, 'countData'), encodeObject({
                        //         collection: currentCommand.collection,
                        //         params: angular.copy(params)
                        //     })).then(function successCallback(res) {
                        //         var count = decodeData(res.data.count);
                        //         resData.data = padResponse(resData.data, count, page * currentCommand.paginate, (page + 1) * currentCommand.paginate);
                        //         deferred.resolve(resData);
                        //     }, function errorCallback(resp) {
                        //         deferred.reject(resp);
                        //     });
                        //     return;
                        // }

                        deferred.resolve(resData);
                    });
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function deleteData(backend, collection, id, idName, params) {
                var deferred = $q.defer();
                var apiConfiguration = {};

                if (!collection || id === undefined) {
                    throw new Error('No collection, or no id was provided.');
                }

                apiConfiguration._id = id;
                if (idName) {
                    apiConfiguration.idName = idName;
                }

                $http.post(getUrl(backend, 'deleteData'), encodeObject({
                    collection: collection,
                    configuration: apiConfiguration,
                    params: angular.copy(params)
                })).then(function successCallback() {
                    deferred.resolve();
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function updateData(backend, collection, _id, id, updatedValues, params) {
                var deferred = $q.defer();
                var i;
                var updateObjectArray = [];

                if (!collection || !id) {
                    throw new Error('No collection, or no id was provided.');
                }

                var cookie = readCookie('lockToken');
                if (cookie === null) {
                    throw new Error('No lock cookie is available.');
                }

                updateObjectArray.push({
                    collection: collection,
                    _id: _id,
                    id: id,
                    updateObject: {}
                });

                for (i = 0; i < updatedValues.length; i = i + 1) {
                    updatedValues[i].options.addValueToUpdateObject(updateObjectArray, angular.copy(updatedValues[i].value));
                }

                i = 0;
                libDBEHelpers.whileAsync( // TODO handle the case we need to revert an operation WHEN 2.0 is working BY Richard
                    function () {
                        return i < updateObjectArray.length;
                    },
                    function (next) {
                        if (Object.keys(updateObjectArray[i].updateObject).length === 0) {
                            i += 1;
                            next();
                            return;
                        }
                        $http.post(getUrl(backend, 'updateData'), encodeObject({
                            collection: updateObjectArray[i].collection,
                            _id: updateObjectArray[i]._id,
                            cookie: cookie,
                            updateObject: updateObjectArray[i].updateObject,
                            id: updateObjectArray[i].id,
                            params: angular.copy(params)
                        })).then(function () {
                            i += 1;
                            next();
                        }, function (resp) {
                            next(resp);
                        });
                    },
                    function (err) {
                        if (err) {
                            deferred.reject(err);
                            return;
                        }
                        deferred.resolve();
                    }
                );

                return deferred.promise;
            }

            function insertData(backend, collection, object, params) {
                var deferred = $q.defer();
                var i;
                var insertObject = {};

                for (i = 0; i < object.length; i = i + 1) {
                    if (object[i].value !== undefined) { // filter unset fields
                        object[i].options.addValueToInsertObject(insertObject, object[i].value);
                    }
                }

                //noinspection JSUnresolvedFunction
                $http.post(getUrl(backend, 'insertData'), encodeObject({
                    collection: collection,
                    object: insertObject,
                    params: angular.copy(params)
                })).then(function successCallback() {
                    deferred.resolve();
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function lockData(backend, collection, id, params) {
                var deferred = $q.defer();
                var cookie = readCookie('lockToken');

                $http.post(getUrl(backend, 'lockData'), encodeObject({
                    collection: collection,
                    _id: id,
                    cookie: cookie,
                    params: angular.copy(params)
                })).then(function successCallback(res) {
                    res = decodeObject(res);
                    document.cookie = 'lockToken=' + res.data.cookie;
                    deferred.resolve(res.data.length);
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function unlockData(backend, params) {
                var deferred = $q.defer();
                var cookie = readCookie('lockToken');

                $http.post(getUrl(backend, 'unlockData'), encodeObject({
                    cookie: cookie,
                    params: angular.copy(params)
                })).then(function successCallback() {
                    document.cookie = 'lockToken=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    deferred.resolve();
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            return {
                getTypeValues: getTypeValues,
                parseConfiguration: parseConfiguration,
                getCommandFromName: getCommandFromName,
                getDefaultData: getDefaultData,
                getData: getData,
                countData: countData,
                retrieveData: retrieveData,
                deleteData: deleteData,
                updateData: updateData,
                insertData: insertData,
                lockData: lockData,
                unlockData: unlockData
            };
        }
    ]);

},{"./lib/db-edition-2":14,"./lib/db-edition-2.js":14}],13:[function(require,module,exports){
/**
 * Created by richard on 19/10/16.
 */

'use strict';
require('./db-edition.module.js');
require('./db-edition.service.js');
require('./db-edition-global');
require('./db-edition-id');
require('./db-edition-add');
require('./db-edition-simple-type');
require('./db-edition-component-wrapper');

},{"./db-edition-add":2,"./db-edition-component-wrapper":4,"./db-edition-global":6,"./db-edition-id":8,"./db-edition-simple-type":10,"./db-edition.module.js":11,"./db-edition.service.js":12}],14:[function(require,module,exports){
/**
 * Created by richard on 20/12/16.
 */
//declare function require(name: string) : any;
//require('source-map-support').install();
Object.defineProperty(exports, "__esModule", { value: true });
var DisplayType;
(function (DisplayType) {
    DisplayType[DisplayType["Component"] = 0] = "Component";
    DisplayType[DisplayType["Readonly"] = 1] = "Readonly";
    DisplayType[DisplayType["Text"] = 2] = "Text";
    DisplayType[DisplayType["Number"] = 3] = "Number";
    DisplayType[DisplayType["Checkbox"] = 4] = "Checkbox";
    DisplayType[DisplayType["Checkboxes"] = 5] = "Checkboxes";
    DisplayType[DisplayType["Date"] = 6] = "Date";
    DisplayType[DisplayType["Image"] = 7] = "Image";
    DisplayType[DisplayType["BlobImage"] = 8] = "BlobImage";
    DisplayType[DisplayType["Dropdown"] = 9] = "Dropdown";
})(DisplayType || (DisplayType = {}));
var DbType;
(function (DbType) {
    DbType[DbType["String"] = 0] = "String";
    DbType[DbType["Number"] = 1] = "Number";
    DbType[DbType["Date"] = 2] = "Date";
    DbType[DbType["ObjectId"] = 3] = "ObjectId";
    DbType[DbType["Boolean"] = 4] = "Boolean";
    DbType[DbType["Array"] = 5] = "Array";
    DbType[DbType["Map"] = 6] = "Map";
})(DbType || (DbType = {}));
var View;
(function (View) {
    View[View["Hidden"] = 0] = "Hidden";
    View[View["GlobalView"] = 1] = "GlobalView";
    View[View["DetailedView"] = 2] = "DetailedView";
    View[View["BothViews"] = 3] = "BothViews";
})(View || (View = {}));
var Helper = /** @class */ (function () {
    function Helper() {
    }
    Helper.capitalizeFirstLetter = function (variable) {
        return variable.charAt(0).toUpperCase() + variable.slice(1);
    };
    Helper.isUnset = function (variable) {
        return variable === null || variable === undefined || variable === "";
    };
    Helper.isMap = function (variable) {
        return Object.prototype.toString.call(variable) === "[object Object]";
    };
    Helper.isArray = function (variable) {
        return variable instanceof Array === true;
    };
    Helper.isFunction = function (variable) {
        return typeof variable === 'function';
    };
    Helper.isString = function (variable) {
        return typeof variable === 'string' || variable instanceof String;
    };
    Helper.isBoolean = function (variable) {
        return typeof variable === 'boolean' || variable instanceof Boolean;
    };
    Helper.isNumber = function (variable) {
        return typeof variable === 'number' || variable instanceof Number;
    };
    Helper.isDisplayType = function (variable) {
        if (this.isNumber(variable) && !this.isUnset(DisplayType[variable])) {
            return true;
        }
        else if (this.isString(variable)) {
            variable = this.capitalizeFirstLetter(variable);
            return variable in DisplayType;
        }
        else {
            return false;
        }
    };
    Helper.sanitizeMap = function (variable) {
        if (this.isMap(variable)) {
            return variable;
        }
        else {
            throw new TypeError('Expected object, found ' + typeof (variable) + '.');
        }
    };
    Helper.sanitizeArray = function (variable) {
        if (this.isArray(variable)) {
            return variable;
        }
        else {
            throw new TypeError('Expected array, found ' + typeof (variable) + '.');
        }
    };
    Helper.sanitizeFunction = function (variable) {
        if (this.isFunction(variable)) {
            return variable;
        }
        else {
            throw new TypeError('Expected function, found ' + typeof (variable) + '.');
        }
    };
    Helper.sanitizeString = function (variable) {
        if (this.isString(variable)) {
            return variable.valueOf();
        }
        else {
            throw new TypeError('Expected string, found ' + typeof (variable) + '.');
        }
    };
    Helper.sanitizeBoolean = function (variable) {
        if (this.isBoolean(variable)) {
            return variable.valueOf();
        }
        else {
            throw new TypeError('Expected boolean, found ' + typeof (variable) + '.');
        }
    };
    Helper.sanitizeNumber = function (variable) {
        if (this.isNumber(variable)) {
            return variable.valueOf();
        }
        else {
            throw new TypeError('Expected number, found ' + typeof (variable) + '.');
        }
    };
    Helper.sanitizeDisplayType = function (variable) {
        if (this.isNumber(variable)) {
            if (DisplayType[variable]) {
                return variable;
            }
            else {
                throw new TypeError('Unknown value (' + variable + ') for the enum DisplayType.');
            }
        }
        else if (this.isString(variable)) {
            variable = Helper.capitalizeFirstLetter(variable);
            if (variable in DisplayType) {
                return DisplayType[variable];
            }
            else {
                throw new TypeError('Unknown value (' + variable + ') for the enum DisplayType.');
            }
        }
        else {
            throw new TypeError('Expected a string or a number to parse the enum DisplayType but found ' + typeof variable + '.');
        }
    };
    Helper.sanitizeDbType = function (variable) {
        if (this.isNumber(variable)) {
            if (DbType[variable]) {
                return variable;
            }
            else {
                throw new TypeError('Unknown value (' + variable + ') for the enum DbType.');
            }
        }
        else if (this.isString(variable)) {
            variable = Helper.capitalizeFirstLetter(variable);
            if (variable in DbType) {
                return DbType[variable];
            }
            else {
                throw new TypeError('Unknown value (' + variable + ') for the enum DbType.');
            }
        }
        else {
            throw new TypeError('Expected a string or a number to parse the enum DbType but found ' + typeof variable + '.');
        }
    };
    Helper.sanitizeView = function (variable) {
        if (this.isNumber(variable)) {
            if (View[variable]) {
                return variable;
            }
            else {
                throw new TypeError('Unknown value (' + variable + ') for the enum View.');
            }
        }
        else if (this.isString(variable)) {
            variable = Helper.capitalizeFirstLetter(variable);
            if (variable in View) {
                return View[variable];
            }
            else {
                throw new TypeError('Unknown value (' + variable + ') for the enum View.');
            }
        }
        else {
            throw new TypeError('Expected a string or a number to parse the enum View but found ' + typeof variable + '.');
        }
    };
    Helper.passThrough = function (variable) {
        return variable;
    };
    Helper.joinAccessors = function (str1, str2) {
        if (!str1 || str1 === '')
            return str2;
        if (!str2 || str2 === '')
            return str1;
        return str1 + '.' + str2;
    };
    Helper.whileAsync = function (test, iterate, callback) {
        if (!test()) {
            callback(null);
        }
        else {
            var next = function (err) {
                if (err || !test()) {
                    callback(err);
                }
                else {
                    iterate(next);
                }
            };
            iterate(next);
        }
    };
    Helper.clone = function (originalObject, circular) {
        // First create an empty object with
        // same prototype of our original source
        var propertyIndex;
        var descriptor;
        var keys;
        var current;
        var nextSource;
        var indexOf;
        var copies = [{
                source: originalObject,
                target: Array.isArray(originalObject) ? [] : Object.create(Object.getPrototypeOf(originalObject))
            }];
        var cloneObject = copies[0].target;
        var sourceReferences = [originalObject];
        var targetReferences = [cloneObject];
        // First in, first out
        while (current = copies.shift()) { // jshint ignore:line
            keys = Object.getOwnPropertyNames(current.source);
            for (propertyIndex = 0; propertyIndex < keys.length; propertyIndex += 1) {
                // Save the source's descriptor
                descriptor = Object.getOwnPropertyDescriptor(current.source, keys[propertyIndex]);
                if (!descriptor.value || typeof descriptor.value !== 'object') {
                    Object.defineProperty(current.target, keys[propertyIndex], descriptor);
                    continue;
                }
                nextSource = descriptor.value;
                descriptor.value = Array.isArray(nextSource) ? [] : Object.create(Object.getPrototypeOf(nextSource));
                if (circular) {
                    indexOf = sourceReferences.indexOf(nextSource);
                    if (indexOf !== -1) {
                        // The source is already referenced, just assign reference
                        descriptor.value = targetReferences[indexOf];
                        Object.defineProperty(current.target, keys[propertyIndex], descriptor);
                        continue;
                    }
                    sourceReferences.push(nextSource);
                    targetReferences.push(descriptor.value);
                }
                Object.defineProperty(current.target, keys[propertyIndex], descriptor);
                copies.push({ source: nextSource, target: descriptor.value });
            }
        }
        return cloneObject;
    };
    return Helper;
}());
exports.Helper = Helper;
var Configuration = /** @class */ (function () {
    function Configuration() {
        this.commands = [];
        this.promiseService = null;
        this.DBEService = null;
        this.requestCache = {};
        this.$nameCache = {};
    }
    ;
    Configuration.prototype.getData = function (backend, collection, filterField, filterValue, params, callback) {
        var _this = this;
        if (this.DBEService === null || this.promiseService === null) {
            throw new Error('Get data was called but the services were not set.');
        }
        var cacheString = backend + '_' + collection + '_' + filterField + '_' + filterValue;
        if (this.requestCache[cacheString]) {
            callback(null, this.requestCache[cacheString]);
            return;
        }
        this.DBEService.getData(backend, collection, filterField, filterValue, params).then(function (data) {
            _this.requestCache[cacheString] = data;
            callback(null, data);
        }, function (err) {
            callback(err);
        });
    };
    Configuration.prototype.clearCache = function () {
        this.requestCache = {};
    };
    Configuration.prototype.retrieveData = function (backend, command, id, apiConfiguration) {
        if (this.DBEService === null || this.promiseService === null) {
            throw new Error('Retrieve data was called but the services were not set.');
        }
        var i;
        var d = this.promiseService.defer();
        for (i = 0; i < this.commands.length; i = i + 1) { // will break upon the first match
            if (this.commands[i].name === command) {
                this.DBEService.retrieveData(backend, command, id, apiConfiguration).then(function (data) {
                    //noinspection JSReferencingMutableVariableFromClosure
                    d.resolve(this.commands[i].parseData(data, id)); // accessible from closure, but ok => we break when we found it;
                }, function (err) {
                    d.reject(err);
                });
                return d.promise;
            }
        }
        d.reject(new Error('Command not found.'));
        return d.promise;
    };
    Configuration.prototype.setServices = function (promiseService, DBEService) {
        this.promiseService = promiseService;
        this.DBEService = DBEService;
    };
    Configuration.parseConfigurationObject = function (configurationObject) {
        if (Helper.isArray(configurationObject) === false) {
            throw new SyntaxError('Commands should be an array.');
        }
        var resConfiguration = new Configuration();
        var i;
        this.instance = resConfiguration;
        // parse commands
        for (i = 0; i < configurationObject.length; i = i + 1) {
            resConfiguration.commands.push(Command.parseCommandObject(configurationObject[i]));
        }
        return resConfiguration;
    };
    return Configuration;
}());
exports.Configuration = Configuration;
var Command = /** @class */ (function () {
    function Command(name, collection, backend) {
        this.name = name;
        this.collection = collection;
        this.backend = backend;
        this.dataNodes = [];
        this.components = [];
        this.aggregate = [];
        this.id = '_id';
        // initialize fields
        this.paginate = 4294967295;
        this.sort = false;
        this.search = false;
        this.edit = false;
        this.del = false;
        this.detailedView = false;
        this.add = false;
        this.globalTitle = null;
        this.detailedTitle = null;
        this.addTitle = null;
    }
    ;
    Command.prototype.parseData = function (data, id, callback) {
        if (Helper.isUnset(id)) {
            this.parseDataAsGlobal(data, callback);
        }
        else {
            this.parseDataAsDetailed(data, id, callback);
        }
        Configuration.instance.clearCache();
    };
    Command.prototype.parseDataAsGlobal = function (data, callback) {
        var _this = this;
        var res = {};
        var i;
        var j;
        var node;
        res.options = this;
        res.data = [];
        res.header = this.getGlobalHeader();
        for (j = 0; j < data.length; j++) {
            res.data.push({ _id: data[j][this.id], columns: [] });
        }
        i = 0;
        Helper.whileAsync(function () { return i < _this.dataNodes.length; }, function (outerNext) {
            node = _this.dataNodes[i];
            if (node.view === View.GlobalView || node.view === View.BothViews) {
                j = 0;
                Helper.whileAsync(function () { return j < data.length; }, function (innerNext) {
                    node.readValue(data[j], null, function (err, value) {
                        if (err) {
                            innerNext(err);
                            return;
                        }
                        res.data[j].columns.push(value);
                        j += 1;
                        innerNext();
                    }); //TODO add contextual check on Node, to refuse in the global view the non leaves nodes.
                }, function (err) {
                    i += 1;
                    outerNext(err);
                });
            }
            else {
                i += 1;
                outerNext();
            }
        }, function (err) { return callback(err, res); });
    };
    Command.prototype.getGlobalHeader = function () {
        var i;
        var header = [];
        var node;
        for (i = 0; i < this.dataNodes.length; i += 1) {
            node = this.dataNodes[i];
            if (node.view === View.GlobalView || node.view === View.BothViews) {
                header.push(node.getHeader());
            }
        }
        return header;
    };
    Command.prototype.getOptions = function () {
        return this;
    };
    Command.prototype.parseDataAsDetailed = function (data, id, callback) {
        var _this = this;
        var res = {};
        var i;
        var node;
        res.options = this;
        res.data = [];
        res._id = id;
        data = data[0]; // we parse a detailed data, so the count must be 1
        i = 0;
        Helper.whileAsync(function () { return i < _this.dataNodes.length; }, function (next) {
            node = _this.dataNodes[i];
            if (node.view === View.DetailedView || node.view === View.BothViews) {
                node.readValue(data, null, function (err, value) {
                    if (err) {
                        next(err);
                        return;
                    }
                    i += 1;
                    res.data.push(value);
                    next();
                });
            }
            else {
                i += 1;
                next();
            }
        }, function (err) { return callback(err, res); });
    };
    Command.prototype.getDefaultData = function (callback) {
        var res = {};
        var i;
        // var node: DataNode;
        res.options = this;
        res.data = [];
        i = 0;
        while (i < this.dataNodes.length) {
            res.data.push(this.dataNodes[i].getDefaultValue(null, null));
            i += 1;
        }
        callback(null, res);
    };
    Command.parseCommandObject = function (viewObject) {
        var i;
        var key;
        if (Helper.isUnset(viewObject.name)) {
            throw new SyntaxError('A command must have a name.');
        }
        if (Helper.isUnset(viewObject.collection)) {
            throw new SyntaxError('A command must have a collection.');
        }
        var command = new Command(viewObject.name, viewObject.collection, viewObject.backend);
        for (key in viewObject) {
            if (viewObject.hasOwnProperty(key)) {
                switch (key) {
                    case 'name':
                    case 'collection':
                    case 'backend':
                        break;
                    case 'params':
                        command.params = viewObject.params;
                        break;
                    case 'id':
                        command.id = Helper.sanitizeString(viewObject.id);
                        break;
                    case 'nodes':
                        for (i = 0; i < viewObject.nodes.length; i = i + 1) {
                            command.dataNodes.push(DataNode.parseDataNodeObject(viewObject.nodes[i], command));
                        }
                        break;
                    case 'components':
                        for (i = 0; i < viewObject.components.length; i = i + 1) {
                            command.components.push(Component.parseComponentObject(viewObject.components[i], command));
                        }
                        break;
                    case 'paginate':
                        command.paginate = Helper.sanitizeNumber(viewObject.paginate);
                        if (command.paginate < 0) {
                            throw new SyntaxError('Paginate cannot have a negative value.');
                        }
                        break;
                    case 'sort':
                        command.sort = Helper.sanitizeBoolean(viewObject.sort);
                        break;
                    case 'search':
                        command.search = Helper.sanitizeBoolean(viewObject.search);
                        break;
                    case 'aggregate':
                        for (i = 0; i < viewObject.aggregate.length; i = i + 1) {
                            command.aggregate.push(Aggregator.parseAggregatorObject(viewObject.aggregate[i], command));
                        }
                        break;
                    case 'edit':
                        command.edit = Helper.sanitizeBoolean(viewObject.edit);
                        break;
                    case 'confirmEdit':
                        command.confirmEdit = Helper.sanitizeBoolean(viewObject.confirmEdit);
                        break;
                    case 'del':
                        command.del = Helper.sanitizeBoolean(viewObject.del);
                        break;
                    case 'detailedView':
                        command.detailedView = Helper.sanitizeBoolean(viewObject.detailedView);
                        break;
                    case 'add':
                        command.add = Helper.sanitizeBoolean(viewObject.add);
                        break;
                    case 'disableInitialDataFetch':
                        command.disableInitialDataFetch = Helper.sanitizeBoolean(viewObject.disableInitialDataFetch);
                        break;
                    case 'globalTitle':
                        command.globalTitle = Helper.sanitizeString(viewObject.globalTitle);
                        break;
                    case 'detailedTitle':
                        command.detailedTitle = Helper.sanitizeString(viewObject.detailedTitle);
                        break;
                    case 'addTitle':
                        command.addTitle = Helper.sanitizeString(viewObject.addTitle);
                        break;
                    default:
                        throw new SyntaxError('Unrecognized key ' + key + '.');
                }
            }
        }
        //TODO add contextual checks
        return command;
    };
    return Command;
}());
var Aggregator = /** @class */ (function () {
    function Aggregator(command) {
        this.command = command;
        this.name = null;
        this.aggregationFunction = null;
        this.bgColor = '0xffffff';
    }
    Aggregator.parseAggregatorObject = function (aggregatorObject, command) {
        var resAggregator = new Aggregator(command);
        if (Helper.isUnset(aggregatorObject.name)) {
            throw new SyntaxError('An aggregator must have a name.');
        }
        if (Helper.isUnset(aggregatorObject.aggregationFunction)) {
            throw new SyntaxError('An aggregator must have an aggregationFunction.');
        }
        if (!Helper.isString(aggregatorObject.name)) {
            throw new SyntaxError('A name is supposed to be a string (found ' + typeof aggregatorObject.name + ').');
        }
        if (!Helper.isFunction(aggregatorObject.aggregationFunction)) {
            throw new SyntaxError('Am aggregationFunction is supposed to be a function (found ' + typeof aggregatorObject.aggregationFunction + ').');
        }
        if (Helper.isString(aggregatorObject.bgColor)) {
            resAggregator.bgColor = aggregatorObject.bgColor;
        }
        resAggregator.name = aggregatorObject.name;
        resAggregator.aggregationFunction = aggregatorObject.aggregationFunction;
        return resAggregator;
    };
    return Aggregator;
}());
var DataNode = /** @class */ (function () {
    function DataNode(command) {
        this.command = command;
        this.name = null;
        this.displayAs = null;
        this.accessor = [];
        this.translationToDisplay = null;
        this.translationToDB = null;
        this.saveAs = null;
        this.extensible = false;
        this.searchable = false;
        this.sortable = false;
        this.view = null;
        this.defaultValue = null;
        // this.$name = null;
        // this.alias = null;
        this.aggregatable = [];
        this.values = null;
        this.isLeaf = false;
        this.iterate = false;
        this.hideKey = false;
        this.$keys = null;
        this.$values = null;
    }
    DataNode.prototype.clone = function () {
        return Object.create(this);
    };
    DataNode.prototype.getHeader = function () {
        return { name: this.name, searchable: this.searchable, sortable: this.sortable, aggregatable: this.aggregatable, options: this };
    };
    DataNode.parseDataNodeObject = function (dataNodeObject, command) {
        var key;
        var i;
        var resDataNode = new DataNode(command);
        var tmpDataNode;
        for (key in dataNodeObject) {
            if (dataNodeObject.hasOwnProperty(key)) {
                switch (key) {
                    case 'name':
                        resDataNode.name = Helper.sanitizeString(dataNodeObject.name);
                        break;
                    case 'displayAs':
                        if (Helper.isDisplayType(dataNodeObject.displayAs)) {
                            resDataNode.isLeaf = true;
                            resDataNode.displayAs = Helper.sanitizeDisplayType(dataNodeObject.displayAs);
                        }
                        else if (Helper.isArray(dataNodeObject.displayAs)) {
                            resDataNode.displayAs = [];
                            for (i = 0; i < dataNodeObject.displayAs.length; i = i + 1) {
                                dataNodeObject.displayAs[i].view = Helper.sanitizeView(dataNodeObject.view); // TODO, remove this duplicate
                                tmpDataNode = DataNode.parseDataNodeObject(dataNodeObject.displayAs[i], command);
                                resDataNode.displayAs.push(tmpDataNode);
                            }
                        }
                        else {
                            throw new TypeError('Expected displayAs to be a DisplayType or an array but found ' + typeof (dataNodeObject.displayAs) + '. (' + dataNodeObject.displayAs + ')');
                        }
                        break;
                    case 'accessor':
                        if (Helper.isString(dataNodeObject.accessor)) {
                            dataNodeObject.accessor = dataNodeObject.accessor.split('.');
                        }
                        if (Helper.isArray(dataNodeObject.accessor)) {
                            for (i = 0; i < dataNodeObject.accessor.length; i = i + 1) {
                                resDataNode.accessor.push(Accessor.parseAccessorObject(dataNodeObject.accessor[i], command));
                            }
                        }
                        else {
                            throw new TypeError('Expected the accessor to be a string or an array but found ' + typeof (dataNodeObject.accessor) + '.');
                        }
                        break;
                    case 'translationToDisplay':
                        resDataNode.translationToDisplay = Helper.sanitizeFunction(dataNodeObject.translationToDisplay);
                        break;
                    case 'translationToDB':
                        resDataNode.translationToDB = Helper.sanitizeFunction(dataNodeObject.translationToDB);
                        break;
                    case 'saveAs':
                        resDataNode.saveAs = Helper.sanitizeDbType(dataNodeObject.saveAs);
                        if (resDataNode.defaultValue === null) {
                            resDataNode.defaultValue = this.getDefaultValue(resDataNode.saveAs);
                        }
                        break;
                    case 'extensible':
                        resDataNode.extensible = Helper.sanitizeBoolean(dataNodeObject.extensible);
                        break;
                    case 'searchable':
                        resDataNode.searchable = Helper.sanitizeBoolean(dataNodeObject.searchable);
                        break;
                    case 'sortable':
                        resDataNode.sortable = Helper.sanitizeBoolean(dataNodeObject.sortable);
                        break;
                    case 'aggregatable':
                        for (i = 0; i < dataNodeObject.aggregatable.length; i = i + 1) {
                            resDataNode.aggregatable.push(Helper.sanitizeBoolean(dataNodeObject.aggregatable[i]));
                        }
                        break;
                    case 'view':
                        resDataNode.view = Helper.sanitizeView(dataNodeObject.view);
                        break;
                    case 'values':
                        if (Helper.isArray(dataNodeObject.values)) {
                            resDataNode.values = [];
                            for (i = 0; i < dataNodeObject.values.length; i = i + 1) {
                                resDataNode.values.push(Helper.sanitizeString(dataNodeObject.values[i]));
                            }
                        }
                        else if (Helper.isString(dataNodeObject.values)) {
                            resDataNode.values = dataNodeObject.values;
                        }
                        else {
                            throw new TypeError('Expected the values to be an array or a $name but found ' + typeof (dataNodeObject.values) + '.');
                        }
                        break;
                    case 'iterate':
                        resDataNode.iterate = Helper.sanitizeBoolean(dataNodeObject.iterate);
                        break;
                    case 'hideKey':
                        resDataNode.hideKey = Helper.sanitizeBoolean(dataNodeObject.hideKey);
                        break;
                    case 'saveKeys':
                        resDataNode.$keys = Helper.sanitizeString(dataNodeObject.saveKeys);
                        Configuration.instance.$nameCache[resDataNode.$keys] = [];
                        break;
                    case 'saveValues':
                        resDataNode.$values = Helper.sanitizeString(dataNodeObject.saveValues);
                        Configuration.instance.$nameCache[resDataNode.$values] = [];
                        break;
                    case 'defaultValue':
                        resDataNode.defaultValue = dataNodeObject.defaultValue;
                        break;
                    default:
                        throw new SyntaxError('Unrecognized key ' + key + '.');
                }
            }
        }
        resDataNode.contextualChecks();
        resDataNode.generateTranslationFunctions();
        return resDataNode;
    };
    DataNode.getDefaultValue = function (saveAs) {
        switch (saveAs) {
            case DbType.String:
                return '';
            case DbType.Number:
                return 0;
            case DbType.Date:
                return new Date();
            case DbType.ObjectId:
                return null; // TODO
            case DbType.Boolean:
                return false;
            case DbType.Array:
                return null;
            case DbType.Map:
                return null; // have to be specifically overridden in the case of checkboxes
        }
        return null;
    };
    DataNode.prototype.contextualChecks = function () {
        if (this.extensible) {
            if (this.saveAs !== null) {
                if (this.saveAs !== DbType.Array && this.saveAs !== DbType.Map) {
                    throw new SyntaxError('Extensible is only valid with Maps and Arrays.');
                }
            }
        }
        if (this.displayAs === DisplayType.Dropdown && Helper.isUnset(this.values)) {
            throw new SyntaxError('displayAs: "dropdown" requires values or a $name to be defined.'); // $name => reference a name that was register after a $values or $keys
        }
    };
    DataNode.prototype.generateTranslationFunctions = function () {
        if (this.translationToDB === null) {
            switch (this.saveAs) {
                case DbType.String:
                    this.translationToDB = function (v) { return v.toString(); };
                    break;
                case DbType.Number:
                    this.translationToDB = function (v) { return parseInt(v, 10); };
                    break;
                case DbType.Date:
                    this.translationToDB = function (v) { return new Date(v); };
                    break;
                case DbType.ObjectId:
                    this.translationToDB = Helper.passThrough;
                    break;
                case DbType.Boolean:
                    this.translationToDB = function (v) { return v === true || v === 'true' || v === 1; };
                    break;
                case DbType.Array:
                    this.translationToDB = Helper.passThrough;
                    break;
                case DbType.Map:
                    this.translationToDB = Helper.passThrough;
                    break;
            }
        }
        if (this.translationToDisplay === null) {
            switch (this.displayAs) {
                case DisplayType.Component:
                    this.translationToDisplay = Helper.passThrough;
                    break;
                case DisplayType.Readonly:
                    this.translationToDisplay = function (v) {
                        if (!v) {
                            return ''; // no value
                        }
                        return v.toString();
                    };
                    break;
                case DisplayType.Text:
                    this.translationToDisplay = function (v) {
                        if (!v) {
                            return ''; // no value
                        }
                        return v.toString();
                    };
                    break;
                case DisplayType.Number:
                    this.translationToDisplay = function (v) { return parseInt(v, 10); };
                    break;
                case DisplayType.Checkbox:
                    this.translationToDisplay = function (v) { return v === true || v === 'true' || v === 1; };
                    break;
                case DisplayType.Checkboxes:
                    this.translationToDisplay = Helper.passThrough;
                    break;
                case DisplayType.Date:
                    this.translationToDisplay = Helper.passThrough;
                    break;
                case DisplayType.Image:
                    this.translationToDisplay = Helper.passThrough;
                    break;
                case DisplayType.BlobImage:
                    this.translationToDisplay = Helper.passThrough;
                    break;
                case DisplayType.Dropdown:
                    this.translationToDisplay = function (v) { return v.toString(); };
                    break;
            }
        }
    };
    DataNode.prototype.getDefaultValue = function (value, key) {
        var i;
        var nodes;
        var res;
        var newOptions;
        if (value === null) {
            value = this.defaultValue;
        }
        if (this.displayAs === DisplayType.Component) { // Only if not a component
            return;
        }
        if (key !== null && this.displayAs === DisplayType.Readonly) {
            newOptions = this.clone(); // we alter something hence the copy
            newOptions.name = key;
            newOptions.displayAs = DisplayType.Text;
        }
        else if (this.displayAs === DisplayType.Readonly) {
            newOptions = this.clone();
            newOptions.displayAs = DisplayType.Text;
        }
        else if (key !== null) {
            newOptions = this.clone();
            newOptions.name = key;
        }
        else {
            newOptions = this;
        }
        if (this.iterate) {
            return {
                value: this.iterateReadDefaultValue(value),
                options: newOptions,
                key: key
            };
        }
        else if (this.isLeaf) {
            return {
                value: value,
                options: newOptions,
                key: key
            };
        }
        else {
            res = [];
            nodes = this.displayAs;
            for (i = 0; i < nodes.length; i += 1) {
                res.push(nodes[i].getDefaultValue(value, null));
            }
            return {
                value: res,
                options: newOptions,
                key: key
            };
        }
    };
    DataNode.prototype.iterateReadDefaultValue = function (current) {
        var key;
        var res = [];
        for (key in current) {
            if (current.hasOwnProperty(key)) {
                res.push(this.displayAs[0].getDefaultValue(current[key], key));
            }
        }
        return res;
    };
    DataNode.prototype.readValue = function (row, key, callback) {
        var _this = this;
        var i;
        var current = row;
        var nodes;
        var res;
        var newOptions;
        if (this.displayAs === DisplayType.Component) { // Only if not a component
            callback(null, {
                value: current,
                options: Helper.clone(this, true),
                key: key
            });
            return;
        }
        i = 0;
        Helper.whileAsync(function () { return _this.accessor === null ? false : i < _this.accessor.length; }, function (outerNext) {
            _this.accessor[i].accessData(current, function (err, newValue) {
                if (err) {
                    outerNext(err);
                    return;
                }
                current = newValue;
                i += 1;
                outerNext();
            });
        }, function (err) {
            if (err) {
                callback(err);
                return;
            }
            newOptions = Helper.clone(_this, true);
            if (key !== null) {
                newOptions.name = key;
            }
            if (_this.iterate) {
                _this.iterateFunc(current, function (err, value) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    callback(null, {
                        value: value,
                        options: newOptions,
                        key: key
                    });
                });
            }
            else if (_this.isLeaf) {
                callback(null, {
                    value: _this.translationToDisplay(current),
                    options: newOptions,
                    key: key
                });
            }
            else {
                res = [];
                nodes = _this.displayAs;
                i = 0;
                Helper.whileAsync(function () { return i < nodes.length; }, function (innerNext) {
                    nodes[i].readValue(current, null, function (err, value) {
                        if (err) {
                            innerNext(err);
                            return;
                        }
                        i += 1;
                        res.push(value);
                        innerNext();
                    });
                }, function (err) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    callback(null, {
                        value: res,
                        options: newOptions,
                        key: key
                    });
                });
            }
        });
    };
    DataNode.prototype.iterateFunc = function (current, callback) {
        var _this = this;
        var key;
        var keys = [];
        var res = [];
        var i;
        for (key in current) {
            if (current.hasOwnProperty(key)) {
                keys.push(key);
                if (this.$keys !== null) {
                    Configuration.instance.$nameCache[this.$keys].push(key);
                }
            }
        }
        i = 0;
        Helper.whileAsync(function () { return i < keys.length; }, function (next) {
            _this.displayAs[0].readValue(current[keys[i]], keys[i], function (err, value) {
                if (err) {
                    next(err);
                    return;
                }
                i += 1;
                res.push(value);
                if (_this.$values !== null) {
                    Configuration.instance.$nameCache[_this.$values].push(value.value);
                }
                next();
            });
        }, function (err) { return callback(err, res); });
    };
    DataNode.getUpdateObject = function (updateObjectArray, collection, _id, id) {
        var i;
        var updateObject;
        for (i = 0; i < updateObjectArray.length; i += 1) {
            if (collection === updateObjectArray[i].collection) {
                return updateObjectArray[i].updateObject;
            }
        }
        updateObject = {};
        updateObjectArray.push({
            collection: collection,
            _id: _id,
            updateObject: updateObject,
            id: id
        });
        return updateObject;
    };
    DataNode.prototype.getAccessor = function () {
        var currentAccessor = '';
        var i;
        for (i = 0; i < this.accessor.length; i += 1) {
            if (this.accessor[i].isJumping()) {
                return null; // TODO handle the case we have to filter/sort on another collection
            }
            else {
                currentAccessor = Helper.joinAccessors(currentAccessor, this.accessor[i].getAccessor());
            }
        }
        return currentAccessor;
    };
    DataNode.prototype.addValueToInsertObject = function (insertObject, value) {
        var i;
        var nodes;
        var accessor;
        if (value === null) {
            return;
        }
        if (this.displayAs === DisplayType.Component) {
            return;
        }
        for (i = 0; i < this.accessor.length; i += 1) {
            if (this.accessor[i].isJumping()) {
                return;
            }
        }
        for (i = 0; i < this.accessor.length - 1; i += 1) {
            accessor = this.accessor[i].getAccessor();
            if (insertObject[accessor] === undefined) {
                insertObject[accessor] = {};
            }
            insertObject = insertObject[accessor];
        }
        accessor = this.accessor[i].getAccessor();
        if (this.iterate || this.isLeaf) {
            insertObject[accessor] = this.translationToDB(value);
        }
        else {
            nodes = this.displayAs;
            for (i = 0; i < nodes.length; i += 1) {
                nodes[i].addValueToInsertObject(insertObject, value[i].value);
            }
        }
    };
    DataNode.prototype.addValueToUpdateObject = function (updateObjectArray, value, prefixWith) {
        var i;
        var currentAccessor = prefixWith || '';
        var nodes;
        var updateObject = updateObjectArray[0].updateObject;
        if (this.displayAs === DisplayType.Component || this.displayAs === DisplayType.Readonly || this.view === View.Hidden) {
            return;
        }
        for (i = 0; i < this.accessor.length; i += 1) {
            if (this.accessor[i].isJumping()) {
                currentAccessor = '';
                prefixWith = '';
                updateObject = DataNode.getUpdateObject(updateObjectArray, this.accessor[i].getCollection(), this.accessor[i].getId(), this.command.id);
            }
            else {
                currentAccessor = Helper.joinAccessors(currentAccessor, this.accessor[i].getAccessor());
            }
        }
        if (this.iterate) {
            for (i = 0; i < value.length; i += 1) {
                this.displayAs[0].addValueToUpdateObject(updateObjectArray, value[i].value, currentAccessor + '.' + value[i].key);
            }
        }
        else {
            if (this.isLeaf) {
                updateObject[currentAccessor] = this.translationToDB(value);
            }
            else {
                nodes = this.displayAs;
                for (i = 0; i < nodes.length; i += 1) {
                    nodes[i].addValueToUpdateObject(updateObjectArray, value[i].value, currentAccessor);
                }
            }
        }
    };
    DataNode.prototype.getValues = function () {
        if (Helper.isArray(this.values)) {
            return this.values;
        }
        return Configuration.instance.$nameCache[this.values];
    };
    return DataNode;
}());
var Accessor = /** @class */ (function () {
    function Accessor(accessor, command) {
        this.command = command;
        this.accessor = Helper.sanitizeString(accessor);
        this.collection = null;
        this.transtypingFunction = Helper.passThrough;
        this._id = null;
    }
    Accessor.parseAccessorObject = function (accessorObject, command) {
        var key;
        var resAccessor;
        if (Helper.isString(accessorObject)) {
            return new Accessor(accessorObject, command);
        }
        if (Helper.isUnset(accessorObject.accessor)) {
            throw new SyntaxError('An accessor object must have an accessor field.');
        }
        resAccessor = new Accessor(Helper.sanitizeString(accessorObject.accessor), command);
        for (key in accessorObject) {
            if (accessorObject.hasOwnProperty(key)) {
                switch (key) {
                    case 'accessor':
                        break;
                    case 'collection':
                        resAccessor.collection = Helper.sanitizeString(accessorObject.collection);
                        break;
                    case 'transtypingFunction':
                        resAccessor.transtypingFunction = Helper.sanitizeFunction(accessorObject.transtypingFunction);
                        break;
                    default:
                        throw new SyntaxError('Unrecognized key ' + key + '.');
                }
            }
        }
        return resAccessor;
    };
    Accessor.prototype.accessData = function (rowOrSubrow, callback) {
        var _this = this;
        if (this.collection === null) {
            callback(null, rowOrSubrow[this.accessor]);
        }
        else {
            Configuration.instance.getData(this.command.backend, this.collection, this.accessor, rowOrSubrow, this.command.params, function (err, value) {
                if (value) {
                    _this._id = value[_this.command.id];
                }
                callback(err, value);
            });
        }
    };
    Accessor.prototype.accessDataSync = function (rowOrSubrow) {
        return rowOrSubrow[this.accessor];
    };
    Accessor.prototype.isJumping = function () {
        return this.collection != null;
    };
    Accessor.prototype.getCollection = function () {
        return this.collection;
    };
    Accessor.prototype.getId = function () {
        return this._id;
    };
    Accessor.prototype.getAccessor = function () {
        return this.accessor;
    };
    return Accessor;
}());
var Component = /** @class */ (function () {
    function Component(name, command) {
        this.command = command;
        this.name = name.split(/(?=[A-Z])/).join('-'); // to instantiate a component, we need it in the form db-edition, not dbEdition
        this.anchorToTop = false;
        this.view = 0;
        this.options = null;
    }
    Component.parseComponentObject = function (componentObject, command) {
        var key;
        var resComponent;
        if (Helper.isUnset(componentObject.name)) {
            throw new SyntaxError('A component must have a name.');
        }
        resComponent = new Component(Helper.sanitizeString(componentObject.name), command);
        for (key in componentObject) {
            if (componentObject.hasOwnProperty(key)) {
                switch (key) {
                    case 'name':
                        break;
                    case 'anchorToTop':
                        resComponent.anchorToTop = Helper.sanitizeBoolean(componentObject.anchorToTop);
                        break;
                    case 'view':
                        if (componentObject.view in View) { // leaf
                            if (Helper.isString(componentObject.view)) {
                                resComponent.view = View[componentObject.view];
                            }
                            else {
                                resComponent.view = componentObject.view;
                            }
                        }
                        else {
                            throw new TypeError('Unrecognized view ' + componentObject.view + '.');
                        }
                        break;
                    case 'options':
                        resComponent.options = componentObject.options;
                        break;
                    default:
                        throw new SyntaxError('Unrecognized key ' + key + '.');
                }
            }
        }
        return resComponent;
    };
    return Component;
}());

},{}]},{},[13]);
