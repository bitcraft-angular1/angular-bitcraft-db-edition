/**
 * Created by richard on 20/10/16.
 */

'use strict';
/*global angular*/

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition')
    .component('bitcraftDbEditionAdd', {
        templateUrl: 'js/db-edition/db-edition-add/db-edition-add.template.html',
        controller: [
            '$scope', '$state', '$stateParams', 'Notification', 'DBEdition',
            function ($scope, $state, $stateParams, Notification, DBEdition) {
                //noinspection JSUnusedGlobalSymbols
                /**
                 * Init the data for display.
                 */
                this.$onInit = function () {
                    DBEdition.getDefaultData($stateParams.command).then(function (data) {
                        console.log(data);
                        $scope.global = data;
                        $scope.done = true;
                    }, function (err) {
                        Notification.error({message: 'Failed to retrieve data (' + err.statusText + ').'});
                    });
                };

                var self = this;

                self.cancel = function () {
                    $state.go('^');
                };

                self.add = function () {
                    DBEdition.insertData($scope.global.options.backend, $scope.global.options.collection, $scope.global.data, $stateParams.command.params).then(function () {
                        $state.go('^');
                    }, function (err) {
                        Notification.error({message: 'Failed to add the entry (' + err.statusText + ').'});
                    });
                };

                // select/dropdown
                self.getValues = function (options) {
                    if (options.values) {
                        return options.getValues();
                    }
                };
            }
        ]
    });
