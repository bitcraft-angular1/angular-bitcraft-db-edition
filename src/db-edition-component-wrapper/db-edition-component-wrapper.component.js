/**
 * Created by richard on 20/10/16.
 */

'use strict';
/*global angular, $*/

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition')
    .component('bitcraftDbEditionComponentWrapper', {
        controller: [
            '$scope', '$element', '$compile',
            function ($scope, $element, $compile) {
                this.$onInit = function () {
                    var componentName = this.data.component.split(/(?=[A-Z])/).join('-');
                    var rowString = JSON.stringify(this.data.row).replace(/"/g, '\'');
                    var compiledComponent = $compile('<' + componentName + ' data="' + rowString + '"></' + componentName + '>')($scope.$new(false, $scope));

                    // 2020/02/20 Hack en mousse
                    // Stores the original object instead of a serialized value.
                    // The idea is to retrieve the good data from the original reference instead of
                    // the initial data (serialized one).
                    var self = this;
                    $element[0].getRowData = function() { return self.data; };

                    $($element[0]).append(compiledComponent);
                };
            }
        ],
        bindings: {
            data: '<'
        }
    });
