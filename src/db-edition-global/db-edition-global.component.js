/*global angular, $ */
'use strict';

function saveOldData(row) {
    var i;

    if (row.oldValue) {
        return;
    }

    row.oldValue = [];

    for (i = 0; i < row.columns.length; i = i + 1) {
        row.oldValue.push(angular.copy(row.columns[i].value));
    }
}

function restoreOldData(row) {
    var i;

    for (i = 0; i < row.columns.length; i = i + 1) {
        row.columns[i].value = angular.copy(row.oldValue[i]);
    }
}

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition')
    .component('bitcraftDbEditionGlobal', {
        templateUrl: './js/db-edition/db-edition-global/db-edition.template.html',
        controller: [
            '$scope', '$compile', '$state', '$stateParams', '$uibModal', 'Notification', 'DBEdition',
            function ($scope, $compile, $state, $stateParams, $uibModal, Notification, DBEdition) {
                function initializeComponents() {
                    var i;
                    var component, compiledComponent;
                    var globalView = $('#db-edition-global-view');

                    for (i = 0; i < $scope.command.components.length; i = i + 1) {
                        component = $scope.command.components[i];
                        if (component.view === 1 || component.view === 3) {
                            compiledComponent = $compile('<' + component.name + '></' + component.name + '>')($scope.$new(false, $scope));

                            if (component.anchorToTop) {
                                globalView.prepend(compiledComponent);
                            } else {
                                globalView.append(compiledComponent);
                            }
                        }
                    }

                    $scope.done = true;
                }

                // update data based on the new state
                function updateDisplayedData(next) {
                    DBEdition.retrieveData($scope.command.backend, $scope.command.name, null, null, $scope.currentPageNumber - 1, $scope.sortObj, $scope.searchObj, $scope.command.params).then(function (data) {
                        $scope.global = data;

                        if ($scope.searchObj.searchInput !== '') {
                            $scope.header[$scope.searchObj.index].searchInput = $scope.searchObj.searchInput;
                        }

                        if ($scope.sortObj.sortOn !== '') {
                            $scope.header[$scope.sortObj.index].sortClass = $scope.sortObj.reverse ? 'sort-descendant' : 'sort-ascendant';
                        }

                        if (next) {
                            next();
                        }
                    }, function (err) {
                        Notification.error({message: 'Failed to retrieve data (' + err.statusText + ').'});
                    });
                }

                var self = this;

                this.$onInit = function () {
                    /** @type{Command} */
                    $scope.command = DBEdition.getCommandFromName($stateParams.command);
                    if ($scope.command === null) { // TODO
                        console.error('Failed to retrieve command');
                        return;
                    }

                    // edition
                    $scope.currentlyVisible = null;

                    // pagination
                    $scope.currentPageNumber = 1;

                    // sorting
                    $scope.sortObj = {
                        sortOn: '',
                        reverse: true,
                        index: 0
                    };

                    // searching
                    $scope.searchObj = {
                        searchOn: '',
                        searchInput: '',
                        index: 0
                    };
                    $scope.searchDebounce = null;

                    $scope.header = $scope.command.getGlobalHeader();
                    $scope.options = $scope.command.getOptions();

                    self.paginatedIndex = 0;

                    if ($scope.command.disableInitialDataFetch) {
                        initializeComponents();
                    } else {
                        updateDisplayedData(initializeComponents);
                    }
                };

                // detailedView
                self.seeDetails = function (index) {
                    $state.go('.id', {id: $scope.global.data[index]._id}, {location: true});
                };

                // searching
                function executeSearch(index, searchValue) {
                    $scope.searchDebounce = null;

                    var searchKey = $scope.header[index].options.getAccessor();

                    if (searchKey === $scope.searchObj.searchOn) {
                        $scope.searchObj.searchInput = searchValue;
                    } else {
                        $scope.searchObj.searchOn = searchKey;
                        $scope.searchObj.searchInput = searchValue;
                        $scope.searchObj.index = index;
                    }

                    updateDisplayedData();
                }

                self.log = function (object) {
                    console.log(object);
                };

                self.search = function (index) {
                    if ($scope.searchDebounce !== null) {
                        clearTimeout($scope.searchDebounce);
                        $scope.searchDebounce = null;
                    }

                    $scope.searchDebounce = setTimeout(function () {
                        executeSearch(index, $scope.header[index].searchInput);
                    }, 200);
                };

                // sorting
                self.sort = function (index) {
                    var sortKey = $scope.header[index].options.getAccessor();

                    if (sortKey === $scope.sortObj.sortOn) {
                        $scope.sortObj.reverse = !$scope.sortObj.reverse;
                    } else {
                        $scope.sortObj.sortOn = sortKey;
                        $scope.sortObj.reverse = false;
                        $scope.sortObj.index = index;
                    }

                    updateDisplayedData();
                };

                // pagination
                self.changePage = function (newPageNumber) {
                    self.paginatedIndex = $scope.options.paginate * (newPageNumber - 1);
                    updateDisplayedData();
                };

                // deletion
                self.delete = function (index) {
                    DBEdition.deleteData($scope.command.backend, $scope.command.collection, $scope.global.data[index]._id, $scope.command.id, $scope.command.params).then(function () {
                        $scope.global.data.splice(index, 1);
                    }, function (err) {
                        Notification.error({message: 'Failed to delete the entry (' + err.statusText + ').'});
                    });
                };

                // edition
                var showFunction = function (index, cb) {
                    DBEdition.lockData($scope.command.backend, $scope.command.collection, $scope.global.data[index]._id, $scope.command.params).then(function () {
                        saveOldData($scope.global.data[index]);
                        $scope.global.data[index].visible = true;
                        $scope.currentlyVisible = index;

                        if (cb) {
                            cb();
                        }
                    }, function (err) {
                        Notification.error({message: 'Failed to lock the entry (' + err.statusText + ').'});
                    });
                };

                var cancelFunction = function (index, cb) {
                    DBEdition.unlockData($scope.command.backend, $scope.command.params).then(function () {
                        $scope.global.data[index].visible = false;
                        $scope.global.data[index].preview = false;
                        restoreOldData($scope.global.data[index]);
                        $scope.currentlyVisible = null;

                        if (cb) {
                            cb();
                        }
                    }, function (err) {
                        Notification.error({message: 'Failed to unlock the entry (' + err.statusText + ').'});
                    });
                };

                var updateFunction = function (index, cb) {
                    if ($scope.command.confirmEdit && !$scope.global.data[index].preview) {
                        $scope.global.data[index].preview = true;
                    } else {
                        DBEdition.updateData($scope.command.backend, $scope.command.collection, $scope.global.data[index]._id, $scope.command.id, $scope.global.data[index].columns, $scope.command.params).then(function () {
                            $scope.global.data[index].visible = false;
                            $scope.global.data[index].preview = false;
                            $scope.global.data[index].oldValue = null;
                            $scope.currentlyVisible = null;

                            if (cb) {
                                cb($scope.command.backend, $scope.command.params);
                            }
                        }, function (err) {
                            Notification.error({message: 'Failed to update the entry (' + err.statusText + ').'});
                        });
                    }
                };

                self.edit = function (index, action) {
                    switch (action) {
                    case 'show':
                        if ($scope.currentlyVisible !== null) {
                            cancelFunction($scope.currentlyVisible, function () {
                                showFunction(index);
                            });
                        } else {
                            showFunction(index);
                        }
                        break;
                    case 'cancel':
                        cancelFunction(index);
                        break;
                    case 'update':
                        updateFunction(index, DBEdition.unlockData);
                        break;
                    default:
                        throw new Error('Unknown action. (' + action + ')');
                    }
                };

                // aggregation
                self.aggregate = function (index, func) { // here index is the column
                    var i;
                    var res = null;
                    var totalElements = $scope.global.data.length;

                    for (i = 0; i < totalElements; i = i + 1) {
                        res = func(res, $scope.global.data[i].columns[index].value, i, totalElements - (i + 1), totalElements);
                    }

                    return res;
                };

                // addition
                self.add = function () {
                    $state.go('.add', {location: true});
                };

                // select/dropdown
                self.getValues = function (options) {
                    if (options.values) {
                        return options.values;
                    }
                    if (options.$name) {
                        console.log('TODO');
                    }
                };

                self.updateDisplayedData = updateDisplayedData;
            }
        ]
    });
