'use strict';
/*global $, angular*/

function saveOldData(row) {
    var i;

    if (row.oldValue) {
        return;
    }

    row.oldValue = [];

    for (i = 0; i < row.data.length; i = i + 1) {
        row.oldValue.push(angular.copy(row.data[i].value));
    }
}

function restoreOldData(row) {
    var i;

    for (i = 0; i < row.data.length; i = i + 1) {
        row.data[i].value = angular.copy(row.oldValue[i]);
    }
}

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition')
    .component('bitcraftDbEditionId', {
        templateUrl: 'js/db-edition/db-edition-id/db-edition-id.template.html',
        controller: [
            '$scope', '$state', '$stateParams', '$compile', 'Notification', 'DBEdition',
            function ($scope, $state, $stateParams, $compile, Notification, DBEdition) {
                function initializeComponents() {
                    var i;
                    var component, compiledComponent;
                    var globalView = $('#db-edition-detailed-view');

                    for (i = 0; i < $scope.command.components.length; i = i + 1) {
                        component = $scope.command.components[i];
                        if (component.view === 2 || component.view === 3) {
                            compiledComponent = $compile('<' + component.name + '></' + component.name + '>')($scope.$new(false, $scope));

                            if (component.anchorToTop) {
                                globalView.prepend(compiledComponent);
                            } else {
                                globalView.append(compiledComponent);
                            }
                        }
                    }

                    $scope.done = true;
                }

                // update data based on the new state
                function updateDisplayedData() {
                    DBEdition.retrieveData($scope.command.backend, $scope.command.name, $stateParams.id, $scope.command.id, undefined, undefined, undefined, $scope.command.params).then(function (data) {
                        $scope.global = data;
                        $scope.done = true;
                        console.log(data);
                        initializeComponents();
                    }, function (err) {
                        Notification.error({message: 'Failed to retrieve data (' + err.statusText + ').'});
                    });
                }

                this.$onInit = function () {
                    /** @type{Command} */
                    $scope.command = DBEdition.getCommandFromName($stateParams.command);
                    if ($scope.command === null) { // TODO
                        console.error('Failed to retrieve command');
                        return;
                    }

                    // TODO go to seeGlobal if the config does not let us see the detailed view
                    // edition
                    $scope.currentlyVisible = null;

                    updateDisplayedData();
                };

                var self = this;

                // globalView
                self.seeGlobal = function () {
                    $state.go('^');
                };

                // deletion
                self.delete = function () {
                    DBEdition.deleteData($scope.command.backend, $scope.command.collection, $stateParams.id, $scope.command.id, $scope.command.params).then(function () {
                        self.seeGlobal();
                    }, function (err) {
                        Notification.error({message: 'Failed to delete the entry (' + err.statusText + ').'});
                    });
                };

                // edition
                var showFunction = function (cb) {
                    DBEdition.lockData($scope.command.backend, $scope.command.collection, $stateParams.id, $scope.command.params).then(function () {
                        saveOldData($scope.global);
                        $scope.global.visible = true;

                        if (cb) {
                            cb();
                        }
                    }, function (err) {
                        Notification.error({message: 'Failed to lock the entry (' + err.statusText + ').'});
                    });
                };

                var cancelFunction = function (cb) {
                    DBEdition.unlockData($scope.command.backend, $scope.command.params).then(function () {
                        $scope.global.visible = false;
                        $scope.global.preview = false;
                        restoreOldData($scope.global);

                        if (cb) {
                            cb();
                        }
                    }, function (err) {
                        Notification.error({message: 'Failed to unlock the entry (' + err.statusText + ').'});
                    });
                };

                var updateFunction = function (cb) {
                    if ($scope.command.confirmEdit && !$scope.global.preview) {
                        $scope.global.preview = true;
                    } else {
                        DBEdition.updateData($scope.command.backend, $scope.command.collection, $stateParams.id, $scope.command.id, $scope.global.data, $scope.command.params).then(function () {
                            $scope.global.visible = false;
                            $scope.global.preview = false;
                            $scope.global.oldValue = null;

                            if (cb) {
                                cb($scope.command.backend, $scope.command.params);
                            }
                        }, function (err) {
                            Notification.error({message: 'Failed to update the entry (' + err.statusText + ').'});
                        });
                    }
                };

                self.edit = function (action) {
                    switch (action) {
                    case 'show':
                        showFunction();
                        break;
                    case 'cancel':
                        cancelFunction();
                        break;
                    case 'update':
                        updateFunction(DBEdition.unlockData);
                        break;
                    default:
                        throw new Error('Unknown action. (' + action + ')');
                    }
                };

                // aggregation
                self.aggregate = function (index, func) { // here index is the column
                    var i;
                    var res = null;
                    var totalElements = $scope.global.data.length;

                    for (i = 0; i < totalElements; i = i + 1) {
                        res = func(res, $scope.global.data[i].columns[index].value, i, totalElements - (i + 1), totalElements);
                    }

                    return res;
                };

                // select/dropdown
                self.getValues = function (options) {
                    if (options.values) {
                        return options.getValues();
                    }
                };

                self.isRepeater = function (options) {
                    return options.accessor[0].accessor === '$';
                };

                self.makeObject = function (value, key1) {
                    var res = {};
                    res[key1] = value;
                    return res;
                };

                self.addRow = function (to) {
                    if (to.options.saveAs === 5) {
                        to.value.push(to.options.displayAs[0].getDefaultValue(null, to.value.length));
                    } else {
                        console.error('Not implemented yet !');
                    }
                };

                self.delRow = function (from) {
                    from.value.splice(-1, 1);
                };

                self.forLoop = function (start, limit, step) {
                    var i;
                    var res = [];
                    step = step || 1;
                    for (i = start; i < limit; i += step) {
                        res.push(i);
                    }
                    return res;
                };
            }
        ]
    });
