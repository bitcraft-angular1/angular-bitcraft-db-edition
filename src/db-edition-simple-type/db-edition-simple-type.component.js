/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition')
    .component('bitcraftDbEditionSimpleType', {
        templateUrl: './js/db-edition-simple-type/db-edition-simple-type.template.html', // this line will be replaced
        bindings: {
            value: '=',
            values: '<',
            edit: '<',
            type: '<'
        }
    });
