/*global angular*/

angular.module('bitcraft-db-edition', [
    'angularUtils.directives.dirPagination',
    'ui.bootstrap',
    'ui.bootstrap.modal'
]);
