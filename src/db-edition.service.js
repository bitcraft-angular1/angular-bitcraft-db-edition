/*global angular*/
'use strict';

var libDBE =  require('./lib/db-edition-2.js').Configuration;
var libDBEHelpers = require('./lib/db-edition-2').Helper;

/**
 * @param {string} name of the cookie
 * @returns {string|null}
 */
function readCookie(name) {
    var nameEQ = name + "=";
    var cookies = document.cookie.split(';').map(function (s) { return s.replace(/\s/g, ''); });
    var i;
    for (i = 0; i < cookies.length; i += 1) {
        if (cookies[i].indexOf(nameEQ) === 0) {
            return cookies[i].substring(nameEQ.length);
        }
    }
    return null;
}

function decodeData(str) { //TODO handle mongoObjects WHEN 2.0 is working BY Richard
    var value = str.substr(2);
    if (value === '') {
        return undefined;
    }

    if (str[0] === 's') {
        return value;
    }
    if (str[0] === 'b') {
        if (value === 'true') {
            return true;
        }
        if (value === 'false') {
            return false;
        }
    }
    if (str[0] === 'n') {
        return Number(value);
    }
    if (str[0] === 'd') {
        return new Date(value);
    }
    if (str[0] === 'o') {
        return 'o_' + value;
    }

    return null;
}

function encodeData(data) {
    var type = typeof data;

    if (type === 'object') {
        if (data instanceof Date) {
            return 'd_' + data;
        }
    }

    if (type === 'number' || data instanceof Number) {
        return 'n_' + data;
    }

    if (type === 'boolean' || data instanceof Boolean) {
        return 'b_' + data;
    }

    if (type === 'string' || data instanceof String) {
        if (data[0] === 'o' && data[1] === '_') {
            return data;
        }

        return 's_' + data;
    }
}

function decodeObject(obj) {
    var key;
    var tmpObj;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            tmpObj = obj[key];
            if (typeof tmpObj === 'string' || tmpObj instanceof String) {
                obj[key] = decodeData(tmpObj);
            } else {
                decodeObject(tmpObj);
            }
        }
    }

    return obj;
}

function encodeObject(obj) {
    var key;
    var tmpObj;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            tmpObj = obj[key];
            if (typeof tmpObj === 'object' && !(tmpObj instanceof Date)) {
                encodeObject(tmpObj);
            } else {
                obj[key] = encodeData(tmpObj);
            }
        }
    }

    return obj;
}

function padResponse(array, totalElements, from, to) {
    var res = [];
    var i = 0;

    while (i < from) {
        res.push(null);
        i = i + 1;
    }

    while (i < to && i < totalElements) {
        res.push(array[i - from]);
        i = i + 1;
    }

    while (i < totalElements) {
        res.push(null);
        i = i + 1;
    }

    return res;
}

//noinspection JSUnresolvedFunction
angular.module('bitcraft-db-edition')
    .factory('DBEdition', [
        '$http', '$q',
        function ($http, $q) {
            var config = null;

            /**
             * @param {string} backend
             * @param {string} api
             * @returns {string}
             */
            function getUrl(backend, api) {
                return './rest/' + backend + '/' + api;
            }

            function getTypeValues($name) {
                var i;

                for (i = 0; i < config.types.length; i = i + 1) {
                    if (config.types[i].name === $name) {
                        return config.types[i].values;
                    }
                }

                return null;
            }

            function parseConfiguration(configuration) {
                config = libDBE.parseConfigurationObject(configuration);
                config.setServices($q, this);
            }

            /**
             * @param {string} commandName
             * @returns {Command}
             */
            function getCommandFromName(commandName) {
                /** @type{number} */
                var i;

                if (config === null) {
                    throw new Error('No configuration was provided, or the parsing failed.');
                }

                for (i = 0; i < config.commands.length; i = i + 1) {
                    if (config.commands[i].name === commandName) {
                        return config.commands[i];
                    }
                }

                return null;
            }

            function getDefaultData(command) {
                var deferred = $q.defer();
                var i;
                var currentCommand = null;

                if (config === null) {
                    throw new Error('No configuration was provided, or the parsing failed.');
                }

                for (i = 0; i < config.commands.length; i = i + 1) {
                    if (config.commands[i].name === command) {
                        currentCommand = config.commands[i];
                        break;
                    }
                }

                if (currentCommand === null) {
                    throw new Error('Unknown command (' + command + ').');
                }

                currentCommand.getDefaultData(function (err, data) {
                    if (err) {
                        deferred.reject(err);
                    } else {
                        deferred.resolve(data);
                    }
                });

                return deferred.promise;
            }

            function getData(backend, collection, filterField, filterValue, params) {
                var deferred = $q.defer();

                $http.post(getUrl(backend, 'getData'), encodeObject({
                    collection: collection,
                    filterField: filterField,
                    filterValue: filterValue,
                    params: angular.copy(params)
                })).then(function successCallback(res) {
                    deferred.resolve(decodeObject(res.data));
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function countData(backend, collection, params) {
                var deferred = $q.defer();

                $http.post(getUrl(backend, 'countData'), encodeObject({
                    collection: collection,
                    params: angular.copy(params)
                })).then(function successCallback(res) {
                    deferred.resolve(decodeObject(res.data));
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function retrieveData(backend, command, id, idName, page, sortObj, searchObj, params) {
                var deferred = $q.defer();
                var i;
                var currentCommand = null;
                var apiConfiguration = {};

                if (config === null) {
                    throw new Error('No configuration was provided, or the parsing failed.');
                }

                for (i = 0; i < config.commands.length; i = i + 1) {
                    if (config.commands[i].name === command) {
                        currentCommand = config.commands[i];
                        break;
                    }
                }

                if (currentCommand === null) {
                    throw new Error('Unknown command (' + command + ').');
                }

                if (id) {
                    apiConfiguration._id = id;
                }

                if (idName) {
                    apiConfiguration.idName = idName;
                }

                if (page !== undefined && page !== null && currentCommand.paginate !== 4294967295) {
                    apiConfiguration.from = page * currentCommand.paginate;
                    apiConfiguration.to = (page + 1) * currentCommand.paginate;
                }

                if (sortObj !== undefined && sortObj !== null && sortObj.sortOn !== '') {
                    apiConfiguration.sortOn = sortObj.sortOn;
                    apiConfiguration.reverse = sortObj.reverse;
                }

                if (searchObj !== undefined && searchObj !== null && searchObj.searchInput !== '') {
                    apiConfiguration.searchOn = searchObj.searchOn;
                    apiConfiguration.searchInput = searchObj.searchInput;
                }

                $http.post(getUrl(backend, 'retrieveData'), encodeObject({
                    collection: currentCommand.collection,
                    configuration: apiConfiguration,
                    params: angular.copy(params)
                })).then(function successCallback(res) {
                    currentCommand.parseData(decodeObject(res.data.data), id, function (err, resData) {
                        if (err) {
                            deferred.reject(err);
                            return;
                        }

                        // if (apiConfiguration.from !== undefined) {
                        //     $http.post(getUrl(backend, 'countData'), encodeObject({
                        //         collection: currentCommand.collection,
                        //         params: angular.copy(params)
                        //     })).then(function successCallback(res) {
                        //         var count = decodeData(res.data.count);
                        //         resData.data = padResponse(resData.data, count, page * currentCommand.paginate, (page + 1) * currentCommand.paginate);
                        //         deferred.resolve(resData);
                        //     }, function errorCallback(resp) {
                        //         deferred.reject(resp);
                        //     });
                        //     return;
                        // }

                        deferred.resolve(resData);
                    });
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function deleteData(backend, collection, id, idName, params) {
                var deferred = $q.defer();
                var apiConfiguration = {};

                if (!collection || id === undefined) {
                    throw new Error('No collection, or no id was provided.');
                }

                apiConfiguration._id = id;
                if (idName) {
                    apiConfiguration.idName = idName;
                }

                $http.post(getUrl(backend, 'deleteData'), encodeObject({
                    collection: collection,
                    configuration: apiConfiguration,
                    params: angular.copy(params)
                })).then(function successCallback() {
                    deferred.resolve();
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function updateData(backend, collection, _id, id, updatedValues, params) {
                var deferred = $q.defer();
                var i;
                var updateObjectArray = [];

                if (!collection || !id) {
                    throw new Error('No collection, or no id was provided.');
                }

                var cookie = readCookie('lockToken');
                if (cookie === null) {
                    throw new Error('No lock cookie is available.');
                }

                updateObjectArray.push({
                    collection: collection,
                    _id: _id,
                    id: id,
                    updateObject: {}
                });

                for (i = 0; i < updatedValues.length; i = i + 1) {
                    updatedValues[i].options.addValueToUpdateObject(updateObjectArray, angular.copy(updatedValues[i].value));
                }

                i = 0;
                libDBEHelpers.whileAsync( // TODO handle the case we need to revert an operation WHEN 2.0 is working BY Richard
                    function () {
                        return i < updateObjectArray.length;
                    },
                    function (next) {
                        if (Object.keys(updateObjectArray[i].updateObject).length === 0) {
                            i += 1;
                            next();
                            return;
                        }
                        $http.post(getUrl(backend, 'updateData'), encodeObject({
                            collection: updateObjectArray[i].collection,
                            _id: updateObjectArray[i]._id,
                            cookie: cookie,
                            updateObject: updateObjectArray[i].updateObject,
                            id: updateObjectArray[i].id,
                            params: angular.copy(params)
                        })).then(function () {
                            i += 1;
                            next();
                        }, function (resp) {
                            next(resp);
                        });
                    },
                    function (err) {
                        if (err) {
                            deferred.reject(err);
                            return;
                        }
                        deferred.resolve();
                    }
                );

                return deferred.promise;
            }

            function insertData(backend, collection, object, params) {
                var deferred = $q.defer();
                var i;
                var insertObject = {};

                for (i = 0; i < object.length; i = i + 1) {
                    if (object[i].value !== undefined) { // filter unset fields
                        object[i].options.addValueToInsertObject(insertObject, object[i].value);
                    }
                }

                //noinspection JSUnresolvedFunction
                $http.post(getUrl(backend, 'insertData'), encodeObject({
                    collection: collection,
                    object: insertObject,
                    params: angular.copy(params)
                })).then(function successCallback() {
                    deferred.resolve();
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function lockData(backend, collection, id, params) {
                var deferred = $q.defer();
                var cookie = readCookie('lockToken');

                $http.post(getUrl(backend, 'lockData'), encodeObject({
                    collection: collection,
                    _id: id,
                    cookie: cookie,
                    params: angular.copy(params)
                })).then(function successCallback(res) {
                    res = decodeObject(res);
                    document.cookie = 'lockToken=' + res.data.cookie;
                    deferred.resolve(res.data.length);
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            function unlockData(backend, params) {
                var deferred = $q.defer();
                var cookie = readCookie('lockToken');

                $http.post(getUrl(backend, 'unlockData'), encodeObject({
                    cookie: cookie,
                    params: angular.copy(params)
                })).then(function successCallback() {
                    document.cookie = 'lockToken=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    deferred.resolve();
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            return {
                getTypeValues: getTypeValues,
                parseConfiguration: parseConfiguration,
                getCommandFromName: getCommandFromName,
                getDefaultData: getDefaultData,
                getData: getData,
                countData: countData,
                retrieveData: retrieveData,
                deleteData: deleteData,
                updateData: updateData,
                insertData: insertData,
                lockData: lockData,
                unlockData: unlockData
            };
        }
    ]);
