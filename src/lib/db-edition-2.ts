/**
 * Created by richard on 20/12/16.
 */
//declare function require(name: string) : any;
//require('source-map-support').install();

enum DisplayType {
    Component,
    Readonly,
    Text,
    Number,
    Checkbox,
    Checkboxes,
    Date,
    Image,
    BlobImage,
    Dropdown
}

enum DbType {
    String,
    Number,
    Date,
    ObjectId,
    Boolean,
    Array,
    Map
}

enum View {
    Hidden = 0,
    GlobalView = 1,
    DetailedView = 2,
    BothViews = 3
}

export class Helper {
    public static capitalizeFirstLetter(variable: string) {
        return variable.charAt(0).toUpperCase() + variable.slice(1);
    }

    public static isUnset(variable: any) : boolean {
        return variable === null || variable === undefined || variable === "";
    }

    public static isMap(variable: any) : boolean {
        return Object.prototype.toString.call(variable) === "[object Object]";
    }

    public static isArray(variable: any) : boolean {
        return variable instanceof Array === true;
    }

    public static isFunction(variable: any) : boolean {
        return typeof variable === 'function';
    }

    public static isString(variable: any) : boolean {
        return typeof variable === 'string' || variable instanceof String;
    }

    public static isBoolean(variable: any) : boolean {
        return typeof variable === 'boolean' || variable instanceof Boolean;
    }

    public static isNumber(variable: any) : boolean {
        return typeof variable === 'number' || variable instanceof Number;
    }

    public static isDisplayType(variable: any) : boolean {
        if (this.isNumber(variable) && !this.isUnset(DisplayType[variable])) {
            return true;
        } else if (this.isString(variable)) {
            variable = this.capitalizeFirstLetter(variable);
            return variable in DisplayType;
        } else {
            return false;
        }
    }

    public static sanitizeMap(variable: any) : Object {
        if (this.isMap(variable)) {
            return variable;
        } else {
            throw new TypeError('Expected object, found ' + typeof(variable) + '.');
        }
    }

    public static sanitizeArray(variable: any) : Array<any> {
        if (this.isArray(variable)) {
            return variable;
        } else {
            throw new TypeError('Expected array, found ' + typeof(variable) + '.');
        }
    }

    public static sanitizeFunction(variable: any) : (value: any) => any {
        if (this.isFunction(variable)) {
            return variable;
        } else {
            throw new TypeError('Expected function, found ' + typeof(variable) + '.');
        }
    }

    public static sanitizeString(variable: any) : string {
        if (this.isString(variable)) {
            return variable.valueOf();
        } else {
            throw new TypeError('Expected string, found ' + typeof(variable) + '.');
        }
    }

    public static sanitizeBoolean(variable: any) : boolean {
        if (this.isBoolean(variable)) {
            return variable.valueOf();
        } else {
            throw new TypeError('Expected boolean, found ' + typeof(variable) + '.');
        }
    }

    public static sanitizeNumber(variable: any) : number {
        if (this.isNumber(variable)) {
            return variable.valueOf();
        } else {
            throw new TypeError('Expected number, found ' + typeof(variable) + '.');
        }
    }

    public static sanitizeDisplayType(variable: any) : DisplayType {
        if (this.isNumber(variable)) {
            if (DisplayType[variable]) {
                return variable;
            } else {
                throw new TypeError('Unknown value (' + variable + ') for the enum DisplayType.');
            }
        } else if (this.isString(variable)) {
            variable = Helper.capitalizeFirstLetter(variable);
            if (variable in DisplayType) {
                return <DisplayType><any>DisplayType[variable];
            } else {
                throw new TypeError('Unknown value (' + variable + ') for the enum DisplayType.')
            }
        } else {
            throw new TypeError('Expected a string or a number to parse the enum DisplayType but found ' + typeof variable + '.')
        }
    }

    public static sanitizeDbType(variable: any) : DbType {
        if (this.isNumber(variable)) {
            if (DbType[variable]) {
                return variable;
            } else {
                throw new TypeError('Unknown value (' + variable + ') for the enum DbType.');
            }
        } else if (this.isString(variable)) {
            variable = Helper.capitalizeFirstLetter(variable);
            if (variable in DbType) {
                return <DbType><any>DbType[variable];
            } else {
                throw new TypeError('Unknown value (' + variable + ') for the enum DbType.')
            }
        } else {
            throw new TypeError('Expected a string or a number to parse the enum DbType but found ' + typeof variable + '.')
        }
    }

    public static sanitizeView(variable: any) : View {
        if (this.isNumber(variable)) {
            if (View[variable]) {
                return variable;
            } else {
                throw new TypeError('Unknown value (' + variable + ') for the enum View.');
            }
        } else if (this.isString(variable)) {
            variable = Helper.capitalizeFirstLetter(variable);
            if (variable in View) {
                return <View><any>View[variable];
            } else {
                throw new TypeError('Unknown value (' + variable + ') for the enum View.')
            }
        } else {
            throw new TypeError('Expected a string or a number to parse the enum View but found ' + typeof variable + '.')
        }
    }

    public static passThrough(variable: any) : any {
        return variable;
    }

    public static joinAccessors(str1 : string, str2 : string) : string {
        if (!str1 || str1 === '')
            return str2;

        if (!str2 || str2 === '')
            return str1;

        return str1 + '.' + str2;
    }

    public static whileAsync(test: () => boolean, iterate: (n: any) => void, callback: (err: Error) => void) {
        if (!test()) {
            callback(null);
        } else {
            var next = function (err: Error) {
                if (err || !test()) {
                    callback(err);
                } else {
                    iterate(next);
                }
            };
            iterate(next);
        }
    }

    public static clone<T>(originalObject: T, circular: boolean) : T {
        // First create an empty object with
        // same prototype of our original source

        var propertyIndex : any;
        var descriptor : any;
        var keys : any;
        var current : any;
        var nextSource : any;
        var indexOf : any;
        var copies = [{
            source: originalObject,
            target: Array.isArray(originalObject) ? [] : Object.create(Object.getPrototypeOf(originalObject))
        }];
        var cloneObject = copies[0].target;
        var sourceReferences = [originalObject];
        var targetReferences = [cloneObject];

        // First in, first out
        while (current = copies.shift()) {	// jshint ignore:line
            keys = Object.getOwnPropertyNames(current.source);

            for (propertyIndex = 0; propertyIndex < keys.length; propertyIndex += 1) {
                // Save the source's descriptor
                descriptor = Object.getOwnPropertyDescriptor(current.source, keys[propertyIndex]);

                if (! descriptor.value || typeof descriptor.value !== 'object'){
                    Object.defineProperty(current.target, keys[propertyIndex], descriptor);
                    continue;
                }

                nextSource = descriptor.value;
                descriptor.value = Array.isArray(nextSource) ? [] : Object.create(Object.getPrototypeOf(nextSource));

                if (circular) {
                    indexOf = sourceReferences.indexOf(nextSource);

                    if (indexOf !== -1) {
                        // The source is already referenced, just assign reference
                        descriptor.value = targetReferences[indexOf];
                        Object.defineProperty(current.target, keys[propertyIndex], descriptor);
                        continue;
                    }

                    sourceReferences.push(nextSource);
                    targetReferences.push(descriptor.value);
                }

                Object.defineProperty(current.target, keys[propertyIndex], descriptor);

                copies.push({ source: nextSource, target: descriptor.value });
            }
        }

        return cloneObject;
    }
}

export class Configuration {
    public commands: Command[];

    private promiseService: any;
    private DBEService: any;
    private requestCache: any;
    public $nameCache: any;

    public constructor() {
        this.commands = [];

        this.promiseService = null;
        this.DBEService = null;

        this.requestCache = {};
        this.$nameCache = {};
    };

    public getData(backend: string, collection: string, filterField: string, filterValue: any, params: any, callback: (err: Error, data?: any) => void) {
        if (this.DBEService === null || this.promiseService === null) {
            throw new Error('Get data was called but the services were not set.');
        }

        var cacheString : string = backend + '_' + collection + '_' + filterField + '_' + filterValue;

        if (this.requestCache[cacheString]) {
            callback(null, this.requestCache[cacheString]);
            return;
        }

        this.DBEService.getData(backend, collection, filterField, filterValue, params).then((data: any) => {
            this.requestCache[cacheString] = data;
            callback(null, data);
        }, function (err: Error) {
            callback(err);
        });
    }

    public clearCache() : void {
        this.requestCache = {};
    }

    public retrieveData(backend: string, command: string, id: string, apiConfiguration: any) : any {
        if (this.DBEService === null || this.promiseService === null) {
            throw new Error('Retrieve data was called but the services were not set.');
        }
        var i: number;
        var d = this.promiseService.defer();

        for (i = 0; i < this.commands.length; i = i + 1) { // will break upon the first match
            if (this.commands[i].name === command) {
                this.DBEService.retrieveData(backend, command, id, apiConfiguration).then(function (data: any) {
                    //noinspection JSReferencingMutableVariableFromClosure
                    d.resolve(this.commands[i].parseData(data, id)); // accessible from closure, but ok => we break when we found it;
                }, function (err: any) {
                    d.reject(err);
                });
                return d.promise;
            }
        }

        d.reject(new Error('Command not found.'));
        return d.promise;
    }

    public setServices(promiseService: any, DBEService: any) {
        this.promiseService = promiseService;
        this.DBEService = DBEService;
    }

    public static instance: Configuration;

    public static parseConfigurationObject(configurationObject: any) : Configuration {
        if (Helper.isArray(configurationObject) === false) {
            throw new SyntaxError('Commands should be an array.')
        }

        var resConfiguration = new Configuration();
        var i: number;

        this.instance = resConfiguration;

        // parse commands
        for (i = 0; i < configurationObject.length; i = i + 1) {
            resConfiguration.commands.push(Command.parseCommandObject(configurationObject[i]));
        }

        return resConfiguration;
    }
}

class Command {
    // Core data REQUIRED
    public name: string; // state param
    public collection: string;
    public backend: string;
    public params: any; // this will be given as is to the backend

    public dataNodes: DataNode[];
    public components: Component[];

    // ID field
    public id: string;

    // Configuration options
    public paginate: number;
    public sort: boolean;
    public search: boolean;
    public edit: boolean;
    public confirmEdit: boolean;
    public del: boolean;
    public detailedView: boolean;
    public add: boolean;
    public disableInitialDataFetch: boolean;
    
    // Aggregation functions
    public aggregate: Aggregator[];

    // Title related
    public globalTitle: string;
    public detailedTitle: string;
    public addTitle: string;

    public constructor(name: string, collection: string, backend: string) { // sanity check done in the static method
        this.name = name;
        this.collection = collection;
        this.backend = backend;
        this.dataNodes = [];
        this.components = [];
        this.aggregate = [];

        this.id = '_id';

        // initialize fields
        this.paginate = 4294967295;
        this.sort = false;
        this.search = false;
        this.edit = false;
        this.del = false;
        this.detailedView = false;
        this.add = false;
        this.globalTitle = null;
        this.detailedTitle = null;
        this.addTitle = null;
    };

    public parseData(data: any, id: string, callback: (err: Error, res: any) => void) {
        if (Helper.isUnset(id)) {
            this.parseDataAsGlobal(data, callback);
        } else {
            this.parseDataAsDetailed(data, id, callback);
        }

        Configuration.instance.clearCache();
    }

    public parseDataAsGlobal(data: any, callback: (err: Error, res: any) => void) {
        var res: any = {};
        var i: number;
        var j: number;
        var node: DataNode;

        res.options = this;
        res.data = [];
        res.header = this.getGlobalHeader();

        for (j = 0; j < data.length; j++) {
            res.data.push({_id: data[j][this.id], columns: []});
        }

        i = 0;
        Helper.whileAsync(
            () => i < this.dataNodes.length,
            outerNext => {
                node = this.dataNodes[i];
                if (node.view === View.GlobalView || node.view === View.BothViews) {
                    j = 0;
                    Helper.whileAsync(
                        () => j < data.length,
                        innerNext => {
                            node.readValue(data[j], null, function (err: Error, value?: any) {
                                if (err) {
                                    innerNext(err);
                                    return;
                                }

                                res.data[j].columns.push(value);
                                j += 1;
                                innerNext();
                            }); //TODO add contextual check on Node, to refuse in the global view the non leaves nodes.
                        },
                        err => {
                            i += 1;
                            outerNext(err);
                        }
                    );
                } else {
                    i += 1;
                    outerNext();
                }
            },
            err => callback(err, res)
        );
    }

    public getGlobalHeader() : any {
        var i: number;
        var header: any = [];
        var node: DataNode;

        for (i = 0; i < this.dataNodes.length; i += 1) {
            node = this.dataNodes[i];
            if (node.view === View.GlobalView || node.view === View.BothViews) {
                header.push(node.getHeader());
            }
        }

        return header;
    }

    public getOptions() : any {
        return this;
    }

    public parseDataAsDetailed(data: any, id: string, callback: (err: Error, res: any) => void) {
        var res: any = {};
        var i: number;
        var node: DataNode;

        res.options = this;
        res.data = [];
        res._id = id;

        data = data[0]; // we parse a detailed data, so the count must be 1

        i = 0;
        Helper.whileAsync(
            () => i < this.dataNodes.length,
            next => {
                node = this.dataNodes[i];
                if (node.view === View.DetailedView || node.view === View.BothViews) {
                    node.readValue(data, null, function (err, value) {
                        if (err) {
                            next(err);
                            return;
                        }

                        i += 1;
                        res.data.push(value);
                        next();
                    })
                } else {
                    i += 1;
                    next();
                }
            },
            err => callback(err, res)
        );
    }

    public getDefaultData(callback: (err: Error, res: any) => void) {
        var res: any = {};
        var i: number;
        // var node: DataNode;

        res.options = this;
        res.data = [];

        i = 0;
        while (i < this.dataNodes.length) {
            res.data.push(this.dataNodes[i].getDefaultValue(null, null));
            i += 1;
        }

        callback(null, res);
    }

    public static parseCommandObject(viewObject: any) : Command {
        var i: number;
        var key: string;

        if (Helper.isUnset(viewObject.name)) {
            throw new SyntaxError('A command must have a name.')
        }
        if (Helper.isUnset(viewObject.collection)) {
            throw new SyntaxError('A command must have a collection.')
        }

        var command = new Command(viewObject.name, viewObject.collection, viewObject.backend);

        for (key in viewObject) {
            if (viewObject.hasOwnProperty(key)) {
                switch (key) {
                    case 'name':
                    case 'collection':
                    case 'backend':
                        break;
                    case 'params':
                        command.params = viewObject.params;
                        break;
                    case 'id':
                        command.id = Helper.sanitizeString(viewObject.id);
                        break;
                    case 'nodes':
                        for (i = 0; i < viewObject.nodes.length; i = i + 1) {
                            command.dataNodes.push(DataNode.parseDataNodeObject(viewObject.nodes[i], command));
                        }
                        break;
                    case 'components':
                        for (i = 0; i < viewObject.components.length; i = i + 1) {
                            command.components.push(Component.parseComponentObject(viewObject.components[i], command));
                        }
                        break;
                    case 'paginate':
                        command.paginate = Helper.sanitizeNumber(viewObject.paginate);
                        if (command.paginate < 0) {
                            throw new SyntaxError('Paginate cannot have a negative value.');
                        }
                        break;
                    case 'sort':
                        command.sort = Helper.sanitizeBoolean(viewObject.sort);
                        break;
                    case 'search':
                        command.search = Helper.sanitizeBoolean(viewObject.search);
                        break;
                    case 'aggregate':
                        for (i = 0; i < viewObject.aggregate.length; i = i + 1) {
                            command.aggregate.push(Aggregator.parseAggregatorObject(viewObject.aggregate[i], command));
                        }
                        break;
                    case 'edit':
                        command.edit = Helper.sanitizeBoolean(viewObject.edit);
                        break;
                    case 'confirmEdit':
                        command.confirmEdit = Helper.sanitizeBoolean(viewObject.confirmEdit);
                        break;
                    case 'del':
                        command.del = Helper.sanitizeBoolean(viewObject.del);
                        break;
                    case 'detailedView':
                        command.detailedView = Helper.sanitizeBoolean(viewObject.detailedView);
                        break;
                    case 'add':
                        command.add = Helper.sanitizeBoolean(viewObject.add);
                        break;
                    case 'disableInitialDataFetch':
                        command.disableInitialDataFetch = Helper.sanitizeBoolean(viewObject.disableInitialDataFetch);
                        break;
                    case 'globalTitle':
                        command.globalTitle = Helper.sanitizeString(viewObject.globalTitle);
                        break;
                    case 'detailedTitle':
                        command.detailedTitle = Helper.sanitizeString(viewObject.detailedTitle);
                        break;
                    case 'addTitle':
                        command.addTitle = Helper.sanitizeString(viewObject.addTitle);
                        break;
                    default:
                        throw new SyntaxError('Unrecognized key ' + key + '.');
                }
            }
        }

        //TODO add contextual checks
        return command;
    }
}

class Aggregator {
    // For global data
    private command: Command;

    public name: string;
    public aggregationFunction: (value: any) => any;
    public bgColor: string;

    public constructor(command: Command) {
        this.command = command;

        this.name = null;
        this.aggregationFunction = null;
        this.bgColor = '0xffffff';
    }

    public static parseAggregatorObject(aggregatorObject: any, command: Command) : Aggregator {
        var resAggregator = new Aggregator(command);

        if (Helper.isUnset(aggregatorObject.name)) {
            throw new SyntaxError('An aggregator must have a name.')
        }
        if (Helper.isUnset(aggregatorObject.aggregationFunction)) {
            throw new SyntaxError('An aggregator must have an aggregationFunction.')
        }

        if (!Helper.isString(aggregatorObject.name)) {
            throw new SyntaxError('A name is supposed to be a string (found ' + typeof aggregatorObject.name + ').')
        }

        if (!Helper.isFunction(aggregatorObject.aggregationFunction)) {
            throw new SyntaxError('Am aggregationFunction is supposed to be a function (found ' + typeof aggregatorObject.aggregationFunction + ').')
        }

        if (Helper.isString(aggregatorObject.bgColor)) {
            resAggregator.bgColor = aggregatorObject.bgColor;
        }

        resAggregator.name = aggregatorObject.name;
        resAggregator.aggregationFunction = aggregatorObject.aggregationFunction;
        return resAggregator;
    }
}

class DataNode {
    // For global data
    private command: Command;

    // Core data
    public name: string;
    public displayAs: DataNode[]|DisplayType;
    public accessor: Accessor[];

    // Translation on the fly
    public translationToDisplay: (value: any) => any; // db -> display
    public translationToDB: (value: any) => any; // display -> db

    // Configuration options
    public saveAs: DbType; // if set the field is editable
    public extensible: boolean;
    public searchable: boolean;
    public sortable: boolean;
    public view: View;
    private iterate: boolean;
    private hideKey: boolean;
    private defaultValue: any;

    // Saved on iterate
    private $keys: string;
    private $values: string;

    // Data predefined in a set
    public values: string[]|string;

    // Aggregation
    public aggregatable: boolean[];

    // Advanced options
    // public $name: string; // to reference an iteration with $
    // public alias: string;

    private isLeaf: boolean;

    public constructor(command: Command) {
        this.command = command;

        this.name = null;
        this.displayAs = null;
        this.accessor = [];

        this.translationToDisplay = null;
        this.translationToDB = null;

        this.saveAs = null;
        this.extensible = false;
        this.searchable = false;
        this.sortable = false;
        this.view = null;
        this.defaultValue = null;
        // this.$name = null;
        // this.alias = null;

        this.aggregatable = [];
        this.values = null;

        this.isLeaf = false;

        this.iterate = false;
        this.hideKey = false;
        this.$keys = null;
        this.$values = null;
    }

    public clone(): DataNode {
        return Object.create(this);
    }

    public getHeader() : any {
        return {name: this.name, searchable: this.searchable, sortable: this.sortable, aggregatable: this.aggregatable, options: this};
    }

    public static parseDataNodeObject(dataNodeObject: any, command: Command) : DataNode {
        var key: string;
        var i: number;
        var resDataNode = new DataNode(command);
        var tmpDataNode: DataNode;

        for (key in dataNodeObject) {
            if (dataNodeObject.hasOwnProperty(key)) {
                switch (key) {
                    case 'name':
                        resDataNode.name = Helper.sanitizeString(dataNodeObject.name);
                        break;
                    case 'displayAs':
                        if (Helper.isDisplayType(dataNodeObject.displayAs)) {
                            resDataNode.isLeaf = true;
                            resDataNode.displayAs = Helper.sanitizeDisplayType(dataNodeObject.displayAs);
                        } else if (Helper.isArray(dataNodeObject.displayAs)) {
                            resDataNode.displayAs = [];
                            for (i = 0; i < dataNodeObject.displayAs.length; i = i + 1) {
                                dataNodeObject.displayAs[i].view = Helper.sanitizeView(dataNodeObject.view); // TODO, remove this duplicate
                                tmpDataNode = DataNode.parseDataNodeObject(dataNodeObject.displayAs[i], command);
                                (<DataNode[]>resDataNode.displayAs).push(tmpDataNode);
                            }
                        } else {
                            throw new TypeError('Expected displayAs to be a DisplayType or an array but found ' + typeof(dataNodeObject.displayAs) + '. (' + dataNodeObject.displayAs + ')');
                        }
                        break;
                    case 'accessor':
                        if (Helper.isString(dataNodeObject.accessor)) {
                            dataNodeObject.accessor = dataNodeObject.accessor.split('.');
                        }

                        if (Helper.isArray(dataNodeObject.accessor)) {
                            for (i = 0; i < dataNodeObject.accessor.length; i = i + 1) {
                                resDataNode.accessor.push(Accessor.parseAccessorObject(dataNodeObject.accessor[i], command))
                            }
                        } else {
                            throw new TypeError('Expected the accessor to be a string or an array but found ' + typeof(dataNodeObject.accessor) + '.');
                        }
                        break;
                    case 'translationToDisplay':
                        resDataNode.translationToDisplay = Helper.sanitizeFunction(dataNodeObject.translationToDisplay);
                        break;
                    case 'translationToDB':
                        resDataNode.translationToDB = Helper.sanitizeFunction(dataNodeObject.translationToDB);
                        break;
                    case 'saveAs':
                        resDataNode.saveAs = Helper.sanitizeDbType(dataNodeObject.saveAs);
                        if (resDataNode.defaultValue === null) {
                            resDataNode.defaultValue = this.getDefaultValue(resDataNode.saveAs);
                        }
                        break;
                    case 'extensible':
                        resDataNode.extensible = Helper.sanitizeBoolean(dataNodeObject.extensible);
                        break;
                    case 'searchable':
                        resDataNode.searchable = Helper.sanitizeBoolean(dataNodeObject.searchable);
                        break;
                    case 'sortable':
                        resDataNode.sortable = Helper.sanitizeBoolean(dataNodeObject.sortable);
                        break;
                    case 'aggregatable':
                        for (i = 0; i < dataNodeObject.aggregatable.length; i = i + 1) {
                            resDataNode.aggregatable.push(Helper.sanitizeBoolean(dataNodeObject.aggregatable[i]))
                        }
                        break;
                    case 'view':
                        resDataNode.view = Helper.sanitizeView(dataNodeObject.view);
                        break;
                    case 'values':
                        if (Helper.isArray(dataNodeObject.values)) {
                            resDataNode.values = [];
                            for (i = 0; i < dataNodeObject.values.length; i = i + 1) {
                                (<string[]>resDataNode.values).push(Helper.sanitizeString(dataNodeObject.values[i]));
                            }
                        } else if (Helper.isString(dataNodeObject.values)) {
                            resDataNode.values = dataNodeObject.values;
                        } else {
                            throw new TypeError('Expected the values to be an array or a $name but found ' + typeof(dataNodeObject.values) + '.');
                        }
                        break;
                    case 'iterate':
                        resDataNode.iterate = Helper.sanitizeBoolean(dataNodeObject.iterate);
                        break;
                    case 'hideKey':
                        resDataNode.hideKey = Helper.sanitizeBoolean(dataNodeObject.hideKey);
                        break;
                    case 'saveKeys':
                        resDataNode.$keys = Helper.sanitizeString(dataNodeObject.saveKeys);
                        Configuration.instance.$nameCache[resDataNode.$keys] = [];
                        break;
                    case 'saveValues':
                        resDataNode.$values = Helper.sanitizeString(dataNodeObject.saveValues);
                        Configuration.instance.$nameCache[resDataNode.$values] = [];
                        break;
                    case 'defaultValue':
                        resDataNode.defaultValue = dataNodeObject.defaultValue;
                        break;
                    default:
                        throw new SyntaxError('Unrecognized key ' + key + '.');
                }
            }
        }

        resDataNode.contextualChecks();
        resDataNode.generateTranslationFunctions();
        return resDataNode;
    }

    private static getDefaultValue(saveAs: DbType): any {
        switch (saveAs) {
            case DbType.String:
                return '';
            case DbType.Number:
                return 0;
            case DbType.Date:
                return new Date();
            case DbType.ObjectId:
                return null; // TODO
            case DbType.Boolean:
                return false;
            case DbType.Array:
                return null;
            case DbType.Map:
                return null; // have to be specifically overridden in the case of checkboxes
        }

        return null;
    }

    private contextualChecks() {
        if (this.extensible) {
            if (this.saveAs !== null) {
                if (this.saveAs !== DbType.Array && this.saveAs !== DbType.Map) {
                    throw new SyntaxError('Extensible is only valid with Maps and Arrays.');
                }
            }
        }

        if (this.displayAs === DisplayType.Dropdown && Helper.isUnset(this.values)) {
            throw new SyntaxError('displayAs: "dropdown" requires values or a $name to be defined.'); // $name => reference a name that was register after a $values or $keys
        }
    }

    private generateTranslationFunctions() {
        if (this.translationToDB === null) {
            switch (this.saveAs) {
                case DbType.String:
                    this.translationToDB = function(v) { return v.toString(); };
                    break;
                case DbType.Number:
                    this.translationToDB = function(v) { return parseInt(v, 10); };
                    break;
                case DbType.Date:
                    this.translationToDB = function(v) { return new Date(v); };
                    break;
                case DbType.ObjectId:
                    this.translationToDB = Helper.passThrough;
                    break;
                case DbType.Boolean:
                    this.translationToDB = function(v) { return v === true || v === 'true' || v === 1; };
                    break;
                case DbType.Array:
                    this.translationToDB = Helper.passThrough;
                    break;
                case DbType.Map:
                    this.translationToDB = Helper.passThrough;
                    break;
            }
        }

        if (this.translationToDisplay === null) {
            switch (this.displayAs) {
                case DisplayType.Component:
                    this.translationToDisplay = Helper.passThrough;
                    break;
                case DisplayType.Readonly:
                    this.translationToDisplay = function(v) {
                        if (!v) {
                            return '';  // no value
                        }
                        return v.toString();
                    };
                    break;
                case DisplayType.Text:
                    this.translationToDisplay = function(v) {
                        if (!v) {
                            return '';  // no value
                        }
                        return v.toString();
                    };
                    break;
                case DisplayType.Number:
                    this.translationToDisplay = function(v) { return parseInt(v, 10); };
                    break;
                case DisplayType.Checkbox:
                    this.translationToDisplay = function(v) { return v === true || v === 'true' || v === 1; };
                    break;
                case DisplayType.Checkboxes:
                    this.translationToDisplay = Helper.passThrough;
                    break;
                case DisplayType.Date:
                    this.translationToDisplay = Helper.passThrough;
                    break;
                case DisplayType.Image:
                    this.translationToDisplay = Helper.passThrough;
                    break;
                case DisplayType.BlobImage:
                    this.translationToDisplay = Helper.passThrough;
                    break;
                case DisplayType.Dropdown:
                    this.translationToDisplay = function(v) { return v.toString(); };
                    break;
            }
        }
    }

    public getDefaultValue(value: any, key: string) : any {
        var i: number;
        var nodes: DataNode[];
        var res: any;
        var newOptions: any;

        if (value === null) {
            value = this.defaultValue;
        }

        if (this.displayAs === DisplayType.Component) { // Only if not a component
            return;
        }

        if (key !== null && this.displayAs === DisplayType.Readonly) {
            newOptions = this.clone(); // we alter something hence the copy
            newOptions.name = key;
            newOptions.displayAs = DisplayType.Text;
        } else if (this.displayAs === DisplayType.Readonly) {
            newOptions = this.clone();
            newOptions.displayAs = DisplayType.Text;
        } else if (key !== null) {
            newOptions = this.clone();
            newOptions.name = key;
        } else {
            newOptions = this;
        }

        if (this.iterate) {
            return {
                value: this.iterateReadDefaultValue(value),
                options: newOptions,
                key: key
            };
        } else if (this.isLeaf) {
            return {
                value: value,
                options: newOptions,
                key: key
            };
        } else {
            res = [];
            nodes = <DataNode[]>this.displayAs;

            for (i = 0; i < nodes.length; i += 1) {
                res.push(nodes[i].getDefaultValue(value, null));
            }

            return {
                value: res,
                options: newOptions,
                key: key
            };
        }
    }

    private iterateReadDefaultValue(current: any) : any {
        var key: string;
        var res: any = [];

        for (key in current) {
            if (current.hasOwnProperty(key)) {
                res.push((<DataNode[]>this.displayAs)[0].getDefaultValue(current[key], key));

            }
        }

        return res;
    }

    public readValue(row: any, key: string, callback: (err: Error, value?: any) => void) : void {
        var i: number;
        var current: any = row;
        var nodes: DataNode[];
        var res: any;
        var newOptions: any;

        if (this.displayAs === DisplayType.Component) { // Only if not a component
            callback(null, {
                value: current,
                options: Helper.clone(this, true),
                key: key
            });
            return;
        }

        i = 0;
        Helper.whileAsync(
            () => this.accessor === null ? false : i < this.accessor.length,
            outerNext => {
                this.accessor[i].accessData(current, function (err, newValue) {
                    if (err) {
                        outerNext(err);
                        return;
                    }

                    current = newValue;
                    i += 1;
                    outerNext();
                });
            },
            err => {
                if (err) {
                    callback(err);
                    return;
                }

                newOptions = Helper.clone(this, true);
                if (key !== null) {
                    newOptions.name = key;
                }

                if (this.iterate) {
                    this.iterateFunc(current,  function (err, value) {
                        if (err) {
                            callback(err);
                            return;
                        }
                        callback(null, {
                            value: value,
                            options: newOptions,
                            key: key
                        })
                    });
                } else if (this.isLeaf) {
                    callback(null, {
                        value: this.translationToDisplay(current),
                        options: newOptions,
                        key: key
                    });
                } else {
                    res = [];
                    nodes = <DataNode[]>this.displayAs;

                    i = 0;
                    Helper.whileAsync(
                        () => i < nodes.length,
                        innerNext => {
                            nodes[i].readValue(current, null, function (err, value) {
                                if (err) {
                                    innerNext(err);
                                    return;
                                }

                                i += 1;
                                res.push(value);
                                innerNext();
                            });
                        },
                        err => {
                            if (err) {
                                callback(err);
                                return;
                            }
                            callback(null, {
                                value: res,
                                options: newOptions,
                                key: key
                            })
                        }
                    );
                }
            }
        );
    }

    private iterateFunc(current: any, callback: (err: Error, value?: any) => void) : void {
        var key: string;
        var keys: string[] = [];
        var res: any = [];
        var i: number;

        for (key in current) {
            if (current.hasOwnProperty(key)) {
                keys.push(key);

                if (this.$keys !== null) {
                    Configuration.instance.$nameCache[this.$keys].push(key);
                }
            }
        }

        i = 0;
        Helper.whileAsync(
            () => i < keys.length,
            next => {
                (<DataNode[]>this.displayAs)[0].readValue(current[keys[i]], keys[i], (err, value) => {
                    if (err) {
                        next(err);
                        return;
                    }
                    i += 1;
                    res.push(value);
                    if (this.$values !== null) {
                        Configuration.instance.$nameCache[this.$values].push(value.value);
                    }
                    next();
                });
            },
            err => callback(err, res)
        );
    }

    public static getUpdateObject(updateObjectArray: any, collection: string, _id: any, id: string) : any {
        var i: number;
        var updateObject: any;

        for (i = 0; i < updateObjectArray.length; i += 1) {
            if (collection === updateObjectArray[i].collection) {
                return updateObjectArray[i].updateObject;
            }
        }

        updateObject = {};
        updateObjectArray.push({
            collection: collection,
            _id: _id,
            updateObject: updateObject,
            id: id
        });

        return updateObject;
    }

    public getAccessor() : string {
        var currentAccessor: string = '';
        var i: number;

        for (i = 0; i < this.accessor.length; i += 1) {
            if (this.accessor[i].isJumping()) {
                return null; // TODO handle the case we have to filter/sort on another collection
            } else {
                currentAccessor = Helper.joinAccessors(currentAccessor, this.accessor[i].getAccessor());
            }
        }

        return currentAccessor;
    }

    public addValueToInsertObject(insertObject: any, value: any) {
        var i: number;
        var nodes: DataNode[];
        var accessor: string;

        if (value === null) {
            return;
        }

        if (this.displayAs === DisplayType.Component) {
            return;
        }

        for (i = 0; i < this.accessor.length; i += 1) {
            if (this.accessor[i].isJumping()) {
                return;
            }
        }

        for (i = 0; i < this.accessor.length - 1; i += 1) {
            accessor = this.accessor[i].getAccessor();
            if (insertObject[accessor] === undefined) {
                insertObject[accessor] = {};
            }
            insertObject = insertObject[accessor];
        }
        accessor = this.accessor[i].getAccessor();

        if (this.iterate || this.isLeaf) {
            insertObject[accessor] = this.translationToDB(value);
        } else {
            nodes = <DataNode[]>this.displayAs;
            for (i = 0; i < nodes.length; i += 1) {
                nodes[i].addValueToInsertObject(insertObject, value[i].value);
            }
        }
    }

    public addValueToUpdateObject(updateObjectArray: any, value: any, prefixWith: string) {
        var i: number;
        var currentAccessor: string = prefixWith || '';
        var nodes: DataNode[];
        var updateObject: any = updateObjectArray[0].updateObject;

        if (this.displayAs === DisplayType.Component || this.displayAs === DisplayType.Readonly || this.view === View.Hidden) {
            return;
        }

        for (i = 0; i < this.accessor.length; i += 1) {
            if (this.accessor[i].isJumping()) {
                currentAccessor = '';
                prefixWith = '';

                updateObject = DataNode.getUpdateObject(updateObjectArray, this.accessor[i].getCollection(), this.accessor[i].getId(), this.command.id);
            } else {
                currentAccessor = Helper.joinAccessors(currentAccessor, this.accessor[i].getAccessor());
            }
        }

        if (this.iterate) {
            for (i = 0; i < value.length; i += 1) {
                (<DataNode[]>this.displayAs)[0].addValueToUpdateObject(updateObjectArray, value[i].value, currentAccessor + '.' + value[i].key);
            }
        } else {
            if (this.isLeaf) {
                updateObject[currentAccessor] = this.translationToDB(value);
            } else {
                nodes = <DataNode[]>this.displayAs;
                for (i = 0; i < nodes.length; i += 1) {
                    nodes[i].addValueToUpdateObject(updateObjectArray, value[i].value, currentAccessor);
                }
            }
        }
    }

    public getValues() : string[] {
        if (Helper.isArray(this.values)) {
            return <string[]>this.values;
        }

        return Configuration.instance.$nameCache[<string>this.values];
    }
}

class Accessor {
    private command: Command;

    private backend: string;
    private accessor: string;
    private collection: string;
    private transtypingFunction: (value: any) => any;
    private _id: any; // set in case we accessed another collection

    public constructor(accessor: string, command: Command) {
        this.command = command;

        this.accessor = Helper.sanitizeString(accessor);
        this.collection = null;
        this.transtypingFunction = Helper.passThrough;
        this._id = null;
    }

    public static parseAccessorObject(accessorObject: any, command: Command) : Accessor {
        var key: string;
        var resAccessor : Accessor;

        if (Helper.isString(accessorObject)) {
            return new Accessor(accessorObject, command);
        }

        if (Helper.isUnset(accessorObject.accessor)) {
            throw new SyntaxError('An accessor object must have an accessor field.')
        }

        resAccessor = new Accessor(Helper.sanitizeString(accessorObject.accessor), command);

        for (key in accessorObject) {
            if (accessorObject.hasOwnProperty(key)) {
                switch (key) {
                    case 'accessor':
                        break;
                    case 'collection':
                        resAccessor.collection = Helper.sanitizeString(accessorObject.collection);
                        break;
                    case 'transtypingFunction':
                        resAccessor.transtypingFunction = Helper.sanitizeFunction(accessorObject.transtypingFunction);
                        break;
                    default:
                        throw new SyntaxError('Unrecognized key ' + key + '.');
                }
            }
        }

        return resAccessor;
    }

    public accessData(rowOrSubrow: any, callback: (err: Error, value?: any) => void) : void {
        if (this.collection === null) {
            callback(null, rowOrSubrow[this.accessor]);
        } else {
            Configuration.instance.getData(this.command.backend, this.collection, this.accessor, rowOrSubrow, this.command.params, (err, value) => {
                if (value) {
                    this._id = value[this.command.id];
                }
                callback(err, value);
            });
        }
    }

    public accessDataSync(rowOrSubrow: any) : any {
        return rowOrSubrow[this.accessor];
    }

    public isJumping() : boolean {
        return this.collection != null;
    }

    public getCollection() : string {
        return this.collection;
    }

    public getId() : any {
        return this._id;
    }

    public getAccessor() : string {
        return this.accessor;
    }
}

class Component {
    private command: Command;

    public name: string;
    public anchorToTop: boolean;
    public view: View;
    public options: any;

    public constructor(name: string, command: Command) {
        this.command = command;

        this.name = name.split(/(?=[A-Z])/).join('-'); // to instantiate a component, we need it in the form db-edition, not dbEdition

        this.anchorToTop = false;
        this.view = 0;
        this.options = null;
    }

    public static parseComponentObject(componentObject: any, command: Command) : Component {
        var key: string;
        var resComponent : Component;

        if (Helper.isUnset(componentObject.name)) {
            throw new SyntaxError('A component must have a name.')
        }

        resComponent = new Component(Helper.sanitizeString(componentObject.name), command);

        for (key in componentObject) {
            if (componentObject.hasOwnProperty(key)) {
                switch (key) {
                    case 'name':
                        break;
                    case 'anchorToTop':
                        resComponent.anchorToTop = Helper.sanitizeBoolean(componentObject.anchorToTop);
                        break;
                    case 'view':
                        if (componentObject.view in View) { // leaf
                            if (Helper.isString(componentObject.view)) {
                                resComponent.view = <View><any>View[componentObject.view];
                            } else {
                                resComponent.view = componentObject.view;
                            }
                        } else {
                            throw new TypeError('Unrecognized view ' + componentObject.view + '.');
                        }
                        break;
                    case 'options':
                        resComponent.options = componentObject.options;
                        break;
                    default:
                        throw new SyntaxError('Unrecognized key ' + key + '.');
                }
            }
        }

        return resComponent;
    }
}
