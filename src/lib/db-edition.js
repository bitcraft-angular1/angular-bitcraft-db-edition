/**
 * Created by richard on 24/10/16.
 */

'use strict';

/**
 * Construct the data as it can be represented easily inside tables.
 * @param data {Object} data received by a call to mongodb
 * @param config {Object} configuration object (db-edition.conf.js)
 * @param globals {Object} contains variables to be passed at every level like $log
 * @param map {Object} store the aliases
 * @returns {Array}
 */
function parseRoot(data, config, globals, map) {
    var res = [];
    var pc;
    var node;

    for (node in config) {
        if (!config.hasOwnProperty(node)) {
            continue;
        }

        // treat node
        pc = {};
        res.push(pc);
        parseNode(pc, node, data, config[node].field, config[node].type, config[node].alias, config[node].translation, globals, map, config[node].extensible);
    }
    return res;
}

/**
 * Construct a Node or a Leaf of data.
 * @param placeholder {Object} destination of the node
 * @param nameOfNode {string} name of the node (as given by the user)
 * @param data {Object} data received by a call to mongodb
 * @param accessor {string} match the field 'field' of db-edition.conf.js
 * @param type {string|Object} match the field 'type' of db-edition.conf.js
 * @param alias {string} match the field 'alias' of db-edition.conf.js
 * @param translation {Array} match the field 'translation' of db-edition.conf.js
 * @param globals {Object} contains variables to be passed at every level like $log
 * @param map {Object} store the aliases
 */
function parseNode(placeholder, nameOfNode, data, accessor, type, alias, translation, globals, map, extensible) {
    if (accessor !== '') { // handle case $ is the last accessor
        var accessorFields = accessor.split('.');
        var i;
        var len;
        var arrowAccessor;
        var arrowIndex;
        for (i = 0, len = accessorFields.length; i < len; i++) {
            arrowIndex = accessorFields[i].indexOf('->');
            if (arrowIndex > -1) {
                addToAccessor(placeholder, accessorFields[i]); // the value is not really correct, but we don't care since we store the value
                arrowAccessor = accessorFields[i].substr(0, arrowIndex);
                if (!arrowAccessor)
                    arrowAccessor = globals.lastKey;
                else
                    arrowAccessor = data[arrowAccessor];

                placeholder.accessorValue = data[accessorFields[i].substr(0, arrowIndex)];
                (function (i) { // jshint ignore:line
                    accessDocument(arrowAccessor, accessorFields[i].substr(arrowIndex + 2), globals).then(function (data) {
                        parseNode(placeholder, nameOfNode, data, accessorFields.slice(i + 1).join('.'), type, alias, translation, globals, map);
                    }, function (error) {
                        console.log(error);
                    });
                })(i);
                return;
              //return accessDocument(nameOfNode, data[accessorFields[i].substr(0, arrowIndex)], accessorFields.slice([i + 1]).join('.'), type, types, accessorFields[i].substr(arrowIndex+2));
            } else if (accessorFields[i][0] === '$') {
                readDataIterator(placeholder, nameOfNode, data, accessorFields[i], accessorFields[i + 1] ? accessorFields.slice(i + 1).join('.') : '', type, alias, translation, globals, map, data instanceof Array, extensible);
                return;
            } else if (accessorFields[i][0] === '#') {
                addToAccessor(placeholder, map[accessorFields[i].substr(1)]);
                data = data[map[accessorFields[i].substr(1)]];
            } else {
                addToAccessor(placeholder, accessorFields[i]);
                data = data[accessorFields[i]];
            }
        }
    }

    if (typeof type === 'string' || translation) {
        if (translation) {
            data = translation[0](data);
            placeholder.translate = translation[1];
        }
        if (alias) {
            map[alias] = data;
        }
        formatLeaf(placeholder, nameOfNode, data, type);
    } else {
        formatObject(placeholder, nameOfNode, data, type, globals, map);
    }
}

/**
 * Function invoked when we encounteer '->'. It will fetch a new document inside the mongo db.
 * @param filterValue {Object} value used by mongo to filter the document
 * @param arrowRight {string} right part of the arrow matching the field 'field' of db-edition.conf.js
 * @param globals {Object} contains variables to be passed at every level like $log
 */
function accessDocument(filterValue, arrowRight, globals) {
    var args = arrowRight.split(':');
    var query = {};

    // change the type if a second ':' is present.
    if (args[2]) {
        if (args[2] === 'string') {
            filterValue = filterValue.toString();
        } else {
            console.log("Not implemented yet !");
        }
    }

    query[args[1]] = filterValue;
    var d = globals.$q.defer();
    globals.DBEdition.get(args[0], query, {}).then(function (data) {
        d.resolve(data[0]);
    }, function errorCallback(resp) {
        d.reject(resp);
    });
    return d.promise;
}

/**
 * Construct a node or a leaf of data.
 * @param placeholder {Object} destination of the node
 * @param nameOfNode {string} name of the node (as given by the user)
 * @param data {Object} data received by a call to mongodb
 * @param nameOfDollarType {string} name of the new type to create (immediately following a $ in db-edition.conf.js)
 * @param accessor {string} match the field 'field' of db-edition.conf.js
 * @param type {string|Object} match the field 'type' of db-edition.conf.js
 * @param alias {string} match the field 'alias' of db-edition.conf.js
 * @param translation {Array} match the field 'translation' of db-edition.conf.js
 * @param globals {Object} contains variables to be passed at every level like $log
 * @param map {Object} store the aliases
 * @param isArray {Boolean} true if is an array false otherwise.
 */
function readDataIterator(placeholder, nameOfNode, data, nameOfDollarType, accessor, type, alias, translation, globals, map, isArray, extensible) {
    var res = [];
    var pc;
    var key;

    // init new type
    if (!globals.types[nameOfDollarType]) {
        globals.types[nameOfDollarType] = {};
    }

    // iterate over our data to treat the dollar
    for (key in data) {
        if (!data.hasOwnProperty(key)) {
            continue;
        }

        pc = {};
        res.push(pc);
        addToAccessor(pc, key);
        globals.lastKey = key;
        parseNode(pc, key, data[key], accessor, type, alias, translation, globals, map);
        globals.types[nameOfDollarType][key] = key;
    }

    placeholder.field = nameOfNode;
    placeholder.type = isArray?'array':'object';
    placeholder.value = res;
    placeholder.extensible = extensible;
}

/**
 * Construct a leaf of data
 * @param placeholder {Object} destination of the node
 * @param name {string} name of the leaf (as given by the user)
 * @param value {Object}
 * @param type {string} match the field 'type' of db-edition.conf.js
 */
function formatLeaf(placeholder, name, value, type) {
    placeholder.field = name;
    placeholder.type = type;
    placeholder.nativeType = typeof value;
    if (type === 'number' || type === 'checkbox' || type === 'checkboxes' || !value) {
        placeholder.value = value;
    } else if (type === 'date') {
        placeholder.value = new Date(value);
    } else {
        placeholder.value = value.toString();
    }
}

/**
 * Construct a leaf of data
 * @param placeholder {Object} destination of the node
 * @param name {string} name of the leaf (as given by the user)
 * @param data {Object} data received by a call to mongodb
 * @param type {string} match the field 'type' of db-edition.conf.js
 * @param globals {Object} contains variables to be passed at every level like $log
 * @param map {Object} store the aliases
 */
function formatObject(placeholder, name, data, type, globals, map) {
    placeholder.field = name;
    placeholder.type = 'object';
    placeholder.value = parseRoot(data, type, globals, map);
}

/**
 * Add a field to an accesor
 * @param placeholder {Object} destination of the node
 * @param string {string} string to add
 */
function addToAccessor(placeholder, string) {
    if (placeholder.accessor) {
        placeholder.accessor += '.' + string;
    } else {
        placeholder.accessor = string;
    }
}

/**
 * Initialize the types for the select object to work properly
 * @param types {Object} as per conf file
 * @returns {{}} object containing the types and their translations
 */
var initTypes = function (types) {
    var res = {};
    var type;
    for (type in types) {
        if (!types.hasOwnProperty(type)) {
            continue;
        }

        res[type] = types[type];
    }
    console.log('Loaded types :');
    console.log(res);
    return res;
};

/**
 * Retry if the value was not fetched yet
 * @param node {Object} current node
 */
function retry(node) {
    setTimeout(function () {
        removeAndSetReadonly(node);
    }, 100);
}

/**
 * Set type to readonly when the value is located in another collection
 * Set readonly values to editable
 * @param node {Object} current node
 */
function removeAndSetReadonly(node) {
    if (node.value === undefined) {
        retry(node);
    } else if (node.accessor.indexOf('->') > -1) {
        node.value = undefined;
        node.type = "readonly";
    } else {
        if (node.type === "readonly") {
            if (node.nativeType === "string") {
               node.type = "text";
            } else {
                node.type = node.nativeType;
            }
        }
    }
}

/**
 * Initialize the data so it can be displayed by a table object.
 * @param destination {Object} where to store the data
 * @param config {Object} configuration object (db-edition.conf.js)
 * @param collection {string} collection where to retrieve the data
 * @param types {Object} types defined as per conf
 * @param defaultObject {Object} model to create a new object
 * @param DBEdition {Object} service to make rest calls
 * @param $log {Object} logger service
 * @param $q {Object} asynchronous service
 * @returns {*}
 */
var initEmptyData = function(destination, config, collection, types, defaultObject, DBEdition, $log, $q) {
    var globals = {};
    var i, len;
    globals.DBEdition = DBEdition;
    globals.$q = $q;
    globals.$log = $log;
    globals.types = types;

    // reuse parseRoot to avoid rewriting code
    destination.name = collection;
    destination.data = {};
    destination.data.map = {};
    destination.data.info = parseRoot(defaultObject, config, globals, destination.data.map);
    destination.data.id = defaultObject._id;

    for (i = 0, len = destination.data.info.length; i < len; i++) {
        removeAndSetReadonly(destination.data.info[i]);
    }
};

/**
 * Initialize the data so it can be displayed by a table object.
 * @param destination {Object} where to store the data
 * @param config {Object} configuration object (db-edition.conf.js)
 * @param collection {string} collection where to retrieve the data
 * @param types {Object} types defined as per conf
 * @param id {Object} id of the object to retrieve in the collection
 * @param DBEdition {Object} service to make rest calls
 * @param $log {Object} logger service
 * @param $q {Object} asynchronous service
 * @returns {*}
 */
var initData = function(destination, config, collection, types, id, DBEdition, $log, $q) {
    var globals = {};
    globals.DBEdition = DBEdition;
    globals.$q = $q;
    globals.$log = $log;
    globals.types = types;
    var d = $q.defer();
    DBEdition.get(collection, {_id: id}, {}).then(function (data) {
        console.log(data[0]);
        destination.name = collection;
        destination.data = {};
        destination.data.map = {};
        destination.data.info = parseRoot(data[0], config, globals, destination.data.map);
        destination.data.id = id;

        console.log(destination);
        d.resolve();
    }, function errorCallback(resp) {
        d.reject(resp);
    });
    return d.promise;
};

/**
 * Fetch the data to be displayed on screen. Note: we fetch lazily the data, so it does not return the whole database.
 * @param destination {Object} where to store the data
 * @param collections {Object} collections to fetch
 * @param typesProcessed {Object} types processed by InitTypes
 * @param DBEdition {Object} service to make rest calls
 * @param $log {Object} logger service
 * @param $q {Object} asynchronous service
 */
var initDatabase = function(destination, collections, typesProcessed, DBEdition, $log, $q) {
    var retrieve = function (db, collection) {
        DBEdition.count(collection, {}).then(function(data) {
            var nbrRows = parseInt(data, 10);
            var i;
            for (i = db.functions.paginate; i < nbrRows; i++) {
                if (!db.data[i]) {
                    db.data[i] = null;
                }
            }
        }, function errorCallback(resp) {
            $log.log('Failed to count ' + collection + ' (' + resp.statusText + ')');
        });

        DBEdition.get(collection, {}, { skip: 0, limit: db.functions.paginate }).then(function (data) {
            var k = 0;
            var field;
            var configuration = collections[collection][0];

            // populate the metadata
            for (field in configuration) {
                if (!configuration.hasOwnProperty(field)) {
                    continue;
                }

                db.metadata.push({
                    name: field,
                    field: configuration[field].field,
                    type: configuration[field].type,
                    searchable: configuration[field].searchable === true,
                    summable: configuration[field].summable === true
                });
                k += 1;
            }

            // populate the data
            for (var i = 0; i < data.length; i++) {
                db.data[i] = {};
                db.data[i].map = {};
                db.data[i].info = parseRoot(data[i], configuration, globals, db.data[i].map);
                db.data[i].id = data[i]._id;
            }
        }, function errorCallback(resp) {
            $log.log('Failed to retrieve ' + collection + ' (' + resp.statusText + ')');
        });
    };

    var globals = {};
    var db;
    globals.DBEdition = DBEdition;
    globals.$q = $q;
    globals.$log = $log;
    globals.types = typesProcessed;

    for (var collection in collections) {
        if (!collections.hasOwnProperty(collection)) {
            continue;
        }

        db = {
            name: collection,
            metadata: [],
            data: [],
            functions : collections[collection][2]
        };

        // non supported
        if (db.functions.sum && db.functions.paginate !== undefined && db.functions.paginate !== 4294967295) {
            alert("Sum is not supported with the pagination !")
        }

        // set default value for paginate
        if (!db.functions.paginate) {
            db.functions.paginate = 4294967295; //max uint
        }

        destination[collection] = db;

        retrieve(db, collection);
    }

    setTimeout(function () {
        console.log("Loaded databases (after 1000ms):");
        console.log(destination);
    }, 1000);
};

/**
 * Add data to a collection. Necessary for the lazy loading.
 * @param destination {Object} where to store the data
 * @param collections {Object} existing collections
 * @param collection {string} collection to fetch
 * @param typesProcessed {Object} types processed by InitTypes
 * @param skip {int} number of item to skip
 * @param limit {int} number of item to fetch
 * @param sortObject {Object} how to sort
 * @param queryObject {Object} what to query
 * @param DBEdition {Object} service to make rest calls
 * @param $log {Object} logger service
 * @param $q {Object} asynchronous service
 */
var addData = function(destination, collections, collection, typesProcessed, skip, limit, sortObject, queryObject, DBEdition, $log, $q) {
    var treatData = function (data, db) {
        var configuration = collections[collection][0];

        // populate the data
        for (var i = skip; i < skip+limit && i < skip+data.length; i++) {
            db.data[i] = {};
            db.data[i].map = {};
            db.data[i].info = parseRoot(data[i-skip], configuration, globals, db.data[i].map);
            db.data[i].id = data[i-skip]._id;
        }
    };

    var retrieve = function (db, collection) {
        if (sortObject) {
            if (queryObject) {
                DBEdition.aggregate(collection, [{$sort: sortObject}, {$skip: skip}, {$limit: limit}, {$match: queryObject}]).then(function (data) {
                    treatData(data, db);
                }, function errorCallback(resp) {
                    $log.log('Failed to retrieve ' + collection + ' (' + resp.statusText + ')');
                });
            } else {
                DBEdition.aggregate(collection, [{$sort: sortObject}, {$skip: skip}, {$limit: limit}]).then(function (data) {
                    treatData(data, db);
                }, function errorCallback(resp) {
                    $log.log('Failed to retrieve ' + collection + ' (' + resp.statusText + ')');
                });
            }
        } else {
            DBEdition.count(collection, queryObject).then(function(data) {
                var nbrRows = parseInt(data, 10);
                var i;
                var len = db.data.length;
                if (nbrRows < len) {
                    db.data.splice(nbrRows, len - nbrRows);
                } else {
                    for (i = len; i < nbrRows; i++) {
                        if (!db.data[i]) {
                            db.data[i] = null;
                        }
                    }
                }
            }, function errorCallback(resp) {
                $log.log('Failed to count ' + collection + ' (' + resp.statusText + ')');
            });
            DBEdition.get(collection, queryObject, {skip: skip, limit: limit}).then(function (data) {
                treatData(data, db);
            }, function errorCallback(resp) {
                $log.log('Failed to retrieve ' + collection + ' (' + resp.statusText + ')');
            });
        }
    };

    var globals = {};
    globals.DBEdition = DBEdition;
    globals.$q = $q;
    globals.$log = $log;
    globals.types = typesProcessed;

    retrieve(destination, collection);
};

/**
 * Format basically the data to it can be stored inside the database.
 * @param data {Object} data displayed by the table
 * @returns {{}} formatted object
 */
function formatData(data) {
    var key, key2, node, tmp;
    var res = {};
    for (key in data) {
        if (!data.hasOwnProperty(key)) {
            continue;
        }
        if (key.substr(0, 2) === '$$') {
            continue;
        }

        node = data[key];

        if (node.type === 'component') {
            continue;
        }

        if (node.type !== 'array' && node.type !== 'object') {
            if (node.type === 'date') {
                res[node.accessor] = node.value.getTime();
            } else {
                if (node.translate) {
                    res[node.accessor] = node.translate(node.value);
                } else {
                    res[node.accessor] = node.value;
                }
            }
        } else {
            tmp = formatData(node.value);
            if (node.type === 'array') {
                res[node.accessor] = [];
                for (key2 in tmp) {
                    if (!tmp.hasOwnProperty(key2)) {
                        continue;
                    }

                    res[node.accessor].push(tmp[key2]);
                }
            } else {
                for (key2 in tmp) {
                    if (!tmp.hasOwnProperty(key2)) {
                        continue;
                    }

                    res[node.accessor + '.' + key2] = tmp[key2];
                }
            }
        }
    }

    return res;
}

/**
 * Separate the formatted data into several request.
 * @param data {Object} data displayed by the table
 * @param basicFormat {Object} data formatted by formatData
 * @param collection {string} base collection to update
 * @param filter {string} object on which to filter to store the new data
 * @returns {*[]}
 */
function prepareQueries(data, basicFormat, collection, filter) {
    var res = [{collection: collection, query: {}, projection: {}}];
    var key;
    var indexOfArrow;
    for (key in basicFormat) {
        if (!basicFormat.hasOwnProperty(key)) {
            continue;
        }

        indexOfArrow = key.indexOf('->');
        if (indexOfArrow > -1) { //
            var rightArrow = key.substr(indexOfArrow + 2, key.length);
            var filterValue = getAccessorValue(data, key);
            var splittedDot = rightArrow.split('.');
            var splittedDoubleDot = splittedDot[0].split(':');
            var filterField = splittedDoubleDot[1]?splittedDoubleDot[1]:'_id';

            addToQuery(res, splittedDoubleDot[0], filterField, filterValue, splittedDot.slice(1).join('.'), basicFormat[key]);
        } else {
            if (key === filter) {
                res[0].query[filter] = basicFormat[key];
            } else {
                res[0].projection[key] = basicFormat[key];
            }
        }
    }

    return res;
}

/**
 * Get the value of a node to be able to access a different document
 * @param data {Object} data displayed by the table
 * @param accessor {string} how to reach the node
 * @returns {*}
 */
function getAccessorValue(data, accessor) {
    accessor = accessor.match(/^.*(?=.*->)->[^.]*.[^.]*/)[0];
    var i, len;
    var node;
    for(i = 0, len = data.length; i < len; i++) {
        node = data[i];
        if (node.accessor === accessor) {
            return node.accessorValue;
        }
    }

    return null;
}

/**
 * Add a tuple to a query, and creates it if it does not exists
 * @param res {Object} object to fill with the result.
 * @param collection {string} collection corresponding to this query
 * @param filterField {string} field to use to filter for the update
 * @param filterValue {Object} value to use to filter for the update
 * @param newKey {string} field to add to the query
 * @param newValue {Object} value to add to the query
 */
function addToQuery(res, collection, filterField, filterValue, newKey, newValue) {
    var query = null;
    var i, len;
    for (i = 0, len = res.length; i < len; i++) {
        if (res[i].collection === collection && res[i].query[filterField] === filterValue) {
            query = res[i];
            break;
        }
    }

    if (!query) {
        query = {collection: collection, query: {}, projection: {}};
        query.query[filterField] = filterValue;
        res.push(query);
    }

    query.projection[newKey] = newValue;
}

/**
 * Update the database based on the field that were modified
 * @param DBEdition
 * @param collection
 * @param data
 */
var updateData = function(DBEdition, collection, data, id) {
    console.log(data);
    var i, len;
    var basicFormat = formatData(data);
    var toSend = prepareQueries(data, basicFormat, collection, '_id');
    toSend[0].query._id = id;

    console.log(toSend);
    for (i = 0, len = toSend.length; i < len; i++) {
        if (toSend[i].query.id === null || toSend[i].projection === ""){
            continue;
        }
        DBEdition.update(toSend[i].collection, toSend[i].query, { $set: toSend[i].projection });
    }
};

/**
 * Format basically the data to it can be stored inside the database.
 * @param data {Object} data displayed by the table
 * @returns {{}} formatted object
 */
function formatInsertData(data) {
    var key, key2, node, tmp, i, len;
    var res = {};
    var resAcces;
    var splittedAccessor;
    for (key in data) {
        if (!data.hasOwnProperty(key)) {
            continue;
        }
        if (key.substr(0, 2) === '$$') {
            continue;
        }
        resAcces = res;
        node = data[key];
        if (node.accessor.indexOf('->') > -1) {
            continue;
        }

        if (node.accessor.indexOf('.') > -1) {
            splittedAccessor = node.accessor.split('.');
            for(i = 0, len = splittedAccessor.length - 1; i < len ; i++) {
                if (!resAcces[splittedAccessor[i]]) {
                    resAcces[splittedAccessor[i]] = {};
                }
                resAcces = resAcces[splittedAccessor[i]];
            }
            node.accessor = splittedAccessor[i];
        }

        if (node.type !== 'array' && node.type !== 'object') {
            if (node.type === 'date') {
                resAcces[node.accessor] = node.value.getTime();
            } else {
                resAcces[node.accessor] = node.value;
            }
        } else {
            if (node.type === 'array') {
                resAcces[node.accessor] = [];
                tmp = formatInsertData(node.value);
                for (key2 in tmp) {
                    if (!tmp.hasOwnProperty(key2)) {
                        continue;
                    }

                    resAcces[node.accessor].push(tmp[key2]);
                }
            } else {
                resAcces[node.accessor] = {};
                tmp = formatInsertData(node.value);
                for (key2 in tmp) {
                    if (!tmp.hasOwnProperty(key2)) {
                        continue;
                    }

                    resAcces[node.accessor][key2] = tmp[key2];
                }
            }
        }
    }

    return res;
}

/**
 * Update the database based on the field that were modified
 * @param DBEdition
 * @param collection
 * @param data
 */
var insertData = function(DBEdition, collection, data, defaultValues, $q) {
    console.log(data);
    var idValue;
    var basicFormat = formatInsertData(data);

    //var toSend = prepareQueries(data, basicFormat, collection, '_id');
    console.log(basicFormat);

    idValue = basicFormat._id;
    if (idValue !== "" || idValue === 0 || idValue === undefined) {
        defaultValues._id = idValue;
    } else {
        delete defaultValues._id;
    }

    var d = $q.defer();
    DBEdition.insert(collection, defaultValues).then(function(data) {
        delete basicFormat._id;
        DBEdition.update(collection, data, { $set: basicFormat }).then(function(){
            d.resolve();
        }, function(resp) {
            d.reject(resp);
        });
    }, function (resp) {
        d.reject(resp);
    });

    return d.promise;
};

module.exports.initTypes = initTypes;
module.exports.initData = initData;
module.exports.initEmptyData = initEmptyData;
module.exports.initDatabase = initDatabase;
module.exports.addData = addData;
module.exports.updateData = updateData;
module.exports.insertData = insertData;
