/**
 * Created by richard on 15/02/17.
 */

var config;
module.exports = [];

config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [],
        components: []
    }],
    types: [{
        name: 'bar',
        mapping: {
            foo: 'bar',
            toto: 'tutu'
        }
    }, {
        name: 'bar',
        mapping: {
            bar: 'foo',
            toto: 'tutu'
        }
    }]
};
module.exports.push({ config: config, errorMsg: 'Two types cannot have the same name.' });

config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [],
        components: [],
        paginate: -5
    }],
    types: []
};
module.exports.push({ config: config, errorMsg: 'Paginate cannot have a negative value.' });

config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            name: 'equipment',
            view: 2, // detailled
            saveAs: 'string', // equipment field is an array, here : ['one', 'two', 'three']
            extensible: true,
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                saveAs: 'string',
                accessor: '$',
                displayAs: 'text'
            }]
        }],
        components: []
    }],
    types: []
};
module.exports.push({ config: config, errorMsg: 'Only Maps and Arrays can be extensibles.' });

// config = {
//     commands: [{
//         name: 'foo',
//         collection: 'bar',
//         nodes: [{
//             name: 'equipment',
//             saveAs: 'map',
//             accessor: 'equipmentField',
//             displayAs: [{
//                 name: 'level',
//                 saveAs: 'number',
//                 accessor: 'level',
//                 displayAs: 'number'
//             }]
//         }],
//         components: []
//     }],
//     types: [{
//         name: 'bar',
//         mapping: {
//             foo: 'bar',
//             toto: 'tutu'
//         }
//     }]
// };
// module.exports.push({ config: config, errorMsg: 'Two types cannot have the same name.' });