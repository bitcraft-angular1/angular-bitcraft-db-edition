/**
 * Created by richard on 15/02/17.
 */
'use strict';
var assert = require('assert');

var defaultCheckResult = function (object) {
    assert.notEqual(object, null);
};

var commands = [{
    name: 'foo',
    collection: 'bar',
    nodes: [],
    components: []
}];


module.exports = [
    {
        successMsg: 'One type, with one mapping',
        config: {
            commands: commands,
            types: [{
                name: 'bar',
                mapping: {
                    foo: 'bar'
                }
            }]
        },
        checkResult: defaultCheckResult
    }, {
        successMsg: 'One type, with two mappings',
        config: {
            commands: commands,
            types: [{
                name: 'bar',
                mapping: {
                    foo: 'bar',
                    toto: 'tutu'
                }
            }]
        },
        checkResult: defaultCheckResult
    }, {
        successMsg: 'Two types, with two mappings',
        config: {
            commands: commands,
            types: [{
                name: 'bar',
                mapping: {
                    foo: 'bar',
                    toto: 'tutu'
                }
            }, {
                name: 'bar2',
                mapping: {
                    foo: 'bar',
                    bar: 'foo'
                }
            }]
        },
        checkResult: defaultCheckResult
    }, {
        successMsg: 'Two types, with two mappings',
        config: {
            commands: commands,
            types: [{
                name: 'bar',
                mapping: {
                    foo: 'bar',
                    toto: 'tutu'
                }
            }, {
                name: 'bar2',
                mapping: {
                    foo: 'bar',
                    toto: 'tutu'
                }
            }]
        },
        checkResult: defaultCheckResult
    }, {
        successMsg: 'One type, with two mappings, UTF8',
        config: {
            commands: commands,
            types: [{
                name: 'bar',
                mapping: {
                    foo: 'bar',
                    'すし': '美味しい'
                }
            }]
        },
        checkResult: defaultCheckResult
    }
];
