/**
 * Created by richard on 15/02/17.
 */
'use strict';
var assert = require('assert');
var defaultCheckResult = function (object) {
    assert.notEqual(object, null);
};

var config;
module.exports = [];

// =================== ARRAY PART ================= //

/* result :
 +-----------+
 |    one    |
 +-----------+
 |    two    |
 +-----------+
 |   three   |
 +-----------+
 */
config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            view: 2, // detailled
            saveAs: 'array', // equipment field is an array, here : ['one', 'two', 'three']
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                saveAs: 'string',
                accessor: '$',
                displayAs: 'text'
            }]
        }, {
            view: 2,
            accessor: '#equipFields'
        }],
        components: []
    }],
    types: []
};
module.exports.push({ successMsg: 'Simple array', config: config, checkResult: defaultCheckResult});

/* result :
 +-------------+-----------+
 |             |    one    |
 |             +-----------+
 |  equipment  |    two    |
 |             +-----------+
 |             |   three   |
 +-------------+-----------+
 */
config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            name: 'equipment',
            view: 2, // detailled
            saveAs: 'array', // equipment field is an array, here : ['one', 'two', 'three']
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                saveAs: 'string',
                accessor: '$',
                displayAs: 'text'
            }]
        }],
        components: []
    }],
    types: []
};
module.exports.push({ successMsg: 'Simple array', config: config, checkResult: defaultCheckResult});

/* result :
_id: xxxx,
equipement: ['one', 'two', 'three']
+-------------+---------+-----------+
|             |    0    |    one    |
|             +---------+-----------+
|  equipment  |    1    |    two    |
|             +---------+-----------+
|             |    2    |   three   |
+-------------+---------+-----------+
*/
config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            name: 'equipment',
            view: 2, // detailled
            saveAs: 'array', // equipment field is an array, here : ['one', 'two', 'three']
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                name: '$', // $ as name : key
                saveAs: 'string',
                accessor: '$', // $ as accessor : value
                displayAs: 'text'
            }]
        }],
        components: []
    }],
    types: []
};
module.exports.push({ successMsg: 'Simple array', config: config, checkResult: defaultCheckResult});

/* result :
 equipment: [{text: 'one', value: 1}, {text: 'two', value: 2}, {text: 'three', value: 3}]
 +-------------+---------+-----------+
 |             |    0    |    one    |
 |             +---------+-----------+
 |  equipment  |    1    |    two    |
 |             +---------+-----------+
 |             |    2    |   three   |
 +-------------+---------+-----------+
 */
config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            name: 'equipment',
            view: 2, // detailled
            saveAs: 'array', // equipment field is an array, here : [{text: 'one', value: 1}, {text: 'two', value: 2}, {text: 'three', value: 3}]
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                name: '$',
                saveAs: 'string',
                accessor: ['$', 'text'],
                displayAs: 'text'
            }]
        }],
        components: []
    }],
    types: []
};
module.exports.push({ successMsg: 'Array of objects', config: config, checkResult: defaultCheckResult});

/* result :
 +-----------+-------+
 |           |  one  |
 |           +-------+
 |           |   1   |
 |           +-------+
 |           |  two  |
 | equipment +-------+
 |           |   2   |
 |           +-------+
 |           | three |
 |           +-------+
 |           |   3   |
 +-----------+-------+
 */
config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            name: 'equipment',
            view: 2, // detailled
            saveAs: 'array', // equipment field is an array, here : [{text: 'one', value: 1}, {text: 'two', value: 2}, {text: 'three', value: 3}]
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                name: '$',
                saveAs: 'Map',
                accessor: '$', // here $ : {text: 'one', value: 1}
                displayAs: [{
                    accessor: 'text',
                    saveAs: 'string',
                    displayAs: 'text'
                }, {
                    accessor: 'value',
                    saveAs: 'number',
                    displayAs: 'number'
                }]
            }]
        }],
        components: []
    }],
    types: []
};
module.exports.push({ successMsg: 'Array of objects', config: config, checkResult: defaultCheckResult});

/* result :
 +-----------+-------+-------+
 |           |       |  one  |
 |           |   0   +-------+
 |           |       |   1   |
 |           +---------------+
 |           |       |  two  |
 | equipment |   1   +-------+
 |           |       |   2   |
 |           +---------------+
 |           |       | three |
 |           |   2   +-------+
 |           |       |   3   |
 +-----------+-------+-------+
 */
config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            name: 'equipment',
            view: 2, // detailled
            saveAs: 'array', // equipment field is an array, here : [{text: 'one', value: 1}, {text: 'two', value: 2}, {text: 'three', value: 3}]
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                name: '$',
                saveAs: 'map',
                accessor: '$', // here $ : {text: 'one', value: 1}
                displayAs: [{
                    accessor: 'text',
                    saveAs: 'string',
                    displayAs: 'text'
                }, {
                    accessor: 'value',
                    saveAs: 'number',
                    displayAs: 'number'
                }]
            }]
        }],
        components: []
    }],
    types: []
};
module.exports.push({ successMsg: 'Array of objects', config: config, checkResult: defaultCheckResult});

/* result :
 +-----------+-------+
 |           |  one  |
 |           +-------+
 |           |  un   |
 |           +-------+
 |           |  two  |
 | equipment +-------+
 |           |  deux |
 |           +-------+
 |           | three |
 |           +-------+
 |           | trois |
 +-----------+-------+
 */
config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            name: 'equipment',
            view: 2, // detailled
            saveAs: 'array', // equipment field is an array, here : [['one', 'un'], ['two', 'deux'], ['three', 'trois']]
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                saveAs: 'array',
                accessor: ['$', '$local'], // here $ : ['one', 'un'] | $local: name to iterate on the second array
                displayAs: [{
                    accessor: '$',
                    saveAs: 'string',
                    displayAs: 'text'
                }]
            }]
        }],
        components: []
    }],
    types: []
};
module.exports.push({ successMsg: 'Array of arrays', config: config, checkResult: defaultCheckResult});

/* result :
 +----------+-------+-------+
 |          |       |  one  |
 |          |   0   +-------+
 |          |       |  un   |
 |          +---------------+
 |          |       |  two  |
 | equipment|   1   +-------+
 |          |       |  deux |
 |          +---------------+
 |          |       | three |
 |          |   2   +-------+
 |          |       | trois |
 +----------+-------+-------+
 */
config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            name: 'equipment',
            view: 2, // detailled
            saveAs: 'array', // equipment field is an array, here : [['one', 'un'], ['two', 'deux'], ['three', 'trois']]
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                name: '$',
                saveAs: 'array',
                accessor: ['$', '$local'], // here $ : ['one', 'un'] | $local: name to iterate on the second array
                displayAs: [{
                    accessor: '$',
                    saveAs: 'string',
                    displayAs: 'text'
                }]
            }]
        }],
        components: []
    }],
    types: []
};
module.exports.push({ successMsg: 'Array of arrays', config: config, checkResult: defaultCheckResult});

// =================== MAP PART ================= //

/* result :
 +-----------+-------+-------+
 |           | sword |   1   |
 |           +---------------+
 | equipment |  bow  |   0   |
 |           +---------------+
 |           | armor |   2   |
 +-----------+-------+-------+
 */
config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            name: 'equipment',
            view: 2, // detailled
            saveAs: 'map', // equipment field is a map, here : {sword: 1, bow: 0, armor: 2}
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                name: '$', // here $ is sword, bow, armor
                accessor: '$', // here $ is 1, 0, 2
                displayAs: 'number',
                saveAs: 'number'
            }]
        }],
        components: []
    }],
    types: []
};
module.exports.push({ successMsg: 'Simple map', config: config, checkResult: defaultCheckResult});

/* result :
 +-----------+---------+-----------+---------+
 |           |         |  name     | mySword |
 |           |  sword  +---------------------+
 |           |         |   level   |    1    |
 |           +-------------------------------+
 |           |         |   name    |  myBow  |
 | equipment |   bow   +---------------------+
 |           |         |   level   |    0    |
 |           +-------------------------------+
 |           |         |   name    | myArmor |
 |           |  armor  +---------------------+
 |           |         |   level   |    2    |
 +-----------+---------+-----------+---------+
 */
config = {
    commands: [{
        name: 'foo',
        collection: 'bar',
        nodes: [{
            name: 'equipment',
            view: 2, // detailled
            saveAs: 'map', // equipment field is a map, here : {sword: { name: 'mySword', level: 1}, bow: : { name: 'myBow', level: 1}, armor: : { name: 'myArmor', level: 1}}
            accessor: ['equipmentField', '$equipFields'], // $equipFields is a name only to store the list
            displayAs: [{
                name: '$', // here $ is sword, bow, armor
                accessor: '$', // here $ is { name: 'mySword', level: 1}
                saveAs: 'map',
                displayAs: [{
                    name: 'name',
                    accessor: 'name',
                    saveAs: 'string',
                    displayAs: 'text'
                }, {
                    name: 'level',
                    accessor: 'level',
                    saveAs: 'number',
                    displayAs: 'number'
                }]
            }]
        }],
        components: []
    }],
    types: []
};
module.exports.push({ successMsg: 'Map of maps', config: config, checkResult: defaultCheckResult});
