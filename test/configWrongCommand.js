/**
 * Created by richard on 15/02/17.
 */

var types = [
    {
        name: 'bar',
        mapping: {
            foo: 'bar',
            toto: 'tutu'
        }
    }
];

module.exports = [
    {
        config: { types: types},
        errorMsg: 'No commands were specified.'
    },
    {
        config: { commands: {}, types: types},
        errorMsg: 'Commands should be an array.'
    },
    {
        config: { commands: 1, types: types},
        errorMsg: 'Commands should be an array.'
    },
    {
        config: { commands: 'foo', types: types},
        errorMsg: 'Commands should be an array.'
    },
    {
        config: { commands: true, types: types},
        errorMsg: 'Commands should be an array.'
    },
    {
        config: { commands: false, types: types},
        errorMsg: 'Commands should be an array.'
    },
    {
        config: { commands: new Date(), types: types},
        errorMsg: 'Commands should be an array.'
    }
];