/**
 * Created by richard on 15/02/17.
 */

var commands = [{
    name: 'foo',
    collection: 'bar',
    nodes: [],
    components: []
}];


module.exports = [
    {
        config: { commands: commands, types: {} },
        errorMsg: 'Types should be an array.'
    },
    {
        config: { commands: commands, types: 1  },
        errorMsg: 'Types should be an array.'
    },
    {
        config: { commands: commands, types: '1'  },
        errorMsg: 'Types should be an array.'
    },
    {
        config: { commands: commands, types: true  },
        errorMsg: 'Types should be an array.'
    },
    {
        config: { commands: commands, types: false  },
        errorMsg: 'Types should be an array.'
    },
    {
        config: { commands: commands, types: new Date() },
        errorMsg: 'Types should be an array.'
    }
];