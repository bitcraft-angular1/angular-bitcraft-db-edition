/**
 * Created by richard on 15/02/17.
 */

var commands = [{
    name: 'foo',
    collection: 'bar',
    nodes: [],
    components: []
}];


module.exports = [
    {
        config: { commands: commands, types: ['a'] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }, {
        config: { commands: commands, types: [1] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }, {
        config: { commands: commands, types: [false] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }, {
        config: { commands: commands, types: [{}] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }, {
        config: { commands: commands, types: [{12: 10}] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }, {
        config: { commands: commands, types: [{true: false}] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }, {
        config: { commands: commands, types: [{foo: {}}] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }, {
        config: { commands: commands, types: [{name: [], mapping: {}}] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }, {
        config: { commands: commands, types: [{name: [], mapping: { foo: 'bar'}}] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }, {
        config: { commands: commands, types: [{name: 1, mapping: { foo: 'bar'}}] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }, {
        config: { commands: commands, types: [{name: 'bar', mapping: { foo: 1}}] },
        errorMsg: 'Type should be a object of type { name: string, mapping: { keys: strings } }.'
    }
];