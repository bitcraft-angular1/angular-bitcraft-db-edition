/**
 * Created by richard on 15/02/17.
 */
/*global describe, it*/
'use strict';
var assert = require('assert');
var Config = require('../src/lib/db-edition-2').Configuration;

var failTestHelper = function (testDescription, configuration, errorMessage) {
    it(testDescription, function () {
        var shouldThrow = function () {
            Config.parseConfigurationObject(configuration);
        };
        assert.throws(shouldThrow, SyntaxError, errorMessage);
    });
};

var successTestHelper = function (testDescription, configuration, checkResult) {
    it(testDescription, function () {
        checkResult(Config.parseConfigurationObject(configuration));
    });
};

var defaultCheckResult = function (object) {
    assert.notEqual(object, null);
};

describe('Test helper methods', function() {
    var Helper = require('../src/lib/db-edition-2').Helper;

    describe('isUnset', function () {
        it('Should return true', function () {
            var a;
            assert(Helper.isUnset(null) === true);
            assert(Helper.isUnset(undefined) === true);
            assert(Helper.isUnset("") === true);
            assert(Helper.isUnset(a) === true);
        });

        it('Should return false', function () {
            assert(Helper.isUnset({}) === false);
            assert(Helper.isUnset([]) === false);
            assert(Helper.isUnset([1]) === false);
            assert(Helper.isUnset(new Date()) === false);
            assert(Helper.isUnset(true) === false);
            assert(Helper.isUnset(false) === false);
            assert(Helper.isUnset(1) === false);
            assert(Helper.isUnset(546541869413216731684731106584) === false);
            assert(Helper.isUnset('akjhsd') === false);
        });
    });

    describe('isMap', function () {
        it('Should return true', function () {
            assert(Helper.isMap({}) === true);
            assert(Helper.isMap({a: 1}) === true);
            assert(Helper.isMap(new Object({})) === true);
            assert(Helper.isMap(new Object({a: 5})) === true);
        });

        it('Should return false', function () {
            assert(Helper.isMap([]) === false);
            assert(Helper.isMap([1]) === false);
            assert(Helper.isMap(new Date()) === false);
            assert(Helper.isMap(true) === false);
            assert(Helper.isMap(false) === false);
            assert(Helper.isMap(1) === false);
            assert(Helper.isMap(546541869413216731684731106584) === false);
            assert(Helper.isMap('akjhsd') === false);
            assert(Helper.isMap(null) === false);
            assert(Helper.isMap(undefined) === false);
            //noinspection JSPrimitiveTypeWrapperUsage
            assert(Helper.isArray(new Array()) === true);
            //noinspection JSPrimitiveTypeWrapperUsage
            assert(Helper.isArray(new Array(1, 2, 3)) === true);
        });
    });

    describe('isArray', function () {
        it('Should return true', function () {
            assert(Helper.isArray([]) === true);
            assert(Helper.isArray([1, 2]) === true);
            assert(Helper.isArray(['a', 'b']) === true);
            //noinspection JSPrimitiveTypeWrapperUsage
            assert(Helper.isArray(new Array()) === true);
            //noinspection JSPrimitiveTypeWrapperUsage
            assert(Helper.isArray(new Array(1, 2, 3)) === true);
        });

        it('Should return false', function () {
            assert(Helper.isArray({}) === false);
            assert(Helper.isArray({a: []}) === false);
            assert(Helper.isArray(new Date()) === false);
            assert(Helper.isArray(true) === false);
            assert(Helper.isArray(false) === false);
            assert(Helper.isArray(1) === false);
            assert(Helper.isArray(546541869413216731684731106584) === false);
            assert(Helper.isArray('akjhsd') === false);
            assert(Helper.isArray(null) === false);
            assert(Helper.isArray(undefined) === false);
        });
    });

    describe('isFunction', function () {
        it('Should return true', function () {
            var a = function () {
                return 0;
            };
            assert(Helper.isFunction(Helper.isFunction) === true);
            assert(Helper.isFunction(a) === true);
            assert(Helper.isFunction(function () { return true; }) === true);
            assert(Helper.isFunction(new Object(function () { return true; })) === true);
        });

        it('Should return false', function () {
            assert(Helper.isFunction({}) === false);
            assert(Helper.isFunction([]) === false);
            assert(Helper.isFunction(true) === false);
            assert(Helper.isFunction(false) === false);
            assert(Helper.isFunction(null) === false);
            assert(Helper.isFunction(undefined) === false);
            assert(Helper.isFunction('str') === false);
        });
    });

    describe('isString', function () {
        it('Should return true', function () {
            assert(Helper.isString('') === true);
            assert(Helper.isString("") === true);
            assert(Helper.isString('hi') === true);
            assert(Helper.isString("hi") === true);
            //noinspection JSPrimitiveTypeWrapperUsage
            assert(Helper.isString(new String()) === true);
            //noinspection JSPrimitiveTypeWrapperUsage
            assert(Helper.isString(new String('hello')) === true);
            assert(Helper.isString(new Object('hello')) === true);
        });

        it('Should return false', function () {
            assert(Helper.isString({}) === false);
            assert(Helper.isString([]) === false);
            assert(Helper.isString({a: []}) === false);
            assert(Helper.isString(new Date()) === false);
            assert(Helper.isString(true) === false);
            assert(Helper.isString(false) === false);
            assert(Helper.isString(1) === false);
            assert(Helper.isString(546541869413216731684731106584) === false);
            assert(Helper.isString(null) === false);
            assert(Helper.isString(undefined) === false);
        });
    });

    describe('isBoolean', function () {
        it('Should return true', function () {
            assert(Helper.isBoolean(true) === true);
            assert(Helper.isBoolean(false) === true);
            assert(Helper.isBoolean(new Object(true)) === true);
            assert(Helper.isBoolean(new Object(false)) === true);
            assert(Helper.isBoolean(new Boolean(true)) === true);
            assert(Helper.isBoolean(new Boolean(false)) === true);
        });

        it('Should return false', function () {
            assert(Helper.isBoolean({}) === false);
            assert(Helper.isBoolean([]) === false);
            assert(Helper.isBoolean({a: []}) === false);
            assert(Helper.isBoolean(new Date()) === false);
            assert(Helper.isBoolean(1) === false);
            assert(Helper.isBoolean(546541869413216731684731106584) === false);
            assert(Helper.isBoolean(null) === false);
            assert(Helper.isBoolean(undefined) === false);
            assert(Helper.isBoolean('bar') === false);
        });
    });

    describe('isNumber', function () {
        it('Should return true', function () {
            assert(Helper.isNumber(0) === true);
            assert(Helper.isNumber(0.0) === true);
            assert(Helper.isNumber(12.0) === true);
            assert(Helper.isNumber(12) === true);
            assert(Helper.isNumber(1245648641386386764153486415867413) === true);
        });

        it('Should return false', function () {
            assert(Helper.isNumber({}) === false);
            assert(Helper.isNumber([]) === false);
            assert(Helper.isNumber({a: []}) === false);
            assert(Helper.isNumber(new Date()) === false);
            assert(Helper.isNumber(true) === false);
            assert(Helper.isNumber(false) === false);
            assert(Helper.isNumber(null) === false);
            assert(Helper.isNumber(undefined) === false);
        });
    });

    describe('sanitizeString', function () {
        it('Should return "foo"', function () {
            assert(Helper.sanitizeString('foo') === 'foo');
            assert(Helper.sanitizeString(new Object('foo')) === 'foo');
            assert(Helper.sanitizeString(new String('foo')) === 'foo');
        });

        it('Should throw', function () {
            var throws = function (value) {
                var loc = function () {
                    Helper.sanitizeString(value);
                };

                assert.throws(loc, TypeError);
            };

            throws({});
            throws([]);
            throws(false);
            throws(true);
            throws(null);
            throws(undefined);
        });
    });

    describe('sanitizeBoolean', function () {
        it('Should return true|false', function () {
            assert(Helper.sanitizeBoolean(true) === true);
            assert(Helper.sanitizeBoolean(new Object(true)) === true);
            assert(Helper.sanitizeBoolean(new Boolean(true)) === true);
            assert(Helper.sanitizeBoolean(false) === false);
            assert(Helper.sanitizeBoolean(new Object(false)) === false);
            assert(Helper.sanitizeBoolean(new Boolean(false)) === false);
        });

        it('Should throw', function () {
            var throws = function (value) {
                var loc = function () {
                    Helper.sanitizeBoolean(value);
                };

                assert.throws(loc, TypeError);
            };

            throws({});
            throws([]);
            throws(1);
            throws(0);
            throws(null);
            throws(undefined);
            throws('');
        });
    });

    describe('sanitizeNumber', function () {
        it('Should return 5', function () {
            assert(Helper.sanitizeNumber(5) === 5);
            assert(Helper.sanitizeNumber(new Object(5)) === 5);
            assert(Helper.sanitizeNumber(new Number(5)) === 5);
        });

        it('Should throw', function () {
            var throws = function (value) {
                var loc = function () {
                    Helper.sanitizeNumber(value);
                };

                assert.throws(loc, TypeError);
            };

            throws({});
            throws([]);
            throws(true);
            throws(false);
            throws(null);
            throws(undefined);
            throws('');
            throws('5');
        });
    });


    describe('sanitizeDisplayType', function () {
        it('Should return 3', function () {
            assert(Helper.sanitizeDisplayType(3) === 3);
            assert(Helper.sanitizeDisplayType('Checkbox') === 3);
        });

        it('Should throw', function () {
            var throws = function (value) {
                var loc = function () {
                    Helper.sanitizeDisplayType(value);
                };

                assert.throws(loc, TypeError);
            };

            throws({});
            throws([]);
            throws(true);
            throws(false);
            throws(null);
            throws(undefined);
            throws('');
            throws('5a');
        });
    });
});

describe('Fail parse configuration', function () {
    var currentConfig;
    var i;

    /*describe('Types', function () {
        currentConfig = require('./configWrongTypes.js');
        for (i = 0; i < currentConfig.length; i = i + 1) {
            failTestHelper('Should throw if types if of a wrong type', currentConfig[i].config, currentConfig[i].errorMsg);
        }

        currentConfig = require('./configWrongTypesContent.js');
        for (i = 0; i < currentConfig.length; i = i + 1) {
            failTestHelper('Should throw if the content of type is wrong', currentConfig[i].config, currentConfig[i].errorMsg);
        }
    });

    describe('Commands', function () {
        currentConfig = require('./configWrongCommand.js');
        for (i = 0; i < currentConfig.length; i = i + 1) {
            failTestHelper('Should throw if there is no commands or of wrong type', currentConfig[i].config, currentConfig[i].errorMsg);
        }
    });*/

    describe('Contextuals errors', function () {
        currentConfig = require('./configContextualErrors.js');
        for (i = 0; i < currentConfig.length; i = i + 1) {
            failTestHelper(currentConfig[i].errorMsg, currentConfig[i].config);
        }
    });
});

describe('Success parse configuration', function () {
    var currentConfig;
    var i;

    describe('Types', function () {
        currentConfig = require('./configGoodTypesContent.js');
        for (i = 0; i < currentConfig.length; i = i + 1) {
            successTestHelper(currentConfig[i].successMsg, currentConfig[i].config, currentConfig[i].checkResult);
        }
    });

    describe('$ samples', function () {
        currentConfig = require('./configValid$Sample.js');
        for (i = 0; i < currentConfig.length; i = i + 1) {
            successTestHelper(currentConfig[i].successMsg, currentConfig[i].config, currentConfig[i].checkResult);
        }
    });
});

describe('Parse data', function () {
    describe('Global view', function () {
        var currentConfig = require('./configParseGlobal.js');


    });
});