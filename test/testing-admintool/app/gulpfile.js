var gulp = require('gulp');
var gutil = require('gulp-util');
var browserify = require('browserify');
var vinylSource = require('vinyl-source-stream');
var vinylBuffer = require('vinyl-buffer');
var path = require('path');

gulp.task('js', function () {
    var distPath = path.join(__dirname, 'dist');

    gutil.log(distPath);
    return browserify('./js/app.js', {debug: true /* isDebug */})
        .bundle()
        .on('error', function (err) {
            gutil.log(err);
        })
        .pipe(vinylSource('bundle.js'))
        .pipe(vinylBuffer())
        .pipe(gulp.dest(distPath))
});
