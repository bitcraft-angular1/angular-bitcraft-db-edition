/*global angular, console, $*/
'use strict';

var adminTool = angular.module('AdminToolApp', [
    'dialogs.main',
    'pascalprecht.translate', // multi language support
    'ngCookies',
    'ui.bootstrap',
    'ui.router',
    'ui-notification',
    //Add components here
    'bitcraft-db-edition',
    'explanation',
    'hello-world'
]);

require('./explanation');
require('./hello-world');

adminTool.config(function ($stateProvider, $urlRouterProvider, $translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: 'js/locale-',
        suffix: '.json'
    });
    $translateProvider
        .preferredLanguage('en')
        .fallbackLanguage('en');
    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.useCookieStorage();

    $urlRouterProvider.when('/', '/db-edition/display-edit-same-collection');
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('root', {
            url: '/',
            views: {
                header: {
                    templateUrl: 'partials/header.html'
                },
                sidebar: {
                    templateUrl: 'partials/sidebar.html'
                },
                content: {
                    templateUrl: 'partials/content.html'
                }
            }
        })
        .state('root.db-edition', {
            url: 'db-edition/:command',
            views: {
                'content@': {
                    component: 'bitcraftDbEditionGlobal'
                }
            }
        })
        .state('root.db-edition.add', {
            url: '/add',
            views: {
                'content@': {
                    component: 'bitcraftDbEditionAdd'
                }
            }
        })
        .state('root.db-edition.id', {
            url: '/view/:id',
            views: {
                'content@': {
                    component: 'bitcraftDbEditionId'
                }
            }
        });
});

adminTool.controller('AdminToolAppCtrl', [
    '$scope',
    function ($scope) {
        var navbarContext = {
            toggled: false,
            toggleButton: null
        };

        $scope.applicationName = 'Admin Tool';

        $scope.menu = [{
            title: 'Display and edit same collection',
            href: 'root.db-edition({command: "display-edit-same-collection"})',
            icon: 'fa-database'
        }, {
            title: 'Display and edit another collection (jump)',
            href: 'root.db-edition({command: "display-edit-other-collection"})',
            icon: 'fa-database'
        }, {
            title: 'Iterate display',
            href: 'root.db-edition({command: "iterate-display"})',
            icon: 'fa-database'
        }, {
            title: 'Iterate edit',
            href: 'root.db-edition({command: "iterate-edit"})',
            icon: 'fa-database'
        }, {
            title: 'Dropdown',
            href: 'root.db-edition({command: "dropdown"})',
            icon: 'fa-database'
        }, {
            title: 'Save keys or values',
            href: 'root.db-edition({command: "save-keys-values"})',
            icon: 'fa-database'
        }, {
            title: 'Translation',
            href: 'root.db-edition({command: "translation"})',
            icon: 'fa-database'
        }, {
            title: 'Aggregation',
            href: 'root.db-edition({command: "aggregation"})',
            icon: 'fa-database'
        }];

        /**
         * @param {Object} [$event]
         */
        $scope.toggleNav = function ($event) {
            if (!navbarContext.toggleButton) {
                navbarContext.toggleButton = $(angular.element($event.currentTarget));
            }
            navbarContext.toggleButton.toggleClass('visible-xs text-center');
            //noinspection JSUnresolvedFunction
            navbarContext.toggleButton.find('i').toggleClass('glyphicon-chevron-right glyphicon-chevron-left');
            // toggle the menu
            $('#lg-menu').toggleClass('hidden-xs').toggleClass('visible-xs');
            //noinspection JSJQueryEfficiency
            $('#lg-menu ul').toggleClass('hidden-xs').toggleClass('visible-xs');
            $('#xs-menu').toggleClass('visible-xs').toggleClass('hidden-xs');

            // we want to turn the sidebar as an overlay
            $('#sidebar').toggleClass('col-sm-2 col-xs-1').toggleClass('col-sm-4 col-xs-8').toggleClass('overlay');

            navbarContext.toggled = !navbarContext.toggled;
        };

        $scope.onNavLinkClicked = function () {
            if (navbarContext.toggled) {
                $scope.toggleNav(null);
            }
        };
    }
]);

adminTool.run([
    '$rootScope', '$translate', 'DBEdition',
    function ($rootScope, $translate, DBEdition) {
        console.log('Admin tool started');

        var dbEditionConfig = require('./db-edition.conf.js');
        DBEdition.parseConfiguration(dbEditionConfig);

        $rootScope.changeLanguage = function (lang) {
            $translate.use(lang);
        };
    }
]);

/**
 * @typedef {Object} applicationEnvironment
 * @property {string} name
 * @property {string} title
 */