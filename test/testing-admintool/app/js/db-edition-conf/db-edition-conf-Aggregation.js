'use strict';

module.exports = {
    name: 'aggregation',
    collection: 'aggregation',
    edit: true,
    nodes: [{
        name: 'Month',
        view: 3,
        saveAs: 'string',
        accessor: 'month',
        displayAs: 'readonly',
        sortable: true,
        searchable: true
    }, {
        name: 'Sales',
        view: 3,
        saveAs: 'number',
        accessor: 'sales',
        displayAs: 'number',
        sortable: true,
        searchable: true,
        aggregatable: [true, true, true, true, true]
    }],
    aggregate: [{
        name: 'Sum',
        bgColor: 'lemonChiffon',
        aggregationFunction: function (lastReturned, currentValue, currentIndex, remainingElements, totalElements) {
            return lastReturned + currentValue;
        }
    }, {
        name: 'Mean',
        bgColor: 'lemonChiffon',
        aggregationFunction: function (lastReturned, currentValue, currentIndex, remainingElements, totalElements) {
            if (remainingElements === 0) {
                return (lastReturned + currentValue) / totalElements;
            }
            return lastReturned + currentValue;
        }
    }, {
        name: 'Max',
        bgColor: 'lemonChiffon',
        aggregationFunction: function (lastReturned, currentValue, currentIndex, remainingElements, totalElements) {
            return lastReturned > currentValue ?  lastReturned : currentValue;
        }
    }, {
        name: 'Std dev',
        bgColor: 'lemonChiffon',
        aggregationFunction: function (lastReturned, currentValue, currentIndex, remainingElements, totalElements) {
            //sqrt(1sum((x - xi)^2)/n)
            if (remainingElements === 0) {
                var mean = (lastReturned.tot + currentValue) / totalElements;
                var sum = 0;
                var i;

                for (i = 0; i < lastReturned.all.length; i += 1) {
                    sum += (lastReturned.all[i] - mean) * (lastReturned.all[i] - mean);
                }

                return Math.sqrt(sum / totalElements);
            }

            if (!lastReturned) {
                lastReturned = { tot: 0, all: []};
            }
            lastReturned.all.push(currentValue);
            lastReturned.tot += currentValue;
            return lastReturned;
        }
    }, {
        name: 'Number of elements',
        bgColor: 'lemonChiffon',
        aggregationFunction: function (lastReturned, currentValue, currentIndex, remainingElements, totalElements) {
            return totalElements;
        }
    }],
    components: [{
        name: 'explanation',
        view: 1,
        anchorToTop: false
    }]
};
