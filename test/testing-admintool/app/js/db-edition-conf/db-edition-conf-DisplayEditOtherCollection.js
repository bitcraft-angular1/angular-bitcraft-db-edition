module.exports = {
    name: 'display-edit-other-collection',
    collection: 'display-edit-other-collection',
    edit: true,
    sort: true,
    detailedView: true,
    add: false,
    nodes: [{
        name: 'Name',
        view: 3,
        saveAs: 'string',
        accessor: 'name',
        displayAs: 'readonly'
    }, {
        name: 'Readonly',
        view: 3,
        saveAs: 'string',
        accessor: ['other-id', {accessor: 'id', collection: 'display-edit-same-collection'}, 'readonly'],
        displayAs: 'readonly'
    }, {
        name: 'Text',
        view: 3,
        saveAs: 'string',
        accessor: ['other-id', {accessor: 'id', collection: 'display-edit-same-collection'}, 'text'],
        displayAs: 'text'
    }, {
        name: 'Number',
        view: 3,
        saveAs: 'number',
        accessor: ['other-id', {accessor: 'id', collection: 'display-edit-same-collection'}, 'number'],
        displayAs: 'number'
    }, {
        name: 'Checkbox',
        view: 3,
        saveAs: 'boolean',
        accessor: ['other-id', {accessor: 'id', collection: 'display-edit-same-collection'}, 'checkbox'],
        displayAs: 'checkbox'
    }, {
        name: 'Checkboxes',
        view: 3,
        saveAs: 'map',
        accessor: ['other-id', {accessor: 'id', collection: 'display-edit-same-collection'}, 'checkboxes'],
        displayAs: 'checkboxes'
    }, {
        name: 'Date',
        view: 3,
        saveAs: 'date',
        accessor: ['other-id', {accessor: 'id', collection: 'display-edit-same-collection'}, 'date'],
        displayAs: 'date'
    }, {
        name: 'Image',
        view: 3,
        saveAs: 'string',
        accessor: ['other-id', {accessor: 'id', collection: 'display-edit-same-collection'}, 'image'],
        displayAs: 'image'
    }],
    components: [{
        name: 'explanation',
        view: 1,
        anchorToTop: false
    }]
};
