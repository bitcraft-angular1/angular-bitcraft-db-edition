module.exports = {
    name: 'display-edit-same-collection',
    collection: 'display-edit-same-collection',
    edit: true,
    sort: true,
    search: true,
    detailedView: true,
    add: true,
    del: true,
    nodes: [{
        name: 'id',
        view: 0,
        saveAs: 'string',
        accessor: 'id',
        displayAs: 'text',
        defaultValue: '1234-5678-9abc'
    }, {
        name: 'Readonly',
        view: 'BothViews',
        saveAs: 'string',
        accessor: 'readonly',
        displayAs: 'readonly',
        sortable: true,
        searchable: true,
        defaultValue: 'cannot_change'
    }, {
        name: 'Text',
        view: 3,
        saveAs: 'string',
        accessor: 'text',
        displayAs: 'text',
        sortable: true,
        searchable: true,
        defaultValue: 'text'
    }, {
        name: 'Number',
        view: 3,
        saveAs: 'number',
        accessor: 'number',
        displayAs: 'number',
        sortable: true,
        searchable: true,
        defaultValue: 1
    }, {
        name: 'Checkbox',
        view: 3,
        saveAs: 'boolean',
        accessor: 'checkbox',
        displayAs: 'checkbox',
        sortable: true,
        searchable: true,
        defaultValue: true
    }, {
        name: 'Checkboxes',
        view: 3,
        saveAs: 'map',
        accessor: 'checkboxes',
        displayAs: 'checkboxes',
        sortable: true,
        searchable: true,
        defaultValue: {check1: false, check2: false, check3: false}
    }, {
        name: 'Date',
        view: 3,
        saveAs: 'date',
        accessor: 'date',
        displayAs: 'date',
        sortable: true,
        searchable: true,
        defaultValue: Date.now()
    }, {
        name: 'Image',
        view: 3,
        saveAs: 'string',
        accessor: 'image',
        displayAs: 'image',
        sortable: true,
        searchable: true,
        defaultValue: 'public/angel.png'
    }, {
        name: 'Deep field',
        view: 3,
        saveAs: 'string',
        accessor: ['deep', 'text'],
        displayAs: 'text',
        sortable: true,
        searchable: true,
        defaultValue: 'deep text'
    }, {
        name: 'Deeper field',
        view: 3,
        saveAs: 'string',
        accessor: ['deep', 'deeper', 'text'],
        displayAs: 'text',
        sortable: true,
        searchable: true,
        defaultValue: 'deeper text'
    }],
    components: [{
        name: 'explanation',
        view: 1,
        anchorToTop: false
    }]
};
