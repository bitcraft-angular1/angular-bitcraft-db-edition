'use strict';

module.exports = {
    name: 'dropdown',
    collection: 'dropdown',
    edit: true,
    nodes: [{
        name: 'Simple string dropdown',
        view: 3,
        saveAs: 'string',
        accessor: 'simpleDropdown1',
        displayAs: 'Dropdown',
        values: ['Choice1', 'Choice2', 'Choice3']
    }, {
        name: 'Simple number dropdown',
        view: 3,
        saveAs: 'number',
        accessor: 'simpleDropdown2',
        displayAs: 'Dropdown', // dropdown expects a string, if it's possible, the casting is made automatically (parseInt(v, 10))
        values: ['1', '2', '3', '4']
    }, {
        name: 'Simple number dropdown (letters)',
        view: 3,
        saveAs: 'number',
        accessor: 'simpleDropdown3',
        displayAs: 'Dropdown', // otherwise, you need to provide the translations functions.
        values: ['one', 'two', 'three', 'four'],
        translationToDisplay: function (v) {
            switch (v) {
            case 1:
                return 'one';
            case 2:
                return 'two';
            case 3:
                return 'three';
            case 4:
                return 'four';
            default:
                return 'ERROR';
            }
        },
        translationToDB: function (v) {
            switch (v) {
            case 'one':
                return 1;
            case 'two':
                return 2;
            case 'three':
                return 3;
            case 'four':
                return 4;
            default:
                return 1;
            }
        }
    }, {
        name: 'Simple boolean dropdown',
        view: 3,
        saveAs: 'boolean',
        accessor: 'simpleDropdown4',
        displayAs: 'Dropdown', // same here  (v === 'true')
        values: ['true', 'false']
    }, {
        name: 'Simple boolean dropdown (upper case)',
        view: 3,
        saveAs: 'boolean',
        accessor: 'simpleDropdown5',
        displayAs: 'Dropdown',
        values: ['TRUE', 'FALSE'],
        translationToDisplay: function (v) { return v.toString().toUpperCase(); },
        translationToDB: function (v) { return v.toLowerCase() === 'true'; }
    }, {
        name: 'Weird schema',
        view: 3,
        saveAs: 'map',
        accessor: 'mapToDropdown',
        displayAs: 'Dropdown',
        values: ['Male', 'Female', 'Cyborg'],
        translationToDisplay: function (v) {
            if (v.male === 1) {
                return 'Male';
            }
            if (v.female === 1) {
                return 'Female';
            }
            if (v.cyborg === 1) {
                return 'Cyborg';
            }
            return 'Unknown';
        },
        translationToDB: function (v) {
            return {
                male: v === 'Male' ? 1 : 0,
                female: v === 'Female' ? 1 : 0,
                cyborg: v === 'Cyborg' ? 1 : 0
            };
        }
    }],
    components: [{
        name: 'explanation',
        view: 1,
        anchorToTop: false
    }]
};
