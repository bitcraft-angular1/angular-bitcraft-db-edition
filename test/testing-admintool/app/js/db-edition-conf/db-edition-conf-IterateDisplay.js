module.exports = {
    name: 'iterate-display',
    collection: 'iterate',
    detailedView: true,
    nodes: [{
        name: 'ID',
        view: 3,
        saveAs: 'ObjectId',
        accessor: '_id',
        displayAs: 'readonly'
    }, {
        name: 'Iterate on map',
        view: 'DetailedView',
        saveAs: 'map',
        accessor: 'map',
        iterate: true,
        displayAs: [{
            saveAs: 'string',
            displayAs: 'text'
        }]
    }, {
        name: 'Iterate on array',
        view: 'DetailedView',
        saveAs: 'array',
        accessor: 'array',
        iterate: true,
        displayAs: [{
            saveAs: 'string',
            displayAs: 'text'
        }]
    }, {
        name: 'Iterate on array (hidden keys)',
        view: 'DetailedView',
        saveAs: 'array',
        accessor: 'array',
        iterate: true,
        displayAs: [{
            hideKey: true,
            saveAs: 'string',
            displayAs: 'text'
        }]
    }, {
        name: 'Iterate on array -> Field',
        view: 'DetailedView',
        saveAs: 'array',
        accessor: 'arraymap',
        iterate: true,
        displayAs: [{
            saveAs: 'string',
            displayAs: 'text',
            accessor: 'one'
        }]
    }, {
        name: 'Array -> Field',
        view: 'DetailedView',
        saveAs: 'array',
        accessor: 'arraymap',
        displayAs: [{
            name: 'French',
            saveAs: 'string',
            displayAs: 'text',
            accessor: ['0', 'one']
        }, {
            name: 'German',
            saveAs: 'string',
            displayAs: 'text',
            accessor: ['1', 'one']
        }, {
            name: 'Japanese',
            saveAs: 'string',
            displayAs: 'text',
            accessor: ['2', 'one']
        }]
    }, {
        name: 'Iterate on Array, iterate on Array',
        view: 2,
        saveAs: 'array',
        accessor: 'arrayarray',
        iterate: true,
        displayAs: [{
            saveAs: 'array',
            iterate: true,
            displayAs: [{
                saveAs: 'string',
                displayAs: 'text'
            }]
        }]
    }, {
        name: 'Iterate on Array (keys hidden), iterate on Array (keys hidden)',
        view: 2,
        saveAs: 'array',
        accessor: 'arrayarray',
        iterate: true,
        displayAs: [{
            saveAs: 'array',
            hideKey: true,
            iterate: true,
            displayAs: [{
                hideKey: true,
                saveAs: 'string',
                displayAs: 'text'
            }]
        }]
    }, {
        name: 'Iterate on Array, iterate on Map',
        view: 2,
        saveAs: 'array',
        iterate: true,
        accessor: 'arraymap',
        displayAs: [{
            saveAs: 'map',
            iterate: true,
            displayAs: [{
                saveAs: 'string',
                displayAs: 'text'
            }]
        }]
    }, {
        name: 'Iterate on Map, iterate on Array',
        view: 2,
        saveAs: 'map',
        iterate: true,
        accessor: 'maparray',
        displayAs: [{
            saveAs: 'array',
            iterate: true,
            displayAs: [{
                saveAs: 'string',
                displayAs: 'text'
            }]
        }]
    }, {
        name: 'Iterate on Map, iterate on Map',
        view: 2,
        saveAs: 'map',
        iterate: true,
        accessor: 'mapmap',
        displayAs: [{
            saveAs: 'map',
            iterate: true,
            displayAs: [{
                saveAs: 'string',
                displayAs: 'text'
            }]
        }]
    }],
    components: [{
        name: 'explanation',
        view: 1,
        anchorToTop: false
    }]
};
