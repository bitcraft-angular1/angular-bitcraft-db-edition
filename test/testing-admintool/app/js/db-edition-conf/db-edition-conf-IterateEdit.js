module.exports = {
    name: 'iterate-edit',
    collection: 'iterate',
    edit: true,
    add: true,
    detailedView: true,
    nodes: [{
        name: 'ID',
        view: 3,
        accessor: '_id',
        displayAs: 'readonly'
    }, {
        name: 'Iterate on map',
        view: 'DetailedView',
        saveAs: 'map',
        accessor: 'map',
        iterate: true,
        displayAs: [{
            saveAs: 'string',
            displayAs: 'text'
        }],
        defaultValue: {one: 'un', two: 'deux', three: 'trois'}
    }, {
        name: 'Iterate on array',
        view: 'DetailedView',
        saveAs: 'array',
        accessor: 'array',
        iterate: true,
        displayAs: [{
            saveAs: 'string',
            displayAs: 'text'
        }],
        defaultValue: ['one', 'two', 'three']
    }, {
        name: 'Iterate on Array, iterate on Array',
        view: 2,
        saveAs: 'array',
        accessor: 'arrayarray',
        iterate: true,
        displayAs: [{
            saveAs: 'array',
            iterate: true,
            displayAs: [{
                saveAs: 'string',
                displayAs: 'text'
            }]
        }],
        defaultValue: [['one', 'two', 'three'], ['un', 'deux', 'trois'], ['ein', 'zwei', 'drei']]
    }, {
        name: 'Iterate on Array, iterate on Map',
        view: 2,
        saveAs: 'array',
        iterate: true,
        accessor: 'arraymap',
        displayAs: [{
            saveAs: 'map',
            iterate: true,
            displayAs: [{
                saveAs: 'string',
                displayAs: 'text'
            }]
        }],
        defaultValue: [{one: 'un', two: 'deux'}, {one: 'ein', two: 'zwei'}, {one: 'ichi', two: 'ni'}]
    }, {
        name: 'Iterate on Map, iterate on Array',
        view: 2,
        saveAs: 'map',
        iterate: true,
        accessor: 'maparray',
        displayAs: [{
            saveAs: 'array',
            iterate: true,
            displayAs: [{
                saveAs: 'string',
                displayAs: 'text'
            }]
        }],
        defaultValue: {english: ['one', 'two', 'three'], french: ['un', 'deux', 'trois']}
    }, {
        name: 'Iterate on Map, iterate on Map',
        view: 2,
        saveAs: 'map',
        iterate: true,
        accessor: 'mapmap',
        displayAs: [{
            saveAs: 'map',
            iterate: true,
            displayAs: [{
                saveAs: 'string',
                displayAs: 'text'
            }]
        }],
        defaultValue: {english: {one: 'one', two: 'two'}, french: {one: 'un', two: 'deux'}}
    }],
    components: [{
        name: 'explanation',
        view: 1,
        anchorToTop: false
    }]
};
