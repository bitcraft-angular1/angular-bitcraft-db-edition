module.exports = {
    name: 'save-keys-values',
    collection: 'save-keys-values',
    detailedView: true,
    edit: true,
    nodes: [{
        name: 'ID',
        view: 3,
        saveAs: 'ObjectId',
        accessor: '_id',
        displayAs: 'readonly'
    }, {
        name: 'Iterate on map',
        view: 'DetailedView',
        saveAs: 'map',
        accessor: 'map',
        iterate: true,
        saveKeys: 'MapKeys',
        saveValues: 'MapValues',
        displayAs: [{
            saveAs: 'string',
            displayAs: 'text'
        }]
    }, {
        name: 'Iterate on array',
        view: 'DetailedView',
        saveAs: 'array',
        accessor: 'array',
        iterate: true,
        saveKeys: 'ArrayKeys',
        saveValues: 'ArrayValues',
        displayAs: [{
            saveAs: 'string',
            displayAs: 'text'
        }]
    }, {
        name: 'Map keys',
        view: 'DetailedView',
        saveAs: 'string',
        accessor: 'mapKeys',
        displayAs: 'dropdown',
        values: 'MapKeys'
    }, {
        name: 'Map values',
        view: 'DetailedView',
        saveAs: 'string',
        accessor: 'mapValues',
        displayAs: 'dropdown',
        values: 'MapValues'
    }, {
        name: 'Array keys',
        view: 'DetailedView',
        saveAs: 'string',
        accessor: 'arrayKeys',
        displayAs: 'dropdown',
        values: 'ArrayKeys'
    }, {
        name: 'Array keys as number',
        view: 'DetailedView',
        saveAs: 'number',
        accessor: 'arrayKeysNumber',
        displayAs: 'dropdown',
        values: 'ArrayKeys'
    }, {
        name: 'Array values',
        view: 'DetailedView',
        saveAs: 'string',
        accessor: 'arrayValues',
        displayAs: 'dropdown',
        values: 'ArrayValues'
    }],
    components: [{
        name: 'explanation',
        view: 'BothViews',
        anchorToTop: false
    }]
};
