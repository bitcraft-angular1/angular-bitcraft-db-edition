'use strict';

module.exports = {
    name: 'translation',
    collection: 'translation',
    edit: true,
    nodes: [{
        name: 'Without translation',
        view: 3,
        saveAs: 'string',
        accessor: 'prefixedString',
        displayAs: 'readonly'
    }, {
        name: 'With translation',
        view: 3,
        saveAs: 'string',
        accessor: 'prefixedString',
        displayAs: 'text',
        translationToDisplay: function (valueInDb) {
            return valueInDb.substr(7);
        },
        translationToDB: function (valueInUI) {
            return 'prefix_' + valueInUI;
        }
    }],
    components: [{
        name: 'explanation',
        view: 1,
        anchorToTop: false
    }]
};
