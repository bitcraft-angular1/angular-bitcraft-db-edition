'use strict';

var DisplayEditSameCollection = require('./db-edition-conf/db-edition-conf-DisplayEditSameCollection.js');
var DisplayEditOtherCollection = require('./db-edition-conf/db-edition-conf-DisplayEditOtherCollection.js');
var IterateDisplay = require('./db-edition-conf/db-edition-conf-IterateDisplay.js');
var IterateEdit = require('./db-edition-conf/db-edition-conf-IterateEdit.js');
var SaveKeysValues = require('./db-edition-conf/db-edition-conf-SaveKeysValues.js');
var Dropdown = require('./db-edition-conf/db-edition-conf-Dropdown.js');
var Translation = require('./db-edition-conf/db-edition-conf-TranslationFunction.js');
var Aggregation = require('./db-edition-conf/db-edition-conf-Aggregation.js');

module.exports = [
    DisplayEditSameCollection,
    DisplayEditOtherCollection,
    IterateDisplay,
    IterateEdit,
    SaveKeysValues,
    Dropdown,
    Translation,
    Aggregation
];
