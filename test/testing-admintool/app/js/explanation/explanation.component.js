/*global angular*/
'use strict';

//noinspection JSUnresolvedFunction
angular.module('explanation').
    component('explanation', {
        templateUrl: 'js/explanation/explanation.template.html',
        controller: [
            '$stateParams',
            function ($stateParams) {
                var self = this;

                this.$onInit = function () {
                    self.which = $stateParams.command;
                };
            }
        ]
    });
