/*global angular*/
'use strict';

//noinspection JSUnresolvedFunction
angular.module('hello-world').
    component('helloWorld', {
        templateUrl: 'js/hello-world/hello-world.template.html',
        controller: ['$scope',
            function ($scope) {
                //noinspection JSUnusedGlobalSymbols
                this.$onInit = function () {
                    $scope.hello = 'Hello World';
                    console.log(this.data);
                };
            }],
        bindings: {
            data: '<'
        }
    });
