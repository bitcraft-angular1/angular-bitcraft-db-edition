'use strict';

var hybridServer = require('./lib/hybridServer');
var rest = require('./lib/rest');
var mongo = require('bitcraft-mongo');

/**
 * @param {string} msg
 * @param {number} delay
 */
function delayedExit(msg, delay) {
    console.log(msg);
    setTimeout(function () {
        process.exit(1);
    }, delay);
}

rest.setup(function (err, handler) {
    /** @type {Server} */
    var server;

    if (err) {
        delayedExit('Failed to REST server: ' + err, 100);
        return;
    }

    server = hybridServer.createServer(__dirname + '/app', handler);
    server.listen(1340);
});

mongo.setup([{
    host: "127.0.0.1",
    port: 27017,
    dbname: "test-dbe"
}], function (err) {
    if (err) {
        delayedExit('Failed to initialize mongoDB: ' + err, 100);
    }
});
