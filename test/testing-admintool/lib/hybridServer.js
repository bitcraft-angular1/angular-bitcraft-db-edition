/* Copyright (C) bitcraft Co. Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Quentin BRANDON <quentin@bitcraft.co.jp>, April 2015
 */

'use strict';
var http = require('http');
var url = require('url');
var staticFiles = require('node-static');

/**
 * @param {Url} reqUrl
 * @returns {string|undefined}
 */
function safeDecodePathname(reqUrl) {
    /** @type {string|undefined} */
    var pathname;
    try {
        pathname = decodeURI(url.parse(reqUrl).pathname);
    } catch (e) {
        return;
    }
    return pathname;
}

/**
 * @param {IncomingMessage} req
 * @returns {boolean}
 */
function handlePathname(req) {
    /** @type {string|undefined} */
    var pathname;
    /** @type {string} */
    var first;

    pathname = safeDecodePathname(req.url);
    if (pathname === undefined) {
        return true;
    }

    first = pathname.split('/')[1];
    if (first === 'rest') {
        req.url = req.url.slice(5);
        return false;
    }
    return true;
}

/**
 * Creates an HTTP server that serves both static and dynamic content:
 * /rest URLs will be passed on to the handler function
 * the other URLs will be handled as accessing static files
 * @param {string} path Relative path to the static files to be served
 * @param {requestListener} handler
 * @returns {Server}
 */
function createServer(path, handler) {
    var file = new staticFiles.Server(path, {gzip: true, cache: false});
    return http.createServer(function (request, response) {
        console.log('handle ' + request.method + ' ' + request.url);
        if (handlePathname(request)) {
            request.addListener('end', function () {
                file.serve(request, response);
            }).resume();
        } else {
            handler(request, response);
        }
    });
}

module.exports = {
    createServer: createServer
};

/**
 * @callback requestListener
 * @param {IncomingMessage} request
 * @param {ServerResponse} response
 */
