/**
 * Created by richard on 23/09/16.
 */

'use strict';
var log4js = require('log4js');
var mongo = require('../plugins/lib/mongo.js');
var util = require('util');
var perm = require('../groupPermissions');

function checkSignature(req, constraints, callback) {
    var newErr = new Error("Unauthorized");
    newErr.code = 401;
    if (!req.headers.cookie) {
        callback(newErr);
        return;
    }
    var token = req.headers.cookie.match(/token=([^;]*)/);
    if (!token || !token[1]) {
        callback(newErr);
        return;
    }
    token = token[1];
    console.log(token);

    mongo.getDefaultDB(function (err, db) {
        if (err) {
            callback(err);
            return;
        }
        db.collection("credentials").find(
            {token: token},
            {_id: 0, group: 1}
        ).toArray(function (err, group) {
            if (err) {
                callback(err);
                return;
            }
            console.log(JSON.stringify(group));
            if (!group[0]) {
                callback(newErr);
            } else {
                if (isPermissionValid(group[0].group, constraints.extra)) {
                    callback(undefined, undefined);
                } else {
                    callback(newErr);
                }
            }
        });
    });
}

function isPermissionValid(mainGroup, resource) {
    var list = perm[resource];
    if (list) {
        return list.length === 0 || list.indexOf(mainGroup) > -1;
    } else {
        return false;
    }
}

function checkPostData(req) {
    return true;
}

module.exports.authChecker = {
    checkSignature: checkSignature,
    checkPostData: checkPostData
};
