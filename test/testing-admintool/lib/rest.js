'use strict';

var REST = require('laposte').REST;

/** @type {REST} */
var rest;

/**
 * @param {function(Error|undefined, function)} callback
 */
module.exports.setup = function (callback) {
    var done = false;
    var handler;

    if (rest) {
        callback(new Error('rest already initialized'));
        return;
    }

    function registerRequestHandler(requestHandler) {
        if (done) {
            callback(undefined, requestHandler);
            return;
        }
        handler = requestHandler;
    }
    rest = new REST();
    rest.run({pluginsPath: './plugins', requestHandler: registerRequestHandler}, function (err) {
        if (err) {
            callback(err);
            return;
        }
        done = true;
        if (handler) {
            callback(undefined, handler);
        }
    });
};
