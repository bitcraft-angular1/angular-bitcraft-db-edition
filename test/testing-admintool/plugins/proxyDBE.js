'use strict';

// mongo style
// var dbEditionPlugins = require('angular-bitcraft-db-edition').mongo;
//
// dbEditionPlugins.setup(5 * 60 * 1000, function () { return 'test-dbe'; }, require('bitcraft-mongo'));
// module.exports = dbEditionPlugins.plugins;


//spanner style
var dbEditionPlugins = require('angular-bitcraft-db-edition').spanner;
// credentials
process.env.GOOGLE_APPLICATION_CREDENTIALS = '/home/richard/.ssh/scream.json';
// spanner setup
var Spanner = require('@google-cloud/spanner');
var spanner = new Spanner({projectId: 'projectId'});
var instance = spanner.instance('instanceId');
var database = instance.database('databaseId');


dbEditionPlugins.setup(5 * 60 * 1000, database);
module.exports = dbEditionPlugins.plugins;
